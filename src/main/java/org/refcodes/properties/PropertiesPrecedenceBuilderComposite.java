// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.properties;

import java.util.List;

import org.refcodes.properties.PropertiesPrecedence.PropertiesPrecedenceBuilder;

/**
 * The Class PropertiesPrecedenceBuilderComposite.
 */
public class PropertiesPrecedenceBuilderComposite extends PropertiesPrecedenceComposite implements PropertiesPrecedenceBuilder {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Creates a {@link PropertiesPrecedenceBuilder} composite querying the
	 * provided {@link Properties} in the given order. Queried properties of the
	 * first {@link Properties} instance containing them are returned.
	 * {@link Properties} before have a higher precedence over
	 * {@link Properties} provided next.
	 * 
	 * @param aProperties The {@link Properties} to be queried in the provided
	 *        order.
	 */
	public PropertiesPrecedenceBuilderComposite( List<Properties> aProperties ) {
		super( aProperties );
	}

	/**
	 * Creates a {@link PropertiesPrecedenceBuilder} composite querying the
	 * provided {@link Properties} in the given order. Queried properties of the
	 * first {@link Properties} instance containing them are returned.
	 * {@link Properties} before have a higher precedence over
	 * {@link Properties} provided next.
	 * 
	 * @param aProperties The {@link Properties} to be queried in the provided
	 *        order.
	 */
	public PropertiesPrecedenceBuilderComposite( Properties... aProperties ) {
		super( aProperties );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean appendProperties( Properties aProperties ) {
		if ( _properties.contains( aProperties ) ) {
			return false;
		}
		_properties.add( aProperties );
		return true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean prependProperties( Properties aProperties ) {
		if ( _properties.contains( aProperties ) ) {
			return false;
		}
		_properties.add( 0, aProperties );
		return true;
	}
}
