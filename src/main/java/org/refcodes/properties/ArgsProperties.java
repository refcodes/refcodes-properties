// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.properties;

import org.refcodes.data.ArgsPrefix;
import org.refcodes.data.Delimiter;
import org.refcodes.runtime.Arguments;

/**
 * The {@link ArgsProperties} takes them command line arguments "as is" and
 * converts them into key/value pairs as of the {@link Properties} semantics.
 * Keys for the key/value-pairs are identified by prefixes, usually as defined
 * by the {@link ArgsPrefix} enumeration. Such a key is without the prefix
 * called option and used used as key in the {@link Properties} instance. If the
 * succeeding argument is not an option, then the succeeding argument is used as
 * the option's value, else the current option is treated as boolean property
 * with a value of "true".
 */
public class ArgsProperties extends PropertiesImpl {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs the {@link ArgsProperties} from the provided command line
	 * arguments using the prefixes as defined by the {@link ArgsPrefix}
	 * enumeration to identify the keys (and the values) from the provided
	 * arguments.
	 * 
	 * @param aArgs The arguments from which to extract key/value-pairs.
	 */
	public ArgsProperties( String[] aArgs ) {
		this( aArgs, ArgsPrefix.toPrefixes() );
	}

	/**
	 * Constructs the {@link ArgsProperties} from the provided command line
	 * arguments using the provided prefixes to identify the keys (and the
	 * values) from the provided arguments.
	 * 
	 * @param aArgs The arguments from which to extract key/value-pairs.
	 * @param aPrefixes The prefixes to be used to identify the keys (and the
	 *        values) from the provided arguments
	 */
	public ArgsProperties( String[] aArgs, String... aPrefixes ) {
		super( Arguments.toProperties( aArgs, aPrefixes, Delimiter.PATH.getChar() ) );
	}
}
