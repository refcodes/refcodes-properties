// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.properties;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.ParseException;

import org.refcodes.properties.ResourceProperties.ResourcePropertiesBuilder;
import org.refcodes.runtime.ConfigLocator;

/**
 * The Class AbstractResourcePropertiesBuilderDecorator.
 *
 * @param <T> the generic type
 */
public class AbstractResourcePropertiesBuilderDecorator<T extends ResourcePropertiesBuilder> extends AbstractPropertiesBuilderDecorator<T> implements ResourcePropertiesBuilder {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Decorates the provided {@link ResourcePropertiesBuilder} with additional
	 * behavior or functionality. Changes applied to the provided
	 * {@link ResourcePropertiesBuilder} affect the decorator.
	 * 
	 * @param aProperties The {@link ResourceProperties} to be decorated.
	 */
	public AbstractResourcePropertiesBuilderDecorator( T aProperties ) {
		super( aProperties );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void flush() throws IOException {
		getProperties().flush();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isFlushable() {
		return getProperties().isFlushable();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Properties loadFrom( File aFile ) throws IOException, ParseException {
		return getProperties().loadFrom( aFile );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Properties loadFrom( InputStream aInputStream ) throws IOException, ParseException {
		return getProperties().loadFrom( aInputStream );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Properties reload() throws IOException, ParseException {
		return getProperties().reload();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Properties reload( ReloadMode aReloadMode ) throws IOException, ParseException {
		return getProperties().reload( aReloadMode );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public File saveTo( File aFile, String aComment, char aDelimiter ) throws IOException {
		return getProperties().saveTo( aFile, aComment, aDelimiter );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void saveTo( OutputStream aOutputStream, String aComment, char aDelimiter ) throws IOException {
		getProperties().saveTo( aOutputStream, aComment, aDelimiter );

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Properties seekFrom( Class<?> aResourceClass, String aFilePath, ConfigLocator aConfigLocator ) throws IOException, ParseException {
		return getProperties().withSeekFrom( aResourceClass, aFilePath );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toSerialized( String aComment, char aDelimiter ) {
		return getProperties().toSerialized( aComment, aDelimiter );
	}

	// /////////////////////////////////////////////////////////////////////////
	// SUB-TYPES:
	// /////////////////////////////////////////////////////////////////////////

	//	/**
	//	 * {@inheritDoc}
	//	 */
	//	@Override
	//	public PropertiesBuilder retrieveFrom( String aFromPath ) {
	//		return getProperties().retrieveFrom( aFromPath );
	//	}
	//
	//	/**
	//	 * {@inheritDoc}
	//	 */
	//	@Override
	//	public PropertiesBuilder retrieveTo( String aToPath ) {
	//		return getProperties().retrieveTo( aToPath );
	//	}
}
