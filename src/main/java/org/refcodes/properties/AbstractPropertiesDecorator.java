// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.properties;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

import org.refcodes.struct.StructureUtility;

/**
 * Decorates the provided {@link Properties} and delegates method calls to them
 * {@link Properties}.
 * 
 * @param <T> The actual (sub-) type of the {@link Properties} to be decorated.
 */
public abstract class AbstractPropertiesDecorator<T extends Properties> implements Properties {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private T _properties;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Decorates the provided {@link Properties} with additional behavior or
	 * functionality. Changes applied to the provided {@link Properties} affect
	 * the decorator.
	 * 
	 * @param aProperties The {@link Properties} to be decorated.
	 */
	public AbstractPropertiesDecorator( T aProperties ) {
		_properties = aProperties;
	}

	/**
	 * Make sure to set the _properties member variable!.
	 */
	protected AbstractPropertiesDecorator() {}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean containsKey( Object aKey ) {
		return getProperties().containsKey( aKey );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String get( Object aKey ) {
		return getProperties().get( aKey );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public char getAnnotator() {
		return getProperties().getAnnotator();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public char getDelimiter() {
		return getProperties().getDelimiter();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isEmpty() {
		return getProperties().isEmpty();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Set<String> keySet() {
		return getProperties().keySet();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Properties retrieveFrom( String aFromPath ) {
		// As a decorator may overwrite methods such as get(), we must not delegate this to the underlying delegatee! |-->
		final PropertiesBuilderImpl theToPathMap = new PropertiesBuilderImpl( getDelimiter() );
		StructureUtility.retrieveFrom( this, aFromPath, theToPathMap );
		return theToPathMap;
		// As a decorator may overwrite methods such as get(), we must not delegate this to the underlying delegatee! <--|
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Properties retrieveTo( String aToPath ) {
		// As a decorator may overwrite methods such as get(), we must not delegate this to the underlying delegatee! |--> 
		final PropertiesBuilder theToPathMap = new PropertiesBuilderImpl( getDelimiter() );
		StructureUtility.retrieveTo( this, aToPath, theToPathMap );
		return theToPathMap;
		// As a decorator may overwrite methods such as get(), we must not delegate this to the underlying delegatee! <--|
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int size() {
		return getProperties().size();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object toDataStructure( String aFromPath ) {
		// As a decorator may overwrite methods such as get(), we must not delegate this to the underlying delegatee! |-->
		return StructureUtility.toDataStructure( this, aFromPath );
		// As a decorator may overwrite methods such as get(), we must not delegate this to the underlying delegatee! <--|
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Map<String, String> toDump() {
		return _properties.toDump();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Map<String, String> toDump( Map<String, String> aDump ) {
		return _properties.toDump( aDump );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toPrintable() {
		return _properties.toPrintable();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Collection<String> values() {
		return getProperties().values();
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Delegate hook, this method is used to retrieve the properties to which to
	 * delegate method calls to.
	 * 
	 * @return The properties to which method calls are delegated to.
	 */
	protected T getProperties() {
		return _properties;
	}

	/**
	 * Hook to set the properties.
	 * 
	 * @param aProperties The properties to which method calls are delegated to.
	 */
	protected void setProperties( T aProperties ) {
		_properties = aProperties;
	}
}
