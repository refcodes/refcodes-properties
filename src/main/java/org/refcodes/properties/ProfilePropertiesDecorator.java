// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.properties;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.refcodes.data.Delimiter;

/**
 * The {@link ProfilePropertiesDecorator} type decorates a {@link Properties}
 * instance and enriches it with profile support as of
 * {@link ProfileProperties}.
 */
public class ProfilePropertiesDecorator extends AbstractPropertiesDecorator<Properties> implements ProfileProperties {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Create a {@link ProfileProperties} instance containing the elements of
	 * the provided {@link Properties} instance using the default path delimiter
	 * "/" ({@link Delimiter#PATH}) for the path declarations.
	 *
	 * @param aProperties the properties to be added.
	 */
	public ProfilePropertiesDecorator( Map<?, ?> aProperties ) {
		super( new PropertiesImpl( aProperties ) );
	}

	/**
	 * Create a {@link ProfilePropertiesDecorator} instance containing the
	 * elements as of {@link MutablePathMap#insert(Object)} using the default
	 * path delimiter "/" ({@link Delimiter#PATH}) for the path declarations:
	 * "Inspects the given object and adds all elements found in the given
	 * object. Elements of type {@link Map}, {@link Collection} and arrays are
	 * identified and handled as of their type: The path for each value in a
	 * {@link Map} is appended with its according key. The path for each value
	 * in a {@link Collection} or array is appended with its according index of
	 * occurrence (in case of a {@link List} or an array, its actual index). In
	 * case of reflection, the path for each member is appended with its
	 * according mamber's name. All elements (e.g. the members and values) are
	 * inspected recursively which results in the according paths of the
	 * terminating values."
	 *
	 * @param aObj The object from which the elements are to be added.
	 */
	public ProfilePropertiesDecorator( Object aObj ) {
		super( new PropertiesImpl( aObj ) );
	}

	/**
	 * Decorates(!) the provided {@link Properties} with the additional behavior
	 * from the {@link ProfileProperties} type. Changes applied to the provided
	 * {@link Properties} affect the decorator.
	 * 
	 * @param aProperties The {@link Properties} to be decorated.
	 */
	public ProfilePropertiesDecorator( Properties aProperties ) {
		super( aProperties );
	}

	/**
	 * Decorates(!) the provided {@link PropertiesBuilder} with the additional
	 * behavior from the {@link ProfileProperties} type. Changes applied to the
	 * provided {@link Properties} affect the decorator.
	 * 
	 * @param aProperties The {@link Properties} to be decorated.
	 */
	public ProfilePropertiesDecorator( PropertiesBuilder aProperties ) {
		super( aProperties );
	}

	// /////////////////////////////////////////////////////////////////////////
	// SUB-TYPED METHODS:
	// /////////////////////////////////////////////////////////////////////////

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

}
