// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.properties;

import java.io.File;

/**
 * Mode of operation regarding the {@link ResourceProperties#reload(ReloadMode)}
 * method.
 */
public enum ReloadMode {

	/**
	 * When reloading properties from a resource, then any properties not
	 * existing in the resource (e.g. in a {@link File}) are removed from the
	 * according {@link ResourceProperties}.
	 */
	ORPHAN_REMOVAL,

	/**
	 * When reloading properties from a resource, then any properties not
	 * existing in the resource (e.g. in a {@link File}) are kept in the
	 * according {@link ResourceProperties}.
	 */
	KEEP_ORPHANS

}
