// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.properties;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.ParseException;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.refcodes.data.Delimiter;
import org.refcodes.data.FilenameExtension;
import org.refcodes.runtime.ConfigLocator;

/**
 * Implementation of the {@link ResourceProperties} interface with support of so
 * called "YAML properties". For YAML properties, see
 * "https://en.wikipedia.org/wiki/YAML"
 */
public class YamlProperties extends AbstractResourceProperties {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Loads the YAML Properties from the given file's path.
	 *
	 * @param aResourceClass The class which's class loader is to take care of
	 *        loading the properties (from inside a JAR).
	 * @param aFilePath The file path of the class's resources from which to
	 *        load the properties.
	 * 
	 * @throws IOException thrown in case accessing or processing the properties
	 *         file failed.
	 * @throws ParseException Signals that an error has been reached
	 *         unexpectedly while parsing the data to be loaded.
	 */
	public YamlProperties( Class<?> aResourceClass, String aFilePath ) throws IOException, ParseException {
		super( new YamlPropertiesBuilder( aResourceClass, aFilePath ) );
	}

	/**
	 * Loads the YAML Properties from the given file's path. A provided
	 * {@link ConfigLocator} describes the locations to additional crawl for the
	 * desired file. Finally (if nothing else succeeds) the properties are
	 * loaded by the provided class's class loader which takes care of loading
	 * the properties (in case the file path is a relative path, also the
	 * absolute path with a prefixed path delimiter "/" is probed).
	 *
	 * @param aResourceClass The class which's class loader is to take care of
	 *        loading the properties (from inside a JAR).
	 * @param aFilePath The file path of the class's resources from which to
	 *        load the properties.
	 * @param aConfigLocator The {@link ConfigLocator} describes the locations
	 *        to additional crawl for the desired file.
	 * 
	 * @throws IOException thrown in case accessing or processing the properties
	 *         file failed.
	 * @throws ParseException Signals that an error has been reached
	 *         unexpectedly while parsing the data to be loaded.
	 */
	public YamlProperties( Class<?> aResourceClass, String aFilePath, ConfigLocator aConfigLocator ) throws IOException, ParseException {
		super( new YamlPropertiesBuilder( aResourceClass, aFilePath, aConfigLocator ) );
	}

	/**
	 * Loads the YAML Properties from the given {@link File}.
	 *
	 * @param aFile The {@link File} from which to load the properties.
	 * 
	 * @throws IOException thrown in case accessing or processing the properties
	 *         file failed.
	 * @throws ParseException Signals that an error has been reached
	 *         unexpectedly while parsing the data to be loaded.
	 */
	public YamlProperties( File aFile ) throws IOException, ParseException {
		super( new YamlPropertiesBuilder( aFile ) );
	}

	/**
	 * Loads or seeks the YAML Properties from the given {@link File}. A
	 * provided {@link ConfigLocator} describes the locations to additional
	 * crawl for the desired file.
	 * 
	 * @param aFile The {@link File} from which to load the properties.
	 * @param aConfigLocator The {@link ConfigLocator} describes the locations
	 *        to additional crawl for the desired file.
	 * 
	 * @throws IOException thrown in case accessing or processing the properties
	 *         file failed.
	 * @throws ParseException Signals that an error has been reached
	 *         unexpectedly while parsing the data to be loaded.
	 */
	public YamlProperties( File aFile, ConfigLocator aConfigLocator ) throws IOException, ParseException {
		super( new YamlPropertiesBuilder( aFile, aConfigLocator ) );
	}

	/**
	 * Reads the YAML Properties from the given {@link InputStream}.
	 *
	 * @param aInputStream The {@link InputStream} from which to read the
	 *        properties.
	 * 
	 * @throws IOException thrown in case accessing or processing the properties
	 *         file failed.
	 * @throws ParseException Signals that an error has been reached
	 *         unexpectedly while parsing the data to be loaded.
	 */
	public YamlProperties( InputStream aInputStream ) throws IOException, ParseException {
		super( new YamlPropertiesBuilder( aInputStream ) );
	}

	/**
	 * Create a {@link YamlProperties} instance containing the elements of the
	 * provided {@link Map} instance using the default path delimiter "/"
	 * ({@link Delimiter#PATH}) for the path declarations.
	 *
	 * @param aProperties the properties to be added.
	 */
	public YamlProperties( Map<?, ?> aProperties ) {
		super( new YamlPropertiesBuilder( aProperties ) );
	}

	/**
	 * Create a {@link YamlProperties} instance containing the elements as of
	 * {@link MutablePathMap#insert(Object)} using the default path delimiter
	 * "/" ({@link Delimiter#PATH}) for the path declarations: "Inspects the
	 * given object and adds all elements found in the given object. Elements of
	 * type {@link Map}, {@link Collection} and arrays are identified and
	 * handled as of their type: The path for each value in a {@link Map} is
	 * appended with its according key. The path for each value in a
	 * {@link Collection} or array is appended with its according index of
	 * occurrence (in case of a {@link List} or an array, its actual index). In
	 * case of reflection, the path for each member is appended with its
	 * according mamber's name. All elements (e.g. the members and values) are
	 * inspected recursively which results in the according paths of the
	 * terminating values."
	 *
	 * @param aObj The object from which the elements are to be added.
	 */
	public YamlProperties( Object aObj ) {
		super( new YamlPropertiesBuilder( aObj ) );
	}

	/**
	 * Create a {@link YamlProperties} instance containing the elements of the
	 * provided {@link Properties} instance using the default path delimiter "/"
	 * ({@link Delimiter#PATH}) for the path declarations.
	 *
	 * @param aProperties the properties to be added.
	 */
	public YamlProperties( Properties aProperties ) {
		super( new YamlPropertiesBuilder( aProperties ) );
	}

	/**
	 * Create a {@link YamlProperties} instance containing the elements of the
	 * provided {@link PropertiesBuilder} instance using the default path
	 * delimiter "/" ({@link Delimiter#PATH}) for the path declarations.
	 *
	 * @param aProperties the properties to be added.
	 */
	public YamlProperties( PropertiesBuilder aProperties ) {
		super( new YamlPropertiesBuilder( aProperties ) );
	}

	/**
	 * Loads the YAML Properties from the given file's path.
	 *
	 * @param aFilePath The path to the file from which to load the properties.
	 * 
	 * @throws IOException thrown in case accessing or processing the properties
	 *         file failed.
	 * @throws ParseException Signals that an error has been reached
	 *         unexpectedly while parsing the data to be loaded.
	 */
	public YamlProperties( String aFilePath ) throws IOException, ParseException {
		super( new YamlPropertiesBuilder( aFilePath ) );
	}

	/**
	 * Loads the YAML Properties from the given file's path. A provided
	 * {@link ConfigLocator} describes the locations to additional crawl for the
	 * desired file.
	 * 
	 * @param aFilePath The path to the file from which to load the properties.
	 * @param aConfigLocator The {@link ConfigLocator} describes the locations
	 *        to additional crawl for the desired file.
	 * 
	 * @throws IOException thrown in case accessing or processing the properties
	 *         file failed.
	 * @throws ParseException Signals that an error has been reached
	 *         unexpectedly while parsing the data to be loaded.
	 */
	public YamlProperties( String aFilePath, ConfigLocator aConfigLocator ) throws IOException, ParseException {
		super( new YamlPropertiesBuilder( aFilePath, aConfigLocator ) );
	}

	/**
	 * Loads the YAML Properties from the given {@link URL}.
	 *
	 * @param aUrl The {@link URL} from which to read the properties.
	 * 
	 * @throws IOException thrown in case accessing or processing the properties
	 *         file failed.
	 * @throws ParseException Signals that an error has been reached
	 *         unexpectedly while parsing the data to be loaded.
	 */
	public YamlProperties( URL aUrl ) throws IOException, ParseException {
		super( new YamlPropertiesBuilder( aUrl ) );
	}

	/**
	 * Create an empty {@link YamlProperties} instance.
	 */
	protected YamlProperties() {
		super( new YamlPropertiesBuilder() );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean containsKey( Object aKey ) {
		return _properties.containsKey( aKey );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String get( Object aKey ) {
		return _properties.get( aKey );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public char getDelimiter() {
		return _properties.getDelimiter();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Class<String> getType() {
		return _properties.getType();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isEmpty() {
		return _properties.isEmpty();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Set<String> keySet() {
		return _properties.keySet();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Properties reload() throws IOException, ParseException {
		return _properties.reload();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Properties reload( ReloadMode aReloadMode ) throws IOException, ParseException {
		return _properties.reload( aReloadMode );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Properties retrieveFrom( String aFromPath ) {
		return _properties.retrieveFrom( aFromPath );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Properties retrieveTo( String aToPath ) {
		return _properties.retrieveTo( aToPath );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int size() {
		return _properties.size();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object toDataStructure( String aFromPath ) {
		return _properties.toDataStructure( aFromPath );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Collection<String> values() {
		return _properties.values();
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * The {@link YamlPropertiesFactory} represents a
	 * {@link ResourcePropertiesFactory} creating instances of type
	 * {@link YamlProperties}.
	 */
	public static class YamlPropertiesFactory implements ResourcePropertiesFactory {

		/**
		 * {@inheritDoc}
		 */
		@Override
		public String[] getFilenameSuffixes() {
			return new String[] { FilenameExtension.YAML.getFilenameSuffix() };
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public ResourceProperties toProperties( Class<?> aResourceClass, String aFilePath, ConfigLocator aConfigLocator ) throws IOException, ParseException {
			return new YamlProperties( aResourceClass, aFilePath, aConfigLocator );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public ResourceProperties toProperties( File aFile, ConfigLocator aConfigLocator ) throws IOException, ParseException {
			return new YamlProperties( aFile, aConfigLocator );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public ResourceProperties toProperties( InputStream aInputStream ) throws IOException, ParseException {
			return new YamlProperties( aInputStream );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public ResourceProperties toProperties( Map<?, ?> aProperties ) {
			return new YamlProperties( aProperties );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public ResourceProperties toProperties( Object aObj ) {
			return new YamlProperties( aObj );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public ResourceProperties toProperties( Properties aProperties ) {
			return new YamlProperties( aProperties );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public ResourcePropertiesBuilder toProperties( PropertiesBuilder aProperties ) {
			return new YamlPropertiesBuilder( aProperties );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public ResourceProperties toProperties( String aFilePath, ConfigLocator aConfigLocator ) throws IOException, ParseException {
			return new YamlProperties( aFilePath, aConfigLocator );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public ResourceProperties toProperties( URL aUrl ) throws IOException, ParseException {
			return new YamlProperties( aUrl );
		}
	}
}
