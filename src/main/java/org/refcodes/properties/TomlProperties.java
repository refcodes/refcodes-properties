// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.properties;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.ParseException;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.refcodes.data.Delimiter;
import org.refcodes.data.FilenameExtension;
import org.refcodes.runtime.ConfigLocator;

/**
 * Implementation of the {@link ResourceProperties} interface with support of so
 * called "TOML properties". For TOML properties, see
 * "https://en.wikipedia.org/wiki/TOML"
 */
public class TomlProperties extends AbstractResourceProperties {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Loads the TOML Properties from the given file's path.
	 *
	 * @param aResourceClass The class which's class loader is to take care of
	 *        loading the properties (from inside a JAR).
	 * @param aFilePath The file path of the class's resources from which to
	 *        load the properties.
	 * 
	 * @throws IOException thrown in case accessing or processing the properties
	 *         file failed.
	 * @throws ParseException Signals that an error has been reached
	 *         unexpectedly while parsing the data to be loaded.
	 */
	public TomlProperties( Class<?> aResourceClass, String aFilePath ) throws IOException, ParseException {
		super( new TomlPropertiesBuilder( aResourceClass, aFilePath ) );
	}

	/**
	 * Loads the TOML Properties from the given file's path.
	 *
	 * @param aResourceClass The class which's class loader is to take care of
	 *        loading the properties (from inside a JAR).
	 * @param aFilePath The file path of the class's resources from which to
	 *        load the properties.
	 * @param aDocumentMetrics Provides various metrics which may be tweaked
	 *        when marshaling or unmarshaling documents of various nations (such
	 *        as INI, XML, YAML, JSON, TOML, PROPERTIES, etc.).
	 * 
	 * @throws IOException thrown in case accessing or processing the properties
	 *         file failed.
	 * @throws ParseException Signals that an error has been reached
	 *         unexpectedly while parsing the data to be loaded.
	 */
	public TomlProperties( Class<?> aResourceClass, String aFilePath, DocumentMetrics aDocumentMetrics ) throws IOException, ParseException {
		super( new TomlPropertiesBuilder( aResourceClass, aFilePath, aDocumentMetrics ) );
	}

	/**
	 * Loads the TOML Properties from the given file's path. A provided
	 * {@link ConfigLocator} describes the locations to additional crawl for the
	 * desired file. Finally (if nothing else succeeds) the properties are
	 * loaded by the provided class's class loader which takes care of loading
	 * the properties (in case the file path is a relative path, also the
	 * absolute path with a prefixed path delimiter "/" is probed).
	 *
	 * @param aResourceClass The class which's class loader is to take care of
	 *        loading the properties (from inside a JAR).
	 * @param aFilePath The file path of the class's resources from which to
	 *        load the properties.
	 * @param aConfigLocator The {@link ConfigLocator} describes the locations
	 *        to additional crawl for the desired file.
	 * 
	 * @throws IOException thrown in case accessing or processing the properties
	 *         file failed.
	 * @throws ParseException Signals that an error has been reached
	 *         unexpectedly while parsing the data to be loaded.
	 */
	public TomlProperties( Class<?> aResourceClass, String aFilePath, ConfigLocator aConfigLocator ) throws IOException, ParseException {
		super( new TomlPropertiesBuilder( aResourceClass, aFilePath, aConfigLocator ) );
	}

	/**
	 * Loads the TOML Properties from the given file's path. A provided
	 * {@link ConfigLocator} describes the locations to additional crawl for the
	 * desired file. Finally (if nothing else succeeds) the properties are
	 * loaded by the provided class's class loader which takes care of loading
	 * the properties (in case the file path is a relative path, also the
	 * absolute path with a prefixed path delimiter "/" is probed).
	 *
	 * @param aResourceClass The class which's class loader is to take care of
	 *        loading the properties (from inside a JAR).
	 * @param aFilePath The file path of the class's resources from which to
	 *        load the properties.
	 * @param aConfigLocator The {@link ConfigLocator} describes the locations
	 *        to additional crawl for the desired file.
	 * @param aDocumentMetrics Provides various metrics which may be tweaked
	 *        when marshaling or unmarshaling documents of various nations (such
	 *        as INI, XML, YAML, JSON, TOML, PROPERTIES, etc.).
	 * 
	 * @throws IOException thrown in case accessing or processing the properties
	 *         file failed.
	 * @throws ParseException Signals that an error has been reached
	 *         unexpectedly while parsing the data to be loaded.
	 */
	public TomlProperties( Class<?> aResourceClass, String aFilePath, ConfigLocator aConfigLocator, DocumentMetrics aDocumentMetrics ) throws IOException, ParseException {
		super( new TomlPropertiesBuilder( aResourceClass, aFilePath, aConfigLocator, aDocumentMetrics ) );
	}

	/**
	 * Loads the TOML Properties from the given {@link File}.
	 *
	 * @param aFile The {@link File} from which to load the properties.
	 * 
	 * @throws IOException thrown in case accessing or processing the properties
	 *         file failed.
	 * @throws ParseException Signals that an error has been reached
	 *         unexpectedly while parsing the data to be loaded.
	 */
	public TomlProperties( File aFile ) throws IOException, ParseException {
		super( new TomlPropertiesBuilder( aFile ) );
	}

	/**
	 * Loads the TOML Properties from the given {@link File}.
	 *
	 * @param aFile The {@link File} from which to load the properties.
	 * @param aDocumentMetrics Provides various metrics which may be tweaked
	 *        when marshaling or unmarshaling documents of various nations (such
	 *        as INI, XML, YAML, JSON, TOML, PROPERTIES, etc.).
	 * 
	 * @throws IOException thrown in case accessing or processing the properties
	 *         file failed.
	 * @throws ParseException Signals that an error has been reached
	 *         unexpectedly while parsing the data to be loaded.
	 */
	public TomlProperties( File aFile, DocumentMetrics aDocumentMetrics ) throws IOException, ParseException {
		super( new TomlPropertiesBuilder( aFile, aDocumentMetrics ) );
	}

	/**
	 * Loads or seeks the TOML Properties from the given {@link File}. A
	 * provided {@link ConfigLocator} describes the locations to additional
	 * crawl for the desired file.
	 * 
	 * @param aFile The {@link File} from which to load the properties.
	 * @param aConfigLocator The {@link ConfigLocator} describes the locations
	 *        to additional crawl for the desired file.
	 * 
	 * @throws IOException thrown in case accessing or processing the properties
	 *         file failed.
	 * @throws ParseException Signals that an error has been reached
	 *         unexpectedly while parsing the data to be loaded.
	 */
	public TomlProperties( File aFile, ConfigLocator aConfigLocator ) throws IOException, ParseException {
		super( new TomlPropertiesBuilder( aFile, aConfigLocator ) );
	}

	/**
	 * Loads or seeks the TOML Properties from the given {@link File}. A
	 * provided {@link ConfigLocator} describes the locations to additional
	 * crawl for the desired file.
	 *
	 * @param aFile The {@link File} from which to load the properties.
	 * @param aConfigLocator The {@link ConfigLocator} describes the locations
	 *        to additional crawl for the desired file.
	 * @param aDocumentMetrics Provides various metrics which may be tweaked
	 *        when marshaling or unmarshaling documents of various nations (such
	 *        as INI, XML, YAML, JSON, TOML, PROPERTIES, etc.).
	 * 
	 * @throws IOException thrown in case accessing or processing the properties
	 *         file failed.
	 * @throws ParseException Signals that an error has been reached
	 *         unexpectedly while parsing the data to be loaded.
	 */
	public TomlProperties( File aFile, ConfigLocator aConfigLocator, DocumentMetrics aDocumentMetrics ) throws IOException, ParseException {
		super( new TomlPropertiesBuilder( aFile, aConfigLocator, aDocumentMetrics ) );
	}

	/**
	 * Reads the TOML Properties from the given {@link InputStream}.
	 *
	 * @param aInputStream The {@link InputStream} from which to read the
	 *        properties.
	 * 
	 * @throws IOException thrown in case accessing or processing the properties
	 *         file failed.
	 * @throws ParseException Signals that an error has been reached
	 *         unexpectedly while parsing the data to be loaded.
	 */
	public TomlProperties( InputStream aInputStream ) throws IOException, ParseException {
		super( new TomlPropertiesBuilder( aInputStream ) );
	}

	/**
	 * Reads the TOML Properties from the given {@link InputStream}.
	 *
	 * @param aInputStream The {@link InputStream} from which to read the
	 *        properties.
	 * @param aDocumentMetrics Provides various metrics which may be tweaked
	 *        when marshaling or unmarshaling documents of various nations (such
	 *        as INI, XML, YAML, JSON, TOML, PROPERTIES, etc.).
	 * 
	 * @throws IOException thrown in case accessing or processing the properties
	 *         file failed.
	 * @throws ParseException Signals that an error has been reached
	 *         unexpectedly while parsing the data to be loaded.
	 */
	public TomlProperties( InputStream aInputStream, DocumentMetrics aDocumentMetrics ) throws IOException, ParseException {
		super( new TomlPropertiesBuilder( aInputStream, aDocumentMetrics ) );
	}

	/**
	 * Create a {@link TomlProperties} instance containing the elements of the
	 * provided {@link Map} instance using the default path delimiter "/"
	 * ({@link Delimiter#PATH}) for the path declarations.
	 *
	 * @param aProperties the properties to be added.
	 */
	public TomlProperties( Map<?, ?> aProperties ) {
		super( new TomlPropertiesBuilder( aProperties ) );
	}

	/**
	 * Create a {@link TomlProperties} instance containing the elements as of
	 * {@link MutablePathMap#insert(Object)} using the default path delimiter
	 * "/" ({@link Delimiter#PATH}) for the path declarations: "Inspects the
	 * given object and adds all elements found in the given object. Elements of
	 * type {@link Map}, {@link Collection} and arrays are identified and
	 * handled as of their type: The path for each value in a {@link Map} is
	 * appended with its according key. The path for each value in a
	 * {@link Collection} or array is appended with its according index of
	 * occurrence (in case of a {@link List} or an array, its actual index). In
	 * case of reflection, the path for each member is appended with its
	 * according mamber's name. All elements (e.g. the members and values) are
	 * inspected recursively which results in the according paths of the
	 * terminating values."
	 *
	 * @param aObj The object from which the elements are to be added.
	 */
	public TomlProperties( Object aObj ) {
		super( new TomlPropertiesBuilder( aObj ) );
	}

	/**
	 * Create a {@link TomlProperties} instance containing the elements of the
	 * provided {@link Properties} instance using the default path delimiter "/"
	 * ({@link Delimiter#PATH}) for the path declarations.
	 *
	 * @param aProperties the properties to be added.
	 */
	public TomlProperties( Properties aProperties ) {
		super( new TomlPropertiesBuilder( aProperties ) );
	}

	/**
	 * Create a {@link TomlProperties} instance containing the elements of the
	 * provided {@link PropertiesBuilder} instance using the default path
	 * delimiter "/" ({@link Delimiter#PATH}) for the path declarations.
	 *
	 * @param aProperties the properties to be added.
	 */
	public TomlProperties( PropertiesBuilder aProperties ) {
		super( new TomlPropertiesBuilder( aProperties ) );
	}

	/**
	 * Loads the TOML Properties from the given file's path.
	 *
	 * @param aFilePath The path to the file from which to load the properties.
	 * 
	 * @throws IOException thrown in case accessing or processing the properties
	 *         file failed.
	 * @throws ParseException Signals that an error has been reached
	 *         unexpectedly while parsing the data to be loaded.
	 */
	public TomlProperties( String aFilePath ) throws IOException, ParseException {
		super( new TomlPropertiesBuilder( aFilePath ) );
	}

	/**
	 * Loads the TOML Properties from the given file's path.
	 *
	 * @param aFilePath The path to the file from which to load the properties.
	 * @param aDocumentMetrics Provides various metrics which may be tweaked
	 *        when marshaling or unmarshaling documents of various nations (such
	 *        as INI, XML, YAML, JSON, TOML, PROPERTIES, etc.).
	 * 
	 * @throws IOException thrown in case accessing or processing the properties
	 *         file failed.
	 * @throws ParseException Signals that an error has been reached
	 *         unexpectedly while parsing the data to be loaded.
	 */
	public TomlProperties( String aFilePath, DocumentMetrics aDocumentMetrics ) throws IOException, ParseException {
		super( new TomlPropertiesBuilder( aFilePath, aDocumentMetrics ) );
	}

	/**
	 * Loads the TOML Properties from the given file's path. A provided
	 * {@link ConfigLocator} describes the locations to additional crawl for the
	 * desired file.
	 * 
	 * @param aFilePath The path to the file from which to load the properties.
	 * @param aConfigLocator The {@link ConfigLocator} describes the locations
	 *        to additional crawl for the desired file.
	 * 
	 * @throws IOException thrown in case accessing or processing the properties
	 *         file failed.
	 * @throws ParseException Signals that an error has been reached
	 *         unexpectedly while parsing the data to be loaded.
	 */
	public TomlProperties( String aFilePath, ConfigLocator aConfigLocator ) throws IOException, ParseException {
		super( new TomlPropertiesBuilder( aFilePath, aConfigLocator ) );
	}

	/**
	 * Loads the TOML Properties from the given file's path. A provided
	 * {@link ConfigLocator} describes the locations to additional crawl for the
	 * desired file.
	 *
	 * @param aFilePath The path to the file from which to load the properties.
	 * @param aConfigLocator The {@link ConfigLocator} describes the locations
	 *        to additional crawl for the desired file.
	 * @param aDocumentMetrics Provides various metrics which may be tweaked
	 *        when marshaling or unmarshaling documents of various nations (such
	 *        as INI, XML, YAML, JSON, TOML, PROPERTIES, etc.).
	 * 
	 * @throws IOException thrown in case accessing or processing the properties
	 *         file failed.
	 * @throws ParseException Signals that an error has been reached
	 *         unexpectedly while parsing the data to be loaded.
	 */
	public TomlProperties( String aFilePath, ConfigLocator aConfigLocator, DocumentMetrics aDocumentMetrics ) throws IOException, ParseException {
		super( new TomlPropertiesBuilder( aFilePath, aConfigLocator, aDocumentMetrics ) );
	}

	/**
	 * Loads the TOML Properties from the given {@link URL}.
	 *
	 * @param aUrl The {@link URL} from which to read the properties.
	 * 
	 * @throws IOException thrown in case accessing or processing the properties
	 *         file failed.
	 * @throws ParseException Signals that an error has been reached
	 *         unexpectedly while parsing the data to be loaded.
	 */
	public TomlProperties( URL aUrl ) throws IOException, ParseException {
		super( new TomlPropertiesBuilder( aUrl ) );
	}

	/**
	 * Loads the TOML Properties from the given {@link URL}.
	 *
	 * @param aUrl The {@link URL} from which to read the properties.
	 * @param aDocumentMetrics Provides various metrics which may be tweaked
	 *        when marshaling or unmarshaling documents of various nations (such
	 *        as INI, XML, YAML, JSON, TOML, PROPERTIES, etc.).
	 * 
	 * @throws IOException thrown in case accessing or processing the properties
	 *         file failed.
	 * @throws ParseException Signals that an error has been reached
	 *         unexpectedly while parsing the data to be loaded.
	 */
	public TomlProperties( URL aUrl, DocumentMetrics aDocumentMetrics ) throws IOException, ParseException {
		super( new TomlPropertiesBuilder( aUrl, aDocumentMetrics ) );
	}

	/**
	 * Create an empty {@link TomlProperties} instance.
	 */
	protected TomlProperties() {
		super( new TomlPropertiesBuilder() );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean containsKey( Object aKey ) {
		return _properties.containsKey( aKey );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String get( Object aKey ) {
		return _properties.get( aKey );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public char getDelimiter() {
		return _properties.getDelimiter();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Class<String> getType() {
		return _properties.getType();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isEmpty() {
		return _properties.isEmpty();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Set<String> keySet() {
		return _properties.keySet();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Properties reload() throws IOException, ParseException {
		return _properties.reload();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Properties reload( ReloadMode aReloadMode ) throws IOException, ParseException {
		return _properties.reload( aReloadMode );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Properties retrieveFrom( String aFromPath ) {
		return _properties.retrieveFrom( aFromPath );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Properties retrieveTo( String aToPath ) {
		return _properties.retrieveTo( aToPath );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int size() {
		return _properties.size();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object toDataStructure( String aFromPath ) {
		return _properties.toDataStructure( aFromPath );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Collection<String> values() {
		return _properties.values();
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * The {@link TomlPropertiesFactory} represents a
	 * {@link ResourcePropertiesFactory} creating instances of type
	 * {@link TomlProperties}.
	 */
	public static class TomlPropertiesFactory implements ResourcePropertiesFactory {

		// /////////////////////////////////////////////////////////////////////
		// VARIABLES:
		// /////////////////////////////////////////////////////////////////////

		private final DocumentMetrics _documentMetrics;

		// /////////////////////////////////////////////////////////////////////
		// CONSTRUCTORS:
		// /////////////////////////////////////////////////////////////////////

		/**
		 * Constructs an {@link TomlPropertiesFactory} for creating
		 * {@link XmlPropertiesBuilder} instances.
		 */
		public TomlPropertiesFactory() {
			this( DocumentNotation.DEFAULT );
		}

		/**
		 * Constructs an {@link TomlPropertiesFactory} for creating
		 * {@link XmlPropertiesBuilder} instances.
		 *
		 * @param aDocumentMetrics Provides various metrics which may be tweaked
	 *        when marshaling or unmarshaling documents of various nations (such
	 *        as INI, XML, YAML, JSON, TOML, PROPERTIES, etc.).
		 */
		public TomlPropertiesFactory( DocumentMetrics aDocumentMetrics ) {
			_documentMetrics = aDocumentMetrics;
		}

		// /////////////////////////////////////////////////////////////////////
		// METHODS:
		// /////////////////////////////////////////////////////////////////////

		/**
		 * {@inheritDoc}
		 */
		@Override
		public String[] getFilenameSuffixes() {
			return new String[] { FilenameExtension.TOML.getFilenameSuffix(), FilenameExtension.INI.getFilenameSuffix() };
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public ResourceProperties toProperties( Class<?> aResourceClass, String aFilePath, ConfigLocator aConfigLocator ) throws IOException, ParseException {
			return new TomlProperties( aResourceClass, aFilePath, aConfigLocator, _documentMetrics );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public ResourceProperties toProperties( File aFile, ConfigLocator aConfigLocator ) throws IOException, ParseException {
			return new TomlProperties( aFile, aConfigLocator, _documentMetrics );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public ResourceProperties toProperties( InputStream aInputStream ) throws IOException, ParseException {
			return new TomlProperties( aInputStream, _documentMetrics );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public ResourceProperties toProperties( Map<?, ?> aProperties ) {
			return new TomlProperties( aProperties );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public ResourceProperties toProperties( Object aObj ) {
			return new TomlProperties( aObj );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public ResourceProperties toProperties( Properties aProperties ) {
			return new TomlProperties( aProperties );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public ResourceProperties toProperties( PropertiesBuilder aProperties ) {
			return new TomlProperties( aProperties );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public ResourceProperties toProperties( String aFilePath, ConfigLocator aConfigLocator ) throws IOException, ParseException {
			return new TomlProperties( aFilePath, aConfigLocator, _documentMetrics );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public ResourceProperties toProperties( URL aUrl ) throws IOException, ParseException {
			return new TomlProperties( aUrl, _documentMetrics );
		}
	}
}
