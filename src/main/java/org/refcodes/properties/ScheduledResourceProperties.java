// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.properties;

import org.refcodes.component.Startable;
import org.refcodes.component.Stoppable;
import org.refcodes.mixin.Disposable;

/**
 * The {@link ScheduledResourceProperties} enrich {@link ResourceProperties}
 * with scheduling functionality whereby the properties are being automatically
 * reloaded periodically via {@link #reload(ReloadMode)}. The scheduler can be
 * stopped via {@link #stop()} and started again via {@link #start()}.
 */
public interface ScheduledResourceProperties extends ResourceProperties, Startable, Stoppable, Disposable {

	/**
	 * The interface {@link ScheduledMuableResourceProperties} defines "dirty"
	 * methods allowing to modify ("mutate") the
	 * {@link ScheduledResourceProperties}.
	 */
	public interface ScheduledMuableResourceProperties extends ScheduledResourceProperties, MutableResoureProperties {}

	/**
	 * The interface {@link ScheduledResourcePropertiesBuilder} defines builder
	 * functionality on top of the {@link ScheduledMuableResourceProperties}.
	 */
	public interface ScheduledResourcePropertiesBuilder extends ScheduledMuableResourceProperties, ResourcePropertiesBuilder {}
}
