// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany and licensed
// under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.properties;

import org.refcodes.data.Delimiters;

/**
 * The {@link DocumentNotation} enumeration provides various predefined
 * {@link DocumentMetrics} variants.
 *
 */
public enum DocumentNotation implements DocumentMetrics {

	/**
	 * Represents default {@link DocumentMetrics} with metrics being document's
	 * envelope disabled, document's array index disabled and delimiters set to
	 * {@link Delimiters#PROPERTIES}.
	 */
	DEFAULT(false, false, Delimiters.PROPERTIES.getChars()),

	/**
	 * Represents {@link DocumentMetrics} with metrics being document's envelope
	 * enabled, document's array index enabled and delimiters set to
	 * {@link Delimiters#PROPERTIES}.
	 */
	ENVELOPE_ARRAY_INDEX_PROPERTIES_DELIMITERS(true, true, Delimiters.PROPERTIES.getChars()),

	/**
	 * Represents {@link DocumentMetrics} with metrics being document's envelope
	 * disabled, document's array index enabled and delimiters set to
	 * {@link Delimiters#PROPERTIES}.
	 */
	NO_ENVELOPE_ARRAY_INDEX_PROPERTIES_DELIMITERS(false, true, Delimiters.PROPERTIES.getChars()),

	/**
	 * Represents {@link DocumentMetrics} with metrics being document's envelope
	 * enabled, document's array index disabled and delimiters set to
	 * {@link Delimiters#PROPERTIES}.
	 */
	ENVELOPE_NO_ARRAY_INDEX_PROPERTIES_DELIMITERS(true, false, Delimiters.PROPERTIES.getChars()),

	/**
	 * Represents {@link DocumentMetrics} with metrics being document's envelope
	 * disabled, document's array index disabled and delimiters set to
	 * {@link Delimiters#PROPERTIES}.
	 */
	NO_ENVELOPE_NO_ARRAY_INDEX_PROPERTIES_DELIMITERS(false, false, Delimiters.PROPERTIES.getChars()),

	/**
	 * Represents {@link DocumentMetrics} with metrics being document's envelope
	 * enabled, document's array index enabled and delimiters set to
	 * {@link Delimiters#PATHS}.
	 */
	ENVELOPE_ARRAY_INDEX_PATHS_DELIMITERS(true, true, Delimiters.PATHS.getChars()),

	/**
	 * Represents {@link DocumentMetrics} with metrics being document's envelope
	 * disabled, document's array index enabled and delimiters set to
	 * {@link Delimiters#PATHS}.
	 */
	NO_ENVELOPE_ARRAY_INDEX_PATHS_DELIMITERS(false, true, Delimiters.PATHS.getChars()),

	/**
	 * Represents {@link DocumentMetrics} with metrics being document's envelope
	 * enabled, document's array index disabled and delimiters set to
	 * {@link Delimiters#PATHS}.
	 */
	ENVELOPE_NO_ARRAY_INDEX_PATHS_DELIMITERS(true, false, Delimiters.PATHS.getChars()),

	/**
	 * Represents {@link DocumentMetrics} with metrics being document's envelope
	 * disabled, document's array index disabled and delimiters set to
	 * {@link Delimiters#PATHS}.
	 */
	NO_ENVELOPE_NO_ARRAY_INDEX_PATHS_DELIMITERS(false, false, Delimiters.PATHS.getChars());

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final boolean _isArrayIndex;
	private final boolean _isEnvelope;
	private final char[] _delimiters;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	private DocumentNotation( boolean aEnvelope, boolean aArrayIndex, char... aDelimiters ) {
		_isEnvelope = aEnvelope;
		_isArrayIndex = aArrayIndex;
		_delimiters = aDelimiters;
	}

	// /////////////////////////////////////////////////////////////////////////
	// INJECTION:
	// /////////////////////////////////////////////////////////////////////////

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isArrayIndex() {
		return _isArrayIndex;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isEnvelope() {
		return _isEnvelope;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public char[] getDelimiters() {
		return _delimiters;
	}
}
