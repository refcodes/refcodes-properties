// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.properties;

import java.io.IOException;
import java.text.ParseException;

/**
 * Decorates the provided {@link ResourceProperties} and delegates method calls
 * to them {@link ResourceProperties}.
 * 
 * @param <T> The actual (sub-) type of the {@link Properties} to be decorated.
 */
public abstract class AbstractResourcePropertiesDecorator<T extends ResourceProperties> extends AbstractPropertiesDecorator<T> implements ResourceProperties {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Decorates the provided {@link ResourceProperties} with additional
	 * behavior or functionality. Changes applied to the provided
	 * {@link ResourceProperties} affect the decorator.
	 * 
	 * @param aProperties The {@link ResourceProperties} to be decorated.
	 */
	public AbstractResourcePropertiesDecorator( T aProperties ) {
		super( aProperties );
	}

	/**
	 * Make sure to set the getProperties() member variable!.
	 */
	protected AbstractResourcePropertiesDecorator() {}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Properties reload() throws IOException, ParseException {
		return getProperties().reload();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Properties reload( ReloadMode aReloadMode ) throws IOException, ParseException {
		return getProperties().reload( aReloadMode );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toSerialized() {
		return getProperties().toSerialized();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toSerialized( String aComment, char aDelimiter ) {
		return getProperties().toSerialized( aComment, aDelimiter );
	}
}
