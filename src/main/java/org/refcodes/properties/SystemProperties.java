// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.properties;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.refcodes.data.Delimiter;
import org.refcodes.struct.StructureUtility;

/**
 * Extension of the {@link Properties} type overwriting methods in order to
 * access system properties as passed via the "-Dkey=value" when launching the
 * JVM (e.g.java -Dconsole.width=220) The keys are transformed to a system
 * properties by removing a prefixed "/" path delimiter (as of
 * {@link #getDelimiter()} and converting all other path delimiters "/" to the
 * system property's (de facto standard) separator ".". If accessing failed,
 * then the lower case version of the so transformed key is probed. Accessing a
 * system property "console.width" would be done with the path "/console/width".
 */
public class SystemProperties implements Properties {

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * The key is transformed to an system property variable by removing a
	 * prefixed "/" path delimiter (as of {@link #getDelimiter()} and converting
	 * all other path delimiters "/" to the system property's (de facto
	 * standard) separator ".". If accessing failed, then the lower case version
	 * of the so transformed key is probed. {@inheritDoc}
	 */
	@Override
	public boolean containsKey( Object aKey ) {
		return get( aKey ) != null;
	}

	/**
	 * The key is transformed to a system property by removing a prefixed "/"
	 * path delimiter (as of {@link #getDelimiter()} and converting all other
	 * path delimiters "/" to the system property's (de facto standard)
	 * separator ".". If accessing failed, then the lower case version of the so
	 * transformed key is probed. {@inheritDoc}
	 */
	@Override
	public String get( Object aKey ) {
		String theValue = null;
		String theKey = aKey != null ? aKey.toString() : null;
		if ( theKey != null ) {
			theKey = NormalizedPropertiesDecorator.fromNormalized( theKey, getDelimiter(), Delimiter.NAMESPACE.getChar() );
			theValue = System.getProperty( theKey );
			if ( theValue == null ) {
				theValue = System.getProperty( theKey.toLowerCase() );
				if ( theValue == null ) {
					String eKey;
					for ( Object eObj : System.getProperties().keySet() ) {
						eKey = eObj.toString();
						if ( eKey.equalsIgnoreCase( theKey ) ) {
							return System.getProperty( eKey );
						}
					}
				}
			}
		}
		return theValue;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isEmpty() {
		return System.getProperties().isEmpty();
	}

	/**
	 * The keys are transformed to a path by prefixing a "/" path delimiter (as
	 * of {@link #getDelimiter()} and converting all other system property's (de
	 * facto standard) separators "." to the path delimiter "/". {@inheritDoc}
	 */
	@Override
	public Set<String> keySet() {
		final Set<String> theKeys = new HashSet<>();
		final java.util.Properties theProperties = System.getProperties();
		for ( Object eKey : theProperties.keySet() ) {
			theKeys.add( NormalizedPropertiesDecorator.toNormalized( eKey.toString(), getDelimiter(), Delimiter.NAMESPACE.getChar() ) );
		}
		return theKeys;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Properties retrieveFrom( String aFromPath ) {
		final PropertiesBuilder theToPathMap = new PropertiesBuilderImpl();
		StructureUtility.retrieveFrom( this, aFromPath, theToPathMap );
		return theToPathMap;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Properties retrieveTo( String aToPath ) {
		final PropertiesBuilder theToPathMap = new PropertiesBuilderImpl();
		StructureUtility.retrieveTo( this, aToPath, theToPathMap );
		return theToPathMap;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int size() {
		return System.getProperties().size();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object toDataStructure( String aFromPath ) {
		return StructureUtility.toDataStructure( this, aFromPath );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Collection<String> values() {
		final List<String> theValues = new ArrayList<>();
		final java.util.Properties theProperties = System.getProperties();
		for ( Object eValue : theProperties.values() ) {
			theValues.add( eValue.toString() );
		}
		return theValues;
	}
}
