// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.properties;

import java.util.Map;

/**
 * Base class for various {@link ResourceProperties} implementations using a
 * {@link ResourcePropertiesBuilder} instance under the hood.
 */
public abstract class AbstractResourceProperties implements ResourceProperties {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	protected ResourcePropertiesBuilder _properties;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs the {@link AbstractResourceProperties} instance with the given
	 * {@link ResourcePropertiesBuilder}.
	 * 
	 * @param aProperties The {@link ResourcePropertiesBuilder} to be used under
	 *        the hood.
	 */
	public AbstractResourceProperties( ResourcePropertiesBuilder aProperties ) {
		_properties = aProperties;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Map<String, String> toDump() {
		return _properties.toDump();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Map<String, String> toDump( Map<String, String> aDump ) {
		return _properties.toDump( aDump );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toPrintable() {
		return _properties.toPrintable();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toSerialized() {
		return _properties.toSerialized();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toSerialized( String aComment, char aDelimiter ) {
		return _properties.toSerialized( aComment, aDelimiter );
	}
}
