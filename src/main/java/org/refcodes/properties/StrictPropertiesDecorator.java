// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.properties;

import org.refcodes.struct.KeyNotFoundRuntimeException;

/**
 * The {@link StrictPropertiesDecorator} type decorates a {@link Properties}
 * instance with all the getters to throw a {@link KeyNotFoundRuntimeException}
 * instead of returning null in case the key was not found.
 */
public class StrictPropertiesDecorator extends AbstractPropertiesDecorator<Properties> implements StrictProperties {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Decorates the provided {@link Properties} with additional behavior with
	 * all the getters to throw a {@link KeyNotFoundRuntimeException} instead of
	 * returning null in case the key was not found . Changes applied to the
	 * provided {@link Properties} affect the decorator.
	 * 
	 * @param aProperties The {@link Properties} to be decorated.
	 */
	public StrictPropertiesDecorator( Properties aProperties ) {
		super( aProperties );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String get( Object aKey ) {
		if ( !containsKey( aKey ) ) {
			throw new KeyNotFoundRuntimeException( aKey.toString(), "There is no such element with key <" + aKey + "> found in this instance!" );
		}
		return super.get( aKey );
	}
}
