// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.properties;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.ParseException;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.refcodes.data.Delimiter;
import org.refcodes.data.FilenameExtension;
import org.refcodes.exception.UnmarshalException;
import org.refcodes.properties.ResourcePropertiesFactory.ResourcePropertiesBuilderFactory;
import org.refcodes.runtime.ConfigLocator;
import org.refcodes.runtime.SystemProperty;
import org.refcodes.struct.CanonicalMapBuilderImpl;
import org.refcodes.struct.ext.factory.CanonicalMapFactory;
import org.refcodes.struct.ext.factory.DocumentProperty;
import org.refcodes.struct.ext.factory.XmlCanonicalMapFactory;

/**
 * Implementation of the {@link ResourcePropertiesBuilder} interface with
 * support of so called "XML properties" (or just "properties"). For XML, see
 * "https://en.wikipedia.org/wiki/XML".
 * 
 * By default the (XML) document's root element is omitted. This behavior can
 * either be changed by calling the according constructor or by the system
 * property {@link SystemProperty#DOCUMENT_ENVELOPE}. When setting XML root
 * elelemt preservation to <code>true</code> then unmarshaling (XML) documents
 * will preserve the preserve the root element (envelope). As an (XML) document
 * requires a root element, the root element often is provided merely as of
 * syntactic reasons and must be omitted as of semantic reasons. Unmarshaling
 * functionality therefore by default skips the root elelemt, as this is
 * considered merely to serve as an envelope. This behavior can be overridden by
 * setting the XML root elelemt preservation to <code>true</code>.
 */
public class XmlPropertiesBuilder extends AbstractResourcePropertiesBuilder {

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Create an empty {@link XmlPropertiesBuilder} instance using the default
	 * path delimiter "/" ({@link Delimiter#PATH}) for the path declarations.
	 */
	public XmlPropertiesBuilder() {
		this( DocumentNotation.DEFAULT );
	}

	/**
	 * Create an empty {@link XmlPropertiesBuilder} instance using the default
	 * path delimiter "/" ({@link Delimiter#PATH}) for the path declarations.
	 * 
	 * @param aDocumentMetrics Provides various metrics which may be tweaked
	 *        when marshaling or unmarshaling documents of various nations (such
	 *        as INI, XML, YAML, JSON, TOML, PROPERTIES, etc.).
	 */
	public XmlPropertiesBuilder( DocumentMetrics aDocumentMetrics ) {
		super( aDocumentMetrics );
	}

	/**
	 * Loads the XML properties from the given file's path.
	 *
	 * @param aResourceClass The class which's class loader is to take care of
	 *        loading the properties (from inside a JAR).
	 * @param aFilePath The file path of the class's resources from which to
	 *        load the properties.
	 * 
	 * @throws IOException thrown in case accessing or processing the properties
	 *         file failed.
	 * @throws ParseException Signals that an error has been reached
	 *         unexpectedly while parsing the data to be loaded.
	 */
	public XmlPropertiesBuilder( Class<?> aResourceClass, String aFilePath ) throws IOException, ParseException {
		this( aResourceClass, aFilePath, DocumentNotation.DEFAULT );
	}

	/**
	 * Loads the XML properties from the given file's path.
	 *
	 * @param aResourceClass The class which's class loader is to take care of
	 *        loading the properties (from inside a JAR).
	 * @param aFilePath The file path of the class's resources from which to
	 *        load the properties.
	 * @param aDocumentMetrics Provides various metrics which may be tweaked
	 *        when marshaling or unmarshaling documents of various nations (such
	 *        as INI, XML, YAML, JSON, TOML, PROPERTIES, etc.).
	 * 
	 * @throws IOException thrown in case accessing or processing the properties
	 *         file failed.
	 * @throws ParseException Signals that an error has been reached
	 *         unexpectedly while parsing the data to be loaded.
	 */
	public XmlPropertiesBuilder( Class<?> aResourceClass, String aFilePath, DocumentMetrics aDocumentMetrics ) throws IOException, ParseException {
		this( aDocumentMetrics );
		loadFrom( aResourceClass, aFilePath );
	}

	/**
	 * Loads the XML properties from the given file's path. A provided
	 * {@link ConfigLocator} describes the locations to additional crawl for the
	 * desired file. Finally (if nothing else succeeds) the properties are
	 * loaded by the provided class's class loader which takes care of loading
	 * the properties (in case the file path is a relative path, also the
	 * absolute path with a prefixed path delimiter "/" is probed).
	 *
	 * @param aResourceClass The class which's class loader is to take care of
	 *        loading the properties (from inside a JAR).
	 * @param aFilePath The file path of the class's resources from which to
	 *        load the properties.
	 * @param aConfigLocator The {@link ConfigLocator} describes the locations
	 *        to additional crawl for the desired file.
	 * 
	 * @throws IOException thrown in case accessing or processing the properties
	 *         file failed.
	 * @throws ParseException Signals that an error has been reached
	 *         unexpectedly while parsing the data to be loaded.
	 */
	public XmlPropertiesBuilder( Class<?> aResourceClass, String aFilePath, ConfigLocator aConfigLocator ) throws IOException, ParseException {
		this( aResourceClass, aFilePath, aConfigLocator, DocumentNotation.DEFAULT );
	}

	/**
	 * Loads the XML properties from the given file's path. A provided
	 * {@link ConfigLocator} describes the locations to additional crawl for the
	 * desired file. Finally (if nothing else succeeds) the properties are
	 * loaded by the provided class's class loader which takes care of loading
	 * the properties (in case the file path is a relative path, also the
	 * absolute path with a prefixed path delimiter "/" is probed).
	 *
	 * @param aResourceClass The class which's class loader is to take care of
	 *        loading the properties (from inside a JAR).
	 * @param aFilePath The file path of the class's resources from which to
	 *        load the properties.
	 * @param aConfigLocator The {@link ConfigLocator} describes the locations
	 *        to additional crawl for the desired file.
	 * @param aDocumentMetrics Provides various metrics which may be tweaked
	 *        when marshaling or unmarshaling documents of various nations (such
	 *        as INI, XML, YAML, JSON, TOML, PROPERTIES, etc.).
	 * 
	 * @throws IOException thrown in case accessing or processing the properties
	 *         file failed.
	 * @throws ParseException Signals that an error has been reached
	 *         unexpectedly while parsing the data to be loaded.
	 */
	public XmlPropertiesBuilder( Class<?> aResourceClass, String aFilePath, ConfigLocator aConfigLocator, DocumentMetrics aDocumentMetrics ) throws IOException, ParseException {
		this( aDocumentMetrics );
		seekFrom( aResourceClass, aFilePath, aConfigLocator );
	}

	/**
	 * Loads the XML properties from the given {@link File}.
	 *
	 * @param aFile The {@link File} from which to load the properties.
	 * 
	 * @throws IOException thrown in case accessing or processing the properties
	 *         file failed.
	 * @throws ParseException Signals that an error has been reached
	 *         unexpectedly while parsing the data to be loaded.
	 */
	public XmlPropertiesBuilder( File aFile ) throws IOException, ParseException {
		this( aFile, DocumentNotation.DEFAULT );
	}

	/**
	 * Loads the XML properties from the given {@link File}.
	 *
	 * @param aFile The {@link File} from which to load the properties.
	 * @param aDocumentMetrics Provides various metrics which may be tweaked
	 *        when marshaling or unmarshaling documents of various nations (such
	 *        as INI, XML, YAML, JSON, TOML, PROPERTIES, etc.).
	 * 
	 * @throws IOException thrown in case accessing or processing the properties
	 *         file failed.
	 * @throws ParseException Signals that an error has been reached
	 *         unexpectedly while parsing the data to be loaded.
	 */
	public XmlPropertiesBuilder( File aFile, DocumentMetrics aDocumentMetrics ) throws IOException, ParseException {
		this( aDocumentMetrics );
		loadFrom( aFile );
	}

	/**
	 * Loads or seeks the XML properties from the given {@link File}. A provided
	 * {@link ConfigLocator} describes the locations to additional crawl for the
	 * desired file.
	 * 
	 * @param aFile The {@link File} from which to load the properties.
	 * @param aConfigLocator The {@link ConfigLocator} describes the locations
	 *        to additional crawl for the desired file.
	 * 
	 * @throws IOException thrown in case accessing or processing the properties
	 *         file failed.
	 * @throws ParseException Signals that an error has been reached
	 *         unexpectedly while parsing the data to be loaded.
	 */
	public XmlPropertiesBuilder( File aFile, ConfigLocator aConfigLocator ) throws IOException, ParseException {
		this( aFile, aConfigLocator, DocumentNotation.DEFAULT );
	}

	/**
	 * Loads or seeks the XML properties from the given {@link File}. A provided
	 * {@link ConfigLocator} describes the locations to additional crawl for the
	 * desired file.
	 *
	 * @param aFile The {@link File} from which to load the properties.
	 * @param aConfigLocator The {@link ConfigLocator} describes the locations
	 *        to additional crawl for the desired file.
	 * @param aDocumentMetrics Provides various metrics which may be tweaked
	 *        when marshaling or unmarshaling documents of various nations (such
	 *        as INI, XML, YAML, JSON, TOML, PROPERTIES, etc.).
	 * 
	 * @throws IOException thrown in case accessing or processing the properties
	 *         file failed.
	 * @throws ParseException Signals that an error has been reached
	 *         unexpectedly while parsing the data to be loaded.
	 */
	public XmlPropertiesBuilder( File aFile, ConfigLocator aConfigLocator, DocumentMetrics aDocumentMetrics ) throws IOException, ParseException {
		this( aDocumentMetrics );
		seekFrom( aFile, aConfigLocator );
	}

	/**
	 * Reads the XML properties from the given {@link InputStream}.
	 *
	 * @param aInputStream The {@link InputStream} from which to read the
	 *        properties.
	 * 
	 * @throws IOException thrown in case accessing or processing the properties
	 *         file failed.
	 * @throws ParseException Signals that an error has been reached
	 *         unexpectedly while parsing the data to be loaded.
	 */
	public XmlPropertiesBuilder( InputStream aInputStream ) throws IOException, ParseException {
		this( aInputStream, DocumentNotation.DEFAULT );
	}

	/**
	 * Reads the XML properties from the given {@link InputStream}.
	 *
	 * @param aInputStream The {@link InputStream} from which to read the
	 *        properties.
	 * @param aDocumentMetrics Provides various metrics which may be tweaked
	 *        when marshaling or unmarshaling documents of various nations (such
	 *        as INI, XML, YAML, JSON, TOML, PROPERTIES, etc.).
	 * 
	 * @throws IOException thrown in case accessing or processing the properties
	 *         file failed.
	 * @throws ParseException Signals that an error has been reached
	 *         unexpectedly while parsing the data to be loaded.
	 */
	public XmlPropertiesBuilder( InputStream aInputStream, DocumentMetrics aDocumentMetrics ) throws IOException, ParseException {
		this( aDocumentMetrics );
		loadFrom( aInputStream );
	}

	/**
	 * Create a {@link XmlPropertiesBuilder} instance containing the elements of
	 * the provided {@link Map} instance using the default path delimiter "/"
	 * ({@link Delimiter#PATH}) for the path declarations.
	 *
	 * @param aProperties the properties to be added.
	 */
	public XmlPropertiesBuilder( Map<?, ?> aProperties ) {
		this( aProperties, DocumentNotation.DEFAULT );
	}

	/**
	 * Create a {@link XmlPropertiesBuilder} instance containing the elements of
	 * the provided {@link Map} instance using the default path delimiter "/"
	 * ({@link Delimiter#PATH}) for the path declarations.
	 *
	 * @param aProperties the properties to be added.
	 * @param aDocumentMetrics Provides various metrics which may be tweaked
	 *        when marshaling or unmarshaling documents of various nations (such
	 *        as INI, XML, YAML, JSON, TOML, PROPERTIES, etc.).
	 */
	public XmlPropertiesBuilder( Map<?, ?> aProperties, DocumentMetrics aDocumentMetrics ) {
		super( aProperties, aDocumentMetrics );
	}

	/**
	 * Create a {@link XmlPropertiesBuilder} instance containing the elements as
	 * of {@link MutablePathMap#insert(Object)} using the default path delimiter
	 * "/" ({@link Delimiter#PATH}) for the path declarations: "Inspects the
	 * given object and adds all elements found in the given object. Elements of
	 * type {@link Map}, {@link Collection} and arrays are identified and
	 * handled as of their type: The path for each value in a {@link Map} is
	 * appended with its according key. The path for each value in a
	 * {@link Collection} or array is appended with its according index of
	 * occurrence (in case of a {@link List} or an array, its actual index). In
	 * case of reflection, the path for each member is appended with its
	 * according mamber's name. All elements (e.g. the members and values) are
	 * inspected recursively which results in the according paths of the
	 * terminating values."
	 *
	 * @param aObj The object from which the elements are to be added.
	 */
	public XmlPropertiesBuilder( Object aObj ) {
		this( aObj, DocumentNotation.DEFAULT );
	}

	/**
	 * Create a {@link XmlPropertiesBuilder} instance containing the elements as
	 * of {@link MutablePathMap#insert(Object)} using the default path delimiter
	 * "/" ({@link Delimiter#PATH}) for the path declarations: "Inspects the
	 * given object and adds all elements found in the given object. Elements of
	 * type {@link Map}, {@link Collection} and arrays are identified and
	 * handled as of their type: The path for each value in a {@link Map} is
	 * appended with its according key. The path for each value in a
	 * {@link Collection} or array is appended with its according index of
	 * occurrence (in case of a {@link List} or an array, its actual index). In
	 * case of reflection, the path for each member is appended with its
	 * according mamber's name. All elements (e.g. the members and values) are
	 * inspected recursively which results in the according paths of the
	 * terminating values."
	 *
	 * @param aObj The object from which the elements are to be added.
	 * @param aDocumentMetrics Provides various metrics which may be tweaked
	 *        when marshaling or unmarshaling documents of various nations (such
	 *        as INI, XML, YAML, JSON, TOML, PROPERTIES, etc.).
	 */
	public XmlPropertiesBuilder( Object aObj, DocumentMetrics aDocumentMetrics ) {
		super( aObj, aDocumentMetrics );
	}

	/**
	 * Create a {@link XmlPropertiesBuilder} instance containing the elements of
	 * the provided {@link Properties} instance using the default path delimiter
	 * "/" ({@link Delimiter#PATH}) for the path declarations.
	 *
	 * @param aProperties the properties to be added.
	 */
	public XmlPropertiesBuilder( Properties aProperties ) {
		this( aProperties, DocumentNotation.DEFAULT );
	}

	/**
	 * Create a {@link XmlPropertiesBuilder} instance containing the elements of
	 * the provided {@link Properties} instance using the default path delimiter
	 * "/" ({@link Delimiter#PATH}) for the path declarations.
	 *
	 * @param aProperties the properties to be added.
	 * @param aDocumentMetrics Provides various metrics which may be tweaked
	 *        when marshaling or unmarshaling documents of various nations (such
	 *        as INI, XML, YAML, JSON, TOML, PROPERTIES, etc.).
	 */
	public XmlPropertiesBuilder( Properties aProperties, DocumentMetrics aDocumentMetrics ) {
		super( aProperties, aDocumentMetrics );
	}

	/**
	 * Create a {@link XmlPropertiesBuilder} instance containing the elements of
	 * the provided {@link PropertiesBuilder} instance using the default path
	 * delimiter "/" ({@link Delimiter#PATH}) for the path declarations.
	 *
	 * @param aProperties the properties to be added.
	 */
	public XmlPropertiesBuilder( PropertiesBuilder aProperties ) {
		this( aProperties, DocumentNotation.DEFAULT );
	}

	/**
	 * Create a {@link XmlPropertiesBuilder} instance containing the elements of
	 * the provided {@link PropertiesBuilder} instance using the default path
	 * delimiter "/" ({@link Delimiter#PATH}) for the path declarations.
	 *
	 * @param aProperties the properties to be added.
	 * @param aDocumentMetrics Provides various metrics which may be tweaked
	 *        when marshaling or unmarshaling documents of various nations (such
	 *        as INI, XML, YAML, JSON, TOML, PROPERTIES, etc.).
	 */
	public XmlPropertiesBuilder( PropertiesBuilder aProperties, DocumentMetrics aDocumentMetrics ) {
		super( aProperties, aDocumentMetrics );
	}

	/**
	 * Loads the XML properties from the given file's path.
	 *
	 * @param aFilePath The path to the file from which to load the properties.
	 * 
	 * @throws IOException thrown in case accessing or processing the properties
	 *         file failed.
	 * @throws ParseException Signals that an error has been reached
	 *         unexpectedly while parsing the data to be loaded.
	 */
	public XmlPropertiesBuilder( String aFilePath ) throws IOException, ParseException {
		this( aFilePath, DocumentNotation.DEFAULT );
	}

	/**
	 * Loads the XML properties from the given file's path.
	 *
	 * @param aFilePath The path to the file from which to load the properties.
	 * @param aDocumentMetrics Provides various metrics which may be tweaked
	 *        when marshaling or unmarshaling documents of various nations (such
	 *        as INI, XML, YAML, JSON, TOML, PROPERTIES, etc.).
	 * 
	 * @throws IOException thrown in case accessing or processing the properties
	 *         file failed.
	 * @throws ParseException Signals that an error has been reached
	 *         unexpectedly while parsing the data to be loaded.
	 */
	public XmlPropertiesBuilder( String aFilePath, DocumentMetrics aDocumentMetrics ) throws IOException, ParseException {
		this( aDocumentMetrics );
		loadFrom( aFilePath );
	}

	/**
	 * Loads the XML properties from the given file's path. A provided
	 * {@link ConfigLocator} describes the locations to additional crawl for the
	 * desired file.
	 * 
	 * @param aFilePath The path to the file from which to load the properties.
	 * @param aConfigLocator The {@link ConfigLocator} describes the locations
	 *        to additional crawl for the desired file.
	 * 
	 * @throws IOException thrown in case accessing or processing the properties
	 *         file failed.
	 * @throws ParseException Signals that an error has been reached
	 *         unexpectedly while parsing the data to be loaded.
	 */
	public XmlPropertiesBuilder( String aFilePath, ConfigLocator aConfigLocator ) throws IOException, ParseException {
		this( aFilePath, aConfigLocator, DocumentNotation.DEFAULT );
	}

	/**
	 * Loads the XML properties from the given file's path. A provided
	 * {@link ConfigLocator} describes the locations to additional crawl for the
	 * desired file.
	 *
	 * @param aFilePath The path to the file from which to load the properties.
	 * @param aConfigLocator The {@link ConfigLocator} describes the locations
	 *        to additional crawl for the desired file.
	 * @param aDocumentMetrics Provides various metrics which may be tweaked
	 *        when marshaling or unmarshaling documents of various nations (such
	 *        as INI, XML, YAML, JSON, TOML, PROPERTIES, etc.).
	 * 
	 * @throws IOException thrown in case accessing or processing the properties
	 *         file failed.
	 * @throws ParseException Signals that an error has been reached
	 *         unexpectedly while parsing the data to be loaded.
	 */
	public XmlPropertiesBuilder( String aFilePath, ConfigLocator aConfigLocator, DocumentMetrics aDocumentMetrics ) throws IOException, ParseException {
		this( aDocumentMetrics );
		seekFrom( aFilePath, aConfigLocator );
	}

	/**
	 * Loads the XML properties from the given {@link URL}.
	 *
	 * @param aUrl The {@link URL} from which to read the properties.
	 * 
	 * @throws IOException thrown in case accessing or processing the properties
	 *         file failed.
	 * @throws ParseException Signals that an error has been reached
	 *         unexpectedly while parsing the data to be loaded.
	 */
	public XmlPropertiesBuilder( URL aUrl ) throws IOException, ParseException {
		this( aUrl, DocumentNotation.DEFAULT );
	}

	/**
	 * Loads the XML properties from the given {@link URL}.
	 *
	 * @param aUrl The {@link URL} from which to read the properties.
	 * @param aDocumentMetrics Provides various metrics which may be tweaked
	 *        when marshaling or unmarshaling documents of various nations (such
	 *        as INI, XML, YAML, JSON, TOML, PROPERTIES, etc.).
	 * 
	 * @throws IOException thrown in case accessing or processing the properties
	 *         file failed.
	 * @throws ParseException Signals that an error has been reached
	 *         unexpectedly while parsing the data to be loaded.
	 */
	public XmlPropertiesBuilder( URL aUrl, DocumentMetrics aDocumentMetrics ) throws IOException, ParseException {
		this( aDocumentMetrics );
		loadFrom( aUrl );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected CanonicalMapFactory createCanonicalMapFactory() {
		return new XmlCanonicalMapFactory() {

			@Override
			public CanonicalMapBuilder fromMarshaled( InputStream aExternalRepresentation, Map<String, String> aProperties ) throws UnmarshalException {
				final CanonicalMapBuilder theCanonicalMap = super.fromMarshaled( aExternalRepresentation, aProperties );
				if ( !DocumentProperty.hasEnvelope( aProperties ) ) {
					// Unwrap the XML root element (envelope) |-->
					if ( ( theCanonicalMap.leaves().size() == 0 && theCanonicalMap.dirs().size() == 1 ) ) {
						return new CanonicalMapBuilderImpl( theCanonicalMap.retrieveFrom( theCanonicalMap.dirs().iterator().next() ) );
					}
					// Unwrap the XML root element (envelope) <--|
				}
				return theCanonicalMap;
			}
		};
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * The {@link XmlPropertiesBuilderFactory} represents a
	 * {@link ResourcePropertiesBuilderFactory} creating instances of type
	 * {@link XmlPropertiesBuilder}.
	 */
	public static class XmlPropertiesBuilderFactory implements ResourcePropertiesBuilderFactory {

		// /////////////////////////////////////////////////////////////////////
		// VARIABLES:
		// /////////////////////////////////////////////////////////////////////

		private final DocumentMetrics _documentMetrics;

		// /////////////////////////////////////////////////////////////////////
		// CONSTRUCTORS:
		// /////////////////////////////////////////////////////////////////////

		/**
		 * Constructs an {@link XmlCanonicalMapFactory} for creating
		 * {@link XmlPropertiesBuilder} instances.
		 */
		public XmlPropertiesBuilderFactory() {
			this( DocumentNotation.DEFAULT );
		}

		/**
		 * Constructs an {@link XmlCanonicalMapFactory} for creating
		 * {@link XmlPropertiesBuilder} instances.
		 * 
		 * @param aDocumentMetrics Provides various metrics which may be tweaked
		 *        when marshaling or unmarshaling documents of various nations
		 *        (such as INI, XML, YAML, JSON, TOML, PROPERTIES, etc.).
		 */
		public XmlPropertiesBuilderFactory( DocumentMetrics aDocumentMetrics ) {
			_documentMetrics = aDocumentMetrics;
		}

		// /////////////////////////////////////////////////////////////////////
		// METHODS:
		// /////////////////////////////////////////////////////////////////////

		/**
		 * {@inheritDoc}
		 */
		@Override
		public String[] getFilenameSuffixes() {
			return new String[] { FilenameExtension.XML.getFilenameSuffix() };
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public ResourcePropertiesBuilder toProperties( Class<?> aResourceClass, String aFilePath, ConfigLocator aConfigLocator ) throws IOException, ParseException {
			return new XmlPropertiesBuilder( aResourceClass, aFilePath, aConfigLocator, _documentMetrics );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public ResourcePropertiesBuilder toProperties( File aFile, ConfigLocator aConfigLocator ) throws IOException, ParseException {
			return new XmlPropertiesBuilder( aFile, aConfigLocator, _documentMetrics );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public ResourcePropertiesBuilder toProperties( InputStream aInputStream ) throws IOException, ParseException {
			return new XmlPropertiesBuilder( aInputStream, _documentMetrics );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public ResourcePropertiesBuilder toProperties( Map<?, ?> aProperties ) {
			return new XmlPropertiesBuilder( aProperties, _documentMetrics );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public ResourcePropertiesBuilder toProperties( Object aObj ) {
			return new XmlPropertiesBuilder( aObj, _documentMetrics );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public ResourcePropertiesBuilder toProperties( Properties aProperties ) {
			return new XmlPropertiesBuilder( aProperties, _documentMetrics );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public ResourcePropertiesBuilder toProperties( PropertiesBuilder aProperties ) {
			return new XmlPropertiesBuilder( aProperties, _documentMetrics );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public ResourcePropertiesBuilder toProperties( String aFilePath, ConfigLocator aConfigLocator ) throws IOException, ParseException {
			return new XmlPropertiesBuilder( aFilePath, aConfigLocator, _documentMetrics );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public ResourcePropertiesBuilder toProperties( URL aUrl ) throws IOException, ParseException {
			return new XmlPropertiesBuilder( aUrl, _documentMetrics );
		}
	}
}
