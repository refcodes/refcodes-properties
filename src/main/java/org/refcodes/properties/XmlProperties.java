// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.properties;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.ParseException;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.refcodes.data.Delimiter;
import org.refcodes.data.FilenameExtension;
import org.refcodes.runtime.ConfigLocator;
import org.refcodes.runtime.SystemProperty;
import org.refcodes.struct.ext.factory.XmlCanonicalMapFactory;

/**
 * Implementation of the {@link ResourceProperties} interface with support of so
 * called "XML properties". For XML properties, see
 * "https://en.wikipedia.org/wiki/XML"
 * 
 * By default the (XML) document's root element is omitted. This behavior can
 * either be changed by calling the according constructor or by the system
 * property {@link SystemProperty#DOCUMENT_ENVELOPE}. When setting XML root
 * elelemt preservation to <code>true</code> then unmarshaling (XML) documents
 * will preserve the preserve the root element (envelope). As an (XML) document
 * requires a root element, the root element often is provided merely as of
 * syntactic reasons and must be omitted as of semantic reasons. Unmarshaling
 * functionality therefore by default skips the root elelemt, as this is
 * considered merely to serve as an envelope. This behavior can be overridden by
 * setting the XML root elelemt preservation to <code>true</code>.
 */
public class XmlProperties extends AbstractResourceProperties {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Loads the XML Properties from the given file's path.
	 *
	 * @param aResourceClass The class which's class loader is to take care of
	 *        loading the properties (from inside a JAR).
	 * @param aFilePath The file path of the class's resources from which to
	 *        load the properties.
	 * 
	 * @throws IOException thrown in case accessing or processing the properties
	 *         file failed.
	 * @throws ParseException Signals that an error has been reached
	 *         unexpectedly while parsing the data to be loaded.
	 */
	public XmlProperties( Class<?> aResourceClass, String aFilePath ) throws IOException, ParseException {
		super( new XmlPropertiesBuilder( aResourceClass, aFilePath ) );
	}

	/**
	 * Loads the XML Properties from the given file's path.
	 *
	 * @param aResourceClass The class which's class loader is to take care of
	 *        loading the properties (from inside a JAR).
	 * @param aFilePath The file path of the class's resources from which to
	 *        load the properties.
	 * @param aDocumentMetrics Provides various metrics which may be tweaked
	 *        when marshaling or unmarshaling documents of various nations (such
	 *        as INI, XML, YAML, JSON, TOML, PROPERTIES, etc.).
	 * 
	 * @throws IOException thrown in case accessing or processing the properties
	 *         file failed.
	 * @throws ParseException Signals that an error has been reached
	 *         unexpectedly while parsing the data to be loaded.
	 */
	public XmlProperties( Class<?> aResourceClass, String aFilePath, DocumentMetrics aDocumentMetrics ) throws IOException, ParseException {
		super( new XmlPropertiesBuilder( aResourceClass, aFilePath, aDocumentMetrics ) );
	}

	/**
	 * Loads the XML Properties from the given file's path. A provided
	 * {@link ConfigLocator} describes the locations to additional crawl for the
	 * desired file. Finally (if nothing else succeeds) the properties are
	 * loaded by the provided class's class loader which takes care of loading
	 * the properties (in case the file path is a relative path, also the
	 * absolute path with a prefixed path delimiter "/" is probed).
	 *
	 * @param aResourceClass The class which's class loader is to take care of
	 *        loading the properties (from inside a JAR).
	 * @param aFilePath The file path of the class's resources from which to
	 *        load the properties.
	 * @param aConfigLocator The {@link ConfigLocator} describes the locations
	 *        to additional crawl for the desired file.
	 * 
	 * @throws IOException thrown in case accessing or processing the properties
	 *         file failed.
	 * @throws ParseException Signals that an error has been reached
	 *         unexpectedly while parsing the data to be loaded.
	 */
	public XmlProperties( Class<?> aResourceClass, String aFilePath, ConfigLocator aConfigLocator ) throws IOException, ParseException {
		super( new XmlPropertiesBuilder( aResourceClass, aFilePath, aConfigLocator ) );
	}

	/**
	 * Loads the XML Properties from the given file's path. A provided
	 * {@link ConfigLocator} describes the locations to additional crawl for the
	 * desired file. Finally (if nothing else succeeds) the properties are
	 * loaded by the provided class's class loader which takes care of loading
	 * the properties (in case the file path is a relative path, also the
	 * absolute path with a prefixed path delimiter "/" is probed).
	 *
	 * @param aResourceClass The class which's class loader is to take care of
	 *        loading the properties (from inside a JAR).
	 * @param aFilePath The file path of the class's resources from which to
	 *        load the properties.
	 * @param aConfigLocator The {@link ConfigLocator} describes the locations
	 *        to additional crawl for the desired file.
	 * @param aDocumentMetrics Provides various metrics which may be tweaked
	 *        when marshaling or unmarshaling documents of various nations (such
	 *        as INI, XML, YAML, JSON, TOML, PROPERTIES, etc.).
	 * 
	 * @throws IOException thrown in case accessing or processing the properties
	 *         file failed.
	 * @throws ParseException Signals that an error has been reached
	 *         unexpectedly while parsing the data to be loaded.
	 */
	public XmlProperties( Class<?> aResourceClass, String aFilePath, ConfigLocator aConfigLocator, DocumentMetrics aDocumentMetrics ) throws IOException, ParseException {
		super( new XmlPropertiesBuilder( aResourceClass, aFilePath, aConfigLocator, aDocumentMetrics ) );
	}

	/**
	 * Loads the XML Properties from the given {@link File}.
	 *
	 * @param aFile The {@link File} from which to load the properties.
	 * 
	 * @throws IOException thrown in case accessing or processing the properties
	 *         file failed.
	 * @throws ParseException Signals that an error has been reached
	 *         unexpectedly while parsing the data to be loaded.
	 */
	public XmlProperties( File aFile ) throws IOException, ParseException {
		super( new XmlPropertiesBuilder( aFile ) );
	}

	/**
	 * Loads the XML Properties from the given {@link File}.
	 *
	 * @param aFile The {@link File} from which to load the properties.
	 * @param aDocumentMetrics Provides various metrics which may be tweaked
	 *        when marshaling or unmarshaling documents of various nations (such
	 *        as INI, XML, YAML, JSON, TOML, PROPERTIES, etc.).
	 * 
	 * @throws IOException thrown in case accessing or processing the properties
	 *         file failed.
	 * @throws ParseException Signals that an error has been reached
	 *         unexpectedly while parsing the data to be loaded.
	 */
	public XmlProperties( File aFile, DocumentMetrics aDocumentMetrics ) throws IOException, ParseException {
		super( new XmlPropertiesBuilder( aFile, aDocumentMetrics ) );
	}

	/**
	 * Loads or seeks the XML Properties from the given {@link File}. A provided
	 * {@link ConfigLocator} describes the locations to additional crawl for the
	 * desired file.
	 * 
	 * @param aFile The {@link File} from which to load the properties.
	 * @param aConfigLocator The {@link ConfigLocator} describes the locations
	 *        to additional crawl for the desired file.
	 * 
	 * @throws IOException thrown in case accessing or processing the properties
	 *         file failed.
	 * @throws ParseException Signals that an error has been reached
	 *         unexpectedly while parsing the data to be loaded.
	 */
	public XmlProperties( File aFile, ConfigLocator aConfigLocator ) throws IOException, ParseException {
		super( new XmlPropertiesBuilder( aFile, aConfigLocator ) );
	}

	/**
	 * Loads or seeks the XML Properties from the given {@link File}. A provided
	 * {@link ConfigLocator} describes the locations to additional crawl for the
	 * desired file.
	 *
	 * @param aFile The {@link File} from which to load the properties.
	 * @param aConfigLocator The {@link ConfigLocator} describes the locations
	 *        to additional crawl for the desired file.
	 * @param aDocumentMetrics Provides various metrics which may be tweaked
	 *        when marshaling or unmarshaling documents of various nations (such
	 *        as INI, XML, YAML, JSON, TOML, PROPERTIES, etc.).
	 * 
	 * @throws IOException thrown in case accessing or processing the properties
	 *         file failed.
	 * @throws ParseException Signals that an error has been reached
	 *         unexpectedly while parsing the data to be loaded.
	 */
	public XmlProperties( File aFile, ConfigLocator aConfigLocator, DocumentMetrics aDocumentMetrics ) throws IOException, ParseException {
		super( new XmlPropertiesBuilder( aFile, aConfigLocator, aDocumentMetrics ) );
	}

	/**
	 * Reads the XML Properties from the given {@link InputStream}.
	 *
	 * @param aInputStream The {@link InputStream} from which to read the
	 *        properties.
	 * 
	 * @throws IOException thrown in case accessing or processing the properties
	 *         file failed.
	 * @throws ParseException Signals that an error has been reached
	 *         unexpectedly while parsing the data to be loaded.
	 */
	public XmlProperties( InputStream aInputStream ) throws IOException, ParseException {
		super( new XmlPropertiesBuilder( aInputStream ) );
	}

	/**
	 * Reads the XML Properties from the given {@link InputStream}.
	 *
	 * @param aInputStream The {@link InputStream} from which to read the
	 *        properties.
	 * @param aDocumentMetrics Provides various metrics which may be tweaked
	 *        when marshaling or unmarshaling documents of various nations (such
	 *        as INI, XML, YAML, JSON, TOML, PROPERTIES, etc.).
	 * 
	 * @throws IOException thrown in case accessing or processing the properties
	 *         file failed.
	 * @throws ParseException Signals that an error has been reached
	 *         unexpectedly while parsing the data to be loaded.
	 */
	public XmlProperties( InputStream aInputStream, DocumentMetrics aDocumentMetrics ) throws IOException, ParseException {
		super( new XmlPropertiesBuilder( aInputStream, aDocumentMetrics ) );
	}

	/**
	 * Create a {@link XmlProperties} instance containing the elements of the
	 * provided {@link Map} instance using the default path delimiter "/"
	 * ({@link Delimiter#PATH}) for the path declarations.
	 *
	 * @param aProperties the properties to be added.
	 */
	public XmlProperties( Map<?, ?> aProperties ) {
		super( new XmlPropertiesBuilder( aProperties ) );
	}

	/**
	 * Create a {@link XmlProperties} instance containing the elements of the
	 * provided {@link Map} instance using the default path delimiter "/"
	 * ({@link Delimiter#PATH}) for the path declarations.
	 *
	 * @param aProperties the properties to be added.
	 * @param aDocumentMetrics Provides various metrics which may be tweaked
	 *        when marshaling or unmarshaling documents of various nations (such
	 *        as INI, XML, YAML, JSON, TOML, PROPERTIES, etc.).
	 */
	public XmlProperties( Map<?, ?> aProperties, DocumentMetrics aDocumentMetrics ) {
		super( new XmlPropertiesBuilder( aProperties, aDocumentMetrics ) );
	}

	/**
	 * Create a {@link XmlProperties} instance containing the elements as of
	 * {@link MutablePathMap#insert(Object)} using the default path delimiter
	 * "/" ({@link Delimiter#PATH}) for the path declarations: "Inspects the
	 * given object and adds all elements found in the given object. Elements of
	 * type {@link Map}, {@link Collection} and arrays are identified and
	 * handled as of their type: The path for each value in a {@link Map} is
	 * appended with its according key. The path for each value in a
	 * {@link Collection} or array is appended with its according index of
	 * occurrence (in case of a {@link List} or an array, its actual index). In
	 * case of reflection, the path for each member is appended with its
	 * according mamber's name. All elements (e.g. the members and values) are
	 * inspected recursively which results in the according paths of the
	 * terminating values."
	 *
	 * @param aObj The object from which the elements are to be added.
	 */
	public XmlProperties( Object aObj ) {
		super( new XmlPropertiesBuilder( aObj ) );
	}

	/**
	 * Create a {@link XmlProperties} instance containing the elements as of
	 * {@link MutablePathMap#insert(Object)} using the default path delimiter
	 * "/" ({@link Delimiter#PATH}) for the path declarations: "Inspects the
	 * given object and adds all elements found in the given object. Elements of
	 * type {@link Map}, {@link Collection} and arrays are identified and
	 * handled as of their type: The path for each value in a {@link Map} is
	 * appended with its according key. The path for each value in a
	 * {@link Collection} or array is appended with its according index of
	 * occurrence (in case of a {@link List} or an array, its actual index). In
	 * case of reflection, the path for each member is appended with its
	 * according mamber's name. All elements (e.g. the members and values) are
	 * inspected recursively which results in the according paths of the
	 * terminating values."
	 *
	 * @param aObj The object from which the elements are to be added.
	 * @param aDocumentMetrics Provides various metrics which may be tweaked
	 *        when marshaling or unmarshaling documents of various nations (such
	 *        as INI, XML, YAML, JSON, TOML, PROPERTIES, etc.).
	 */
	public XmlProperties( Object aObj, DocumentMetrics aDocumentMetrics ) {
		super( new XmlPropertiesBuilder( aObj, aDocumentMetrics ) );
	}

	/**
	 * Create a {@link XmlProperties} instance containing the elements of the
	 * provided {@link Properties} instance using the default path delimiter "/"
	 * ({@link Delimiter#PATH}) for the path declarations.
	 *
	 * @param aProperties the properties to be added.
	 */
	public XmlProperties( Properties aProperties ) {
		super( new XmlPropertiesBuilder( aProperties ) );
	}

	/**
	 * Create a {@link XmlProperties} instance containing the elements of the
	 * provided {@link Properties} instance using the default path delimiter "/"
	 * ({@link Delimiter#PATH}) for the path declarations.
	 *
	 * @param aProperties the properties to be added.
	 * @param aDocumentMetrics Provides various metrics which may be tweaked
	 *        when marshaling or unmarshaling documents of various nations (such
	 *        as INI, XML, YAML, JSON, TOML, PROPERTIES, etc.).
	 */
	public XmlProperties( Properties aProperties, DocumentMetrics aDocumentMetrics ) {
		super( new XmlPropertiesBuilder( aProperties, aDocumentMetrics ) );
	}

	/**
	 * Create a {@link XmlProperties} instance containing the elements of the
	 * provided {@link PropertiesBuilder} instance using the default path
	 * delimiter "/" ({@link Delimiter#PATH}) for the path declarations.
	 *
	 * @param aProperties the properties to be added.
	 */
	public XmlProperties( PropertiesBuilder aProperties ) {
		super( new XmlPropertiesBuilder( aProperties ) );
	}

	/**
	 * Create a {@link XmlProperties} instance containing the elements of the
	 * provided {@link PropertiesBuilder} instance using the default path
	 * delimiter "/" ({@link Delimiter#PATH}) for the path declarations.
	 *
	 * @param aProperties the properties to be added.
	 * @param aDocumentMetrics Provides various metrics which may be tweaked
	 *        when marshaling or unmarshaling documents of various nations (such
	 *        as INI, XML, YAML, JSON, TOML, PROPERTIES, etc.).
	 */
	public XmlProperties( PropertiesBuilder aProperties, DocumentMetrics aDocumentMetrics ) {
		super( new XmlPropertiesBuilder( aProperties, aDocumentMetrics ) );
	}

	/**
	 * Loads the XML Properties from the given file's path.
	 *
	 * @param aFilePath The path to the file from which to load the properties.
	 * 
	 * @throws IOException thrown in case accessing or processing the properties
	 *         file failed.
	 * @throws ParseException Signals that an error has been reached
	 *         unexpectedly while parsing the data to be loaded.
	 */
	public XmlProperties( String aFilePath ) throws IOException, ParseException {
		super( new XmlPropertiesBuilder( aFilePath ) );
	}

	/**
	 * Loads the XML Properties from the given file's path.
	 *
	 * @param aFilePath The path to the file from which to load the properties.
	 * @param aDocumentMetrics Provides various metrics which may be tweaked
	 *        when marshaling or unmarshaling documents of various nations (such
	 *        as INI, XML, YAML, JSON, TOML, PROPERTIES, etc.).
	 * 
	 * @throws IOException thrown in case accessing or processing the properties
	 *         file failed.
	 * @throws ParseException Signals that an error has been reached
	 *         unexpectedly while parsing the data to be loaded.
	 */
	public XmlProperties( String aFilePath, DocumentMetrics aDocumentMetrics ) throws IOException, ParseException {
		super( new XmlPropertiesBuilder( aFilePath, aDocumentMetrics ) );
	}

	/**
	 * Loads the XML Properties from the given file's path. A provided
	 * {@link ConfigLocator} describes the locations to additional crawl for the
	 * desired file.
	 * 
	 * @param aFilePath The path to the file from which to load the properties.
	 * @param aConfigLocator The {@link ConfigLocator} describes the locations
	 *        to additional crawl for the desired file.
	 * 
	 * @throws IOException thrown in case accessing or processing the properties
	 *         file failed.
	 * @throws ParseException Signals that an error has been reached
	 *         unexpectedly while parsing the data to be loaded.
	 */
	public XmlProperties( String aFilePath, ConfigLocator aConfigLocator ) throws IOException, ParseException {
		super( new XmlPropertiesBuilder( aFilePath, aConfigLocator ) );
	}

	/**
	 * Loads the XML Properties from the given file's path. A provided
	 * {@link ConfigLocator} describes the locations to additional crawl for the
	 * desired file.
	 *
	 * @param aFilePath The path to the file from which to load the properties.
	 * @param aConfigLocator The {@link ConfigLocator} describes the locations
	 *        to additional crawl for the desired file.
	 * @param aDocumentMetrics Provides various metrics which may be tweaked
	 *        when marshaling or unmarshaling documents of various nations (such
	 *        as INI, XML, YAML, JSON, TOML, PROPERTIES, etc.).
	 * 
	 * @throws IOException thrown in case accessing or processing the properties
	 *         file failed.
	 * @throws ParseException Signals that an error has been reached
	 *         unexpectedly while parsing the data to be loaded.
	 */
	public XmlProperties( String aFilePath, ConfigLocator aConfigLocator, DocumentMetrics aDocumentMetrics ) throws IOException, ParseException {
		super( new XmlPropertiesBuilder( aFilePath, aConfigLocator, aDocumentMetrics ) );
	}

	/**
	 * Loads the XML Properties from the given {@link URL}.
	 *
	 * @param aUrl The {@link URL} from which to read the properties.
	 * 
	 * @throws IOException thrown in case accessing or processing the properties
	 *         file failed.
	 * @throws ParseException Signals that an error has been reached
	 *         unexpectedly while parsing the data to be loaded.
	 */
	public XmlProperties( URL aUrl ) throws IOException, ParseException {
		super( new XmlPropertiesBuilder( aUrl ) );
	}

	/**
	 * Loads the XML Properties from the given {@link URL}.
	 *
	 * @param aUrl The {@link URL} from which to read the properties.
	 * @param aDocumentMetrics Provides various metrics which may be tweaked
	 *        when marshaling or unmarshaling documents of various nations (such
	 *        as INI, XML, YAML, JSON, TOML, PROPERTIES, etc.).
	 * 
	 * @throws IOException thrown in case accessing or processing the properties
	 *         file failed.
	 * @throws ParseException Signals that an error has been reached
	 *         unexpectedly while parsing the data to be loaded.
	 */
	public XmlProperties( URL aUrl, DocumentMetrics aDocumentMetrics ) throws IOException, ParseException {
		super( new XmlPropertiesBuilder( aUrl, aDocumentMetrics ) );
	}

	/**
	 * Create an empty {@link XmlProperties} instance.
	 */
	protected XmlProperties() {
		super( new XmlPropertiesBuilder() );
	}

	/**
	 * Create an empty {@link XmlProperties} instance.
	 * 
	 * @param aDocumentMetrics Provides various metrics which may be tweaked
	 *        when marshaling or unmarshaling documents of various nations (such
	 *        as INI, XML, YAML, JSON, TOML, PROPERTIES, etc.).
	 */
	protected XmlProperties( DocumentMetrics aDocumentMetrics ) {
		super( new XmlPropertiesBuilder( aDocumentMetrics ) );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean containsKey( Object aKey ) {
		return _properties.containsKey( aKey );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String get( Object aKey ) {
		return _properties.get( aKey );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public char getDelimiter() {
		return _properties.getDelimiter();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Class<String> getType() {
		return _properties.getType();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isEmpty() {
		return _properties.isEmpty();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Set<String> keySet() {
		return _properties.keySet();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Properties reload() throws IOException, ParseException {
		return _properties.reload();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Properties reload( ReloadMode aReloadMode ) throws IOException, ParseException {
		return _properties.reload( aReloadMode );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Properties retrieveFrom( String aFromPath ) {
		return _properties.retrieveFrom( aFromPath );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Properties retrieveTo( String aToPath ) {
		return _properties.retrieveTo( aToPath );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int size() {
		return _properties.size();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object toDataStructure( String aFromPath ) {
		return _properties.toDataStructure( aFromPath );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Collection<String> values() {
		return _properties.values();
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * The {@link XmlPropertiesFactory} represents a
	 * {@link ResourcePropertiesFactory} creating instances of type
	 * {@link XmlProperties}.
	 */
	public static class XmlPropertiesFactory implements ResourcePropertiesFactory {

		// /////////////////////////////////////////////////////////////////////
		// VARIABLES:
		// /////////////////////////////////////////////////////////////////////

		private final DocumentMetrics _documentMetrics;

		// /////////////////////////////////////////////////////////////////////
		// CONSTRUCTORS:
		// /////////////////////////////////////////////////////////////////////

		/**
		 * Constructs an {@link XmlCanonicalMapFactory} for creating
		 * {@link XmlPropertiesBuilder} instances.
		 */
		public XmlPropertiesFactory() {
			this( DocumentNotation.DEFAULT );
		}

		/**
		 * Constructs an {@link XmlCanonicalMapFactory} for creating
		 * {@link XmlPropertiesBuilder} instances.
		 * 
		 * @param aDocumentMetrics Provides various metrics which may be tweaked
		 *        when marshaling or unmarshaling documents of various nations
		 *        (such as INI, XML, YAML, JSON, TOML, PROPERTIES, etc.).
		 */
		public XmlPropertiesFactory( DocumentMetrics aDocumentMetrics ) {
			_documentMetrics = aDocumentMetrics;
		}

		// /////////////////////////////////////////////////////////////////////
		// METHODS:
		// /////////////////////////////////////////////////////////////////////

		/**
		 * {@inheritDoc}
		 */
		@Override
		public String[] getFilenameSuffixes() {
			return new String[] { FilenameExtension.XML.getFilenameSuffix() };
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public ResourceProperties toProperties( Class<?> aResourceClass, String aFilePath, ConfigLocator aConfigLocator ) throws IOException, ParseException {
			return new XmlProperties( aResourceClass, aFilePath, aConfigLocator, _documentMetrics );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public ResourceProperties toProperties( File aFile, ConfigLocator aConfigLocator ) throws IOException, ParseException {
			return new XmlProperties( aFile, aConfigLocator, _documentMetrics );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public ResourceProperties toProperties( InputStream aInputStream ) throws IOException, ParseException {
			return new XmlProperties( aInputStream, _documentMetrics );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public ResourceProperties toProperties( Map<?, ?> aProperties ) {
			return new XmlProperties( aProperties, _documentMetrics );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public ResourceProperties toProperties( Object aObj ) {
			return new XmlProperties( aObj, _documentMetrics );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public ResourceProperties toProperties( Properties aProperties ) {
			return new XmlProperties( aProperties, _documentMetrics );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public ResourcePropertiesBuilder toProperties( PropertiesBuilder aProperties ) {
			return new XmlPropertiesBuilder( aProperties, _documentMetrics );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public ResourceProperties toProperties( String aFilePath, ConfigLocator aConfigLocator ) throws IOException, ParseException {
			return new XmlProperties( aFilePath, aConfigLocator, _documentMetrics );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public ResourceProperties toProperties( URL aUrl ) throws IOException, ParseException {
			return new XmlProperties( aUrl, _documentMetrics );
		}
	}
}
