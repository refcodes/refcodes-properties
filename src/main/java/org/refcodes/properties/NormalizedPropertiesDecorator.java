// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.properties;

import java.util.HashSet;
import java.util.Set;

import org.refcodes.data.Delimiter;
import org.refcodes.struct.AmbiguousKeyRuntimeException;

/**
 * The {@link NormalizedPropertiesDecorator} type decorates a {@link Properties}
 * instance and converts a path delimiter such as the full-stop (".") to the
 * {@link Properties} properties' path delimiter slash ("/", as of
 * {@link #getDelimiter()}). E.g. you get a projection of your
 * {@link Properties} using "." as the path delimiter as if them had a "/" as
 * path delimiter. Invoking
 * {@link #NormalizedPropertiesDecorator(Properties, char[])} the constructor,
 * you can define the delimiters to be projected to the default "/" delimiter
 * (as of {@link #getDelimiter()}). Upon accessing a key, the implementation
 * first tests whether the decorated (underlying) {@link Properties} contain
 * this key, then the decorated (underlying) {@link Properties}' keys are tested
 * by converting the keys using the first provided delimiter to be replaced by
 * the "/" delimiter". If we have a match, then this property is processed, else
 * the procedure continues with the next delimiter. This means that the first
 * match wins, if we have succeeding matches with succeeding delimiters then an
 * {@link AmbiguousKeyRuntimeException} is thrown In case of accessing the
 * {@link #keySet()}, all keys are converted by replacing all the provided
 * delimiters (or the full-stop ("."), depending on the constructor you called)
 * to the default delimiter "/". In case we have a key collision then an
 * {@link AmbiguousKeyRuntimeException} is thrown.
 */
public class NormalizedPropertiesDecorator extends AbstractPropertiesDecorator<Properties> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final char[] _delimiters;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Decorates the provided {@link Properties} with additional behavior by
	 * normalizing the key representing the path pointing to the according
	 * value. You provide {@link Properties} with a path delimiter different
	 * from the default delimiter and you get {@link Properties} which act as if
	 * the delimiter was the default delimiter: Paths containing the namespace
	 * delimiter "." (as of {@link Delimiter#NAMESPACE} are converted to paths'
	 * with delimiter "/" (as of {@link Delimiter#PATH}. Changes applied to the
	 * provided {@link Properties} affect the decorator.
	 * 
	 * @param aProperties The {@link Properties} to be decorated.
	 */
	public NormalizedPropertiesDecorator( Properties aProperties ) {
		super( aProperties );
		_delimiters = new char[] { Delimiter.NAMESPACE.getChar() };
	}

	/**
	 * Decorates the provided {@link Properties} with additional behavior by
	 * normalizing the key representing the path pointing to the according
	 * value. You provide {@link Properties} with a path delimiter different
	 * from the default delimiter and you get {@link Properties} which act as if
	 * the delimiter was the default delimiter: Paths containing the provided
	 * delimiters are converted to the paths' with delimiter "/" (as of
	 * {@link Delimiter#PATH}. Changes applied to the provided
	 * {@link Properties} affect the decorator.
	 * 
	 * @param aProperties The {@link Properties} to be decorated.
	 * @param aDelimiters The delimiters to be converted forth and back
	 */
	public NormalizedPropertiesDecorator( Properties aProperties, char[] aDelimiters ) {
		super( aProperties );
		_delimiters = aDelimiters;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean containsKey( Object aKey ) {
		boolean isContainsKey = super.containsKey( aKey );
		if ( aKey != null ) {
			final String theKey = aKey.toString();
			String eKey;
			String theDelimiters = "";
			for ( char eDelimiter : _delimiters ) {

				// |--> Just for the error aMessage
				if ( theDelimiters.length() > 0 ) {
					theDelimiters += ", ";
				}
				theDelimiters += "'" + eDelimiter + "'";
				// Just for the error aMessage <--|

				eKey = fromNormalized( theKey, getDelimiter(), eDelimiter );
				// |--> De-normalized
				if ( super.containsKey( eKey ) ) {
					if ( isContainsKey ) {
						throw new AmbiguousKeyRuntimeException( "There are multiple keys matching the provided key <" + aKey + "> in your underlyng properties after replacing the delimiter <" + getDelimiter() + "> with one or more of your provided delimiters. The superflous original key is <" + eKey + "> after appliying <" + eDelimiter + "> to the provided key <" + aKey + "> (delimiters processed so far: " + theDelimiters + ").", eKey );
					}
					isContainsKey = true;
				}
				// De-normalized <--|
				// |--> With alternate path delimiter
				if ( super.containsKey( eDelimiter + eKey ) ) {
					if ( isContainsKey ) {
						throw new AmbiguousKeyRuntimeException( "There are multiple keys matching the provided key <" + aKey + "> in your underlyng properties after replacing the delimiter <" + getDelimiter() + "> with one or more of your provided delimiters. The superflous original key is <" + eDelimiter + eKey + "> after appliying <" + eDelimiter + "> to the provided key <" + aKey + "> (delimiters processed so far: " + theDelimiters + ").", eKey );
					}
					isContainsKey = true;
				}
				// With alternate path delimiter <--|
				// Without alternate path delimiter <--|
				if ( eKey.length() > 0 && eKey.charAt( 0 ) == eDelimiter && super.containsKey( eKey.substring( 1 ) ) ) {
					if ( isContainsKey ) {
						throw new AmbiguousKeyRuntimeException( "There are multiple keys matching the provided key <" + aKey + "> in your underlyng properties after replacing the delimiter <" + getDelimiter() + "> with one or more of your provided delimiters. The superflous original key is <" + eKey.substring( 1 ) + "> after appliying <" + eDelimiter + "> to the provided key <" + aKey + "> (delimiters processed so far: " + theDelimiters + ").", eKey );
					}
					isContainsKey = true;
				}
				// Without alternate path delimiter <--|
			}
		}
		return isContainsKey;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String get( Object aKey ) {
		String theValue = super.get( aKey );
		if ( aKey != null ) {
			final String theKey = aKey.toString();
			String eKey;
			String theDelimiters = "";
			String eValue1;
			String eValue2;
			String eValue3;
			for ( char eDelimiter : _delimiters ) {

				// |--> Just for the error aMessage
				if ( theDelimiters.length() > 0 ) {
					theDelimiters += ", ";
				}
				theDelimiters += "'" + eDelimiter + "'";
				// Just for the error aMessage <--|

				eKey = fromNormalized( theKey, getDelimiter(), eDelimiter );
				// |--> De-normalized
				eValue1 = super.get( eKey );
				if ( eValue1 != null ) {
					if ( theValue != null ) {
						throw new AmbiguousKeyRuntimeException( "There are multiple keys matching the provided key <" + aKey + "> in your underlyng properties after replacing the delimiter <" + getDelimiter() + "> with one or more of your provided delimiters. The superflous original key is <" + eKey + "> after appliying <" + eDelimiter + "> to the provided key <" + aKey + "> (delimiters processed so far: " + theDelimiters + ").", eKey );
					}
					theValue = eValue1;
				}
				// De-normalized <--|
				// |--> With alternate path delimiter
				eValue2 = super.get( eDelimiter + eKey );
				if ( eValue2 != null ) {
					if ( theValue != null ) {
						throw new AmbiguousKeyRuntimeException( "There are multiple keys matching the provided key <" + aKey + "> in your underlyng properties after replacing the delimiter <" + getDelimiter() + "> with one or more of your provided delimiters. The superflous original key is <" + eDelimiter + eKey + "> after appliying <" + eDelimiter + "> to the provided key <" + aKey + "> (delimiters processed so far: " + theDelimiters + ").", eKey );
					}
					theValue = eValue2;
				}
				// With alternate path delimiter <--|
				// Without alternate path delimiter <--|
				if ( eKey.length() > 0 && eKey.charAt( 0 ) == eDelimiter ) {
					eValue3 = super.get( eKey.substring( 1 ) );
					if ( eValue3 != null ) {
						if ( theValue != null ) {
							throw new AmbiguousKeyRuntimeException( "There are multiple keys matching the provided key <" + aKey + "> in your underlyng properties after replacing the delimiter <" + getDelimiter() + "> with one or more of your provided delimiters. The superflous original key is <" + eKey.substring( 1 ) + "> after appliying <" + eDelimiter + "> to the provided key <" + aKey + "> (delimiters processed so far: " + theDelimiters + ").", eKey );
						}
						theValue = eValue3;
					}
				}
				// Without alternate path delimiter <--|
			}
		}
		return theValue;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Set<String> keySet() {
		final Set<String> theKeys = super.keySet();
		final Set<String> theProjectedKeys = new HashSet<>();
		String eProjectedKey;
		for ( String eKey : theKeys ) {
			eProjectedKey = eKey;
			eProjectedKey = toNormalized( eProjectedKey, getDelimiter(), _delimiters );
			if ( theProjectedKeys.contains( eProjectedKey ) ) {
				throw new AmbiguousKeyRuntimeException( "There are multiple keys matching the key <" + eKey + "> in your underlyng properties after converting the keys by replacing the delimiters provided <" + toDelimiters() + "> to the delimiter <" + getDelimiter() + ">.", eKey );
			}
			theProjectedKeys.add( eProjectedKey );
		}
		return theProjectedKeys;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Properties retrieveFrom( String aFromPath ) {
		return new PropertiesImpl( this ).retrieveFrom( aFromPath );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Properties retrieveTo( String aToPath ) {
		return new PropertiesImpl( this ).retrieveTo( aToPath );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * To delimiters.
	 *
	 * @return the string
	 */
	protected String toDelimiters() {
		String theDelimiters = "";
		for ( char eDelimiter : _delimiters ) {
			if ( theDelimiters.length() > 0 ) {
				theDelimiters += ", ";
			}
			theDelimiters += "'" + eDelimiter + "'";
		}
		return theDelimiters;
	}

	/**
	 * From normalized.
	 *
	 * @param aKey the key
	 * @param aFromDelimiter the from delimiter
	 * @param aToDelimiter the to delimiter
	 * @return the string
	 */
	protected static String fromNormalized( String aKey, char aFromDelimiter, char aToDelimiter ) {
		if ( aKey != null ) {
			aKey = aKey.replace( aFromDelimiter, aToDelimiter );
			// We do not expect a root path separator for externals |-->
			while ( aKey.length() > 1 && aKey.charAt( 0 ) == aToDelimiter ) {
				aKey = aKey.substring( 1 );
			}
			// We do not expect a root path separator for externals <--|
		}
		return aKey;
	}

	/**
	 * To normalized.
	 *
	 * @param aKey the key
	 * @param aToDelimiter the to delimiter
	 * @param aFromDelimiters the from delimiters
	 * @return the string
	 */
	protected static String toNormalized( String aKey, char aToDelimiter, char... aFromDelimiters ) {
		for ( char eDelimiter : aFromDelimiters ) {
			// We do not want a root path separator from externals <--|
			while ( aKey.length() > 1 && ( aKey.charAt( 0 ) == eDelimiter || aKey.charAt( 0 ) == aToDelimiter ) ) {
				aKey = aKey.substring( 1 );
			}
			// We do not want a root path separator from externals <--|
			aKey = aKey.replace( eDelimiter, aToDelimiter );
		}
		return aToDelimiter + aKey;
	}
}
