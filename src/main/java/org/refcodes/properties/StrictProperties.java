// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.properties;

import java.util.Collection;

import org.refcodes.struct.KeyNotFoundRuntimeException;
import org.refcodes.struct.PathMap;
import org.refcodes.struct.Property;
import org.refcodes.struct.Relation;

/**
 * The {@link StrictProperties} extends the {@link Properties} with all the
 * getters to throw a {@link KeyNotFoundRuntimeException} instead of returning
 * null in case the key was not found.
 */
public interface StrictProperties extends Properties {

	/**
	 * {@inheritDoc}
	 */
	@Override
	String get( Object aKey );

	/**
	 * {@inheritDoc}
	 */
	@Override
	default Boolean getBoolean( String aKey ) {
		if ( !containsKey( aKey ) ) {
			throw new KeyNotFoundRuntimeException( aKey, "There is no such element with key <" + aKey + "> found in this instance!" );
		}
		return Properties.super.getBoolean( aKey );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default Byte getByte( String aKey ) {
		if ( !containsKey( aKey ) ) {
			throw new KeyNotFoundRuntimeException( aKey, "There is no such element with key <" + aKey + "> found in this instance!" );
		}
		return Properties.super.getByte( aKey );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default Character getChar( String aKey ) {
		if ( !containsKey( aKey ) ) {
			throw new KeyNotFoundRuntimeException( aKey, "There is no such element with key <" + aKey + "> found in this instance!" );
		}
		return Properties.super.getChar( aKey );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default Double getDouble( String aKey ) {
		if ( !containsKey( aKey ) ) {
			throw new KeyNotFoundRuntimeException( aKey, "There is no such element with key <" + aKey + "> found in this instance!" );
		}
		return Properties.super.getDouble( aKey );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default Float getFloat( String aKey ) {
		if ( !containsKey( aKey ) ) {
			throw new KeyNotFoundRuntimeException( aKey, "There is no such element with key <" + aKey + "> found in this instance!" );
		}
		return Properties.super.getFloat( aKey );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default Integer getInt( String aKey ) {
		if ( !containsKey( aKey ) ) {
			throw new KeyNotFoundRuntimeException( aKey, "There is no such element with key <" + aKey + "> found in this instance!" );
		}
		return Properties.super.getInt( aKey );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default Long getLong( String aKey ) {
		if ( !containsKey( aKey ) ) {
			throw new KeyNotFoundRuntimeException( aKey, "There is no such element with key <" + aKey + "> found in this instance!" );
		}
		return Properties.super.getLong( aKey );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default Short getShort( String aKey ) {
		if ( !containsKey( aKey ) ) {
			throw new KeyNotFoundRuntimeException( aKey, "There is no such element with key <" + aKey + "> found in this instance!" );
		}
		return Properties.super.getShort( aKey );
	}

	// /////////////////////////////////////////////////////////////////////////
	// MUTATOR:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * The Interface MutableStrictProperties.
	 */
	public interface MutableStrictProperties extends StrictProperties, MutableProperties {}

	// /////////////////////////////////////////////////////////////////////////
	// BUILDER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * The Interface StrictPropertiesBuilder.
	 */
	public interface StrictPropertiesBuilder extends MutableStrictProperties, PropertiesBuilder {

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withInsert( Object aObj ) {
			insert( aObj );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withInsert( PathMap<String> aFrom ) {
			insert( aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withInsertBetween( Collection<?> aToPathElements, Object aFrom, Collection<?> aFromPathElements ) {
			insertBetween( aToPathElements, aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withInsertBetween( Collection<?> aToPathElements, PathMap<String> aFrom, Collection<?> aFromPathElements ) {
			insertBetween( aToPathElements, aFrom, aFromPathElements );
			return this;
		}

		//	/**
		//	 * {@inheritDoc}
		//	 */
		//	@Override
		//	default StrictPropertiesBuilder withPut( Object aPath, String aValue ) {
		//		put( toPath( aPath ), aValue );
		//		return this;
		//	}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withInsertBetween( Object aToPath, Object aFrom, Object aFromPath ) {
			insertBetween( aToPath, aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withInsertBetween( Object aToPath, PathMap<String> aFrom, Object aFromPath ) {
			insertBetween( aToPath, aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withInsertBetween( Object[] aToPathElements, Object aFrom, Object[] aFromPathElements ) {
			insertBetween( aToPathElements, aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withInsertBetween( Object[] aToPathElements, PathMap<String> aFrom, Object[] aFromPathElements ) {
			insertBetween( aToPathElements, aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withInsertBetween( String aToPath, Object aFrom, String aFromPath ) {
			insertBetween( aToPath, aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withInsertBetween( String aToPath, PathMap<String> aFrom, String aFromPath ) {
			insertBetween( aToPath, aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withInsertBetween( String[] aToPathElements, Object aFrom, String[] aFromPathElements ) {
			insertBetween( aToPathElements, aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withInsertBetween( String[] aToPathElements, PathMap<String> aFrom, String[] aFromPathElements ) {
			insertBetween( aToPathElements, aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withInsertFrom( Object aFrom, Collection<?> aFromPathElements ) {
			insertFrom( aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withInsertFrom( Object aFrom, Object aFromPath ) {
			insertFrom( aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withInsertFrom( Object aFrom, Object... aFromPathElements ) {
			withInsertFrom( aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withInsertFrom( Object aFrom, String aFromPath ) {
			insertFrom( aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withInsertFrom( Object aFrom, String... aFromPathElements ) {
			insertFrom( aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withInsertFrom( PathMap<String> aFrom, Collection<?> aFromPathElements ) {
			insertFrom( aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withInsertFrom( PathMap<String> aFrom, Object aFromPath ) {
			insertFrom( aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withInsertFrom( PathMap<String> aFrom, Object... aFromPathElements ) {
			withInsertFrom( aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withInsertFrom( PathMap<String> aFrom, String aFromPath ) {
			insertFrom( aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withInsertFrom( PathMap<String> aFrom, String... aFromPathElements ) {
			insertFrom( aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withInsertTo( Collection<?> aToPathElements, Object aFrom ) {
			insertTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withInsertTo( Collection<?> aToPathElements, PathMap<String> aFrom ) {
			insertTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withInsertTo( Object aToPath, Object aFrom ) {
			insertTo( aToPath, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withInsertTo( Object aToPath, PathMap<String> aFrom ) {
			insertTo( aToPath, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withInsertTo( Object[] aToPathElements, Object aFrom ) {
			insertTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withInsertTo( Object[] aToPathElements, PathMap<String> aFrom ) {
			insertTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withInsertTo( String aToPath, Object aFrom ) {
			insertTo( aToPath, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withInsertTo( String aToPath, PathMap<String> aFrom ) {
			insertTo( aToPath, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withInsertTo( String[] aToPathElements, Object aFrom ) {
			insertTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withInsertTo( String[] aToPathElements, PathMap<String> aFrom ) {
			insertTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withMerge( Object aObj ) {
			merge( aObj );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withMerge( PathMap<String> aFrom ) {
			merge( aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withMergeBetween( Collection<?> aToPathElements, Object aFrom, Collection<?> aFromPathElements ) {
			mergeBetween( aToPathElements, aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withMergeBetween( Collection<?> aToPathElements, PathMap<String> aFrom, Collection<?> aFromPathElements ) {
			mergeBetween( aToPathElements, aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withMergeBetween( Object aToPath, Object aFrom, Object aFromPath ) {
			mergeBetween( aToPath, aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withMergeBetween( Object aToPath, PathMap<String> aFrom, Object aFromPath ) {
			mergeBetween( aToPath, aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withMergeBetween( Object[] aToPathElements, Object aFrom, Object[] aFromPathElements ) {
			mergeBetween( aToPathElements, aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withMergeBetween( Object[] aToPathElements, PathMap<String> aFrom, Object[] aFromPathElements ) {
			mergeBetween( aToPathElements, aFromPathElements, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withMergeBetween( String aToPath, Object aFrom, String aFromPath ) {
			mergeBetween( aToPath, aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withMergeBetween( String aToPath, PathMap<String> aFrom, String aFromPath ) {
			mergeBetween( aToPath, aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withMergeBetween( String[] aToPathElements, Object aFrom, String[] aFromPathElements ) {
			mergeBetween( aToPathElements, aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withMergeBetween( String[] aToPathElements, PathMap<String> aFrom, String[] aFromPathElements ) {
			mergeBetween( aToPathElements, aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withMergeFrom( Object aFrom, Collection<?> aFromPathElements ) {
			mergeFrom( aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withMergeFrom( Object aFrom, Object aFromPath ) {
			mergeFrom( aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withMergeFrom( Object aFrom, Object... aFromPathElements ) {
			mergeFrom( aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withMergeFrom( Object aFrom, String aFromPath ) {
			mergeFrom( aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withMergeFrom( Object aFrom, String... aFromPathElements ) {
			mergeFrom( aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withMergeFrom( PathMap<String> aFrom, Collection<?> aFromPathElements ) {
			mergeFrom( aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withMergeFrom( PathMap<String> aFrom, Object aFromPath ) {
			mergeFrom( aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withMergeFrom( PathMap<String> aFrom, Object... aFromPathElements ) {
			mergeFrom( aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withMergeFrom( PathMap<String> aFrom, String aFromPath ) {
			mergeFrom( aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withMergeFrom( PathMap<String> aFrom, String... aFromPathElements ) {
			mergeFrom( aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withMergeTo( Collection<?> aToPathElements, Object aFrom ) {
			mergeTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withMergeTo( Collection<?> aToPathElements, PathMap<String> aFrom ) {
			mergeTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withMergeTo( Object aToPath, Object aFrom ) {
			mergeTo( aToPath, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withMergeTo( Object aToPath, PathMap<String> aFrom ) {
			mergeTo( aToPath, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withMergeTo( Object[] aToPathElements, Object aFrom ) {
			mergeTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withMergeTo( Object[] aToPathElements, PathMap<String> aFrom ) {
			mergeTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withMergeTo( String aToPath, Object aFrom ) {
			mergeTo( aToPath, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withMergeTo( String aToPath, PathMap<String> aFrom ) {
			mergeTo( aToPath, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withMergeTo( String[] aToPathElements, Object aFrom ) {
			mergeTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withMergeTo( String[] aToPathElements, PathMap<String> aFrom ) {
			mergeTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withPut( Collection<?> aPathElements, String aValue ) {
			put( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withPut( Object[] aPathElements, String aValue ) {
			put( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withPut( Property aProperty ) {
			put( aProperty );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withPut( Relation<String, String> aProperty ) {
			put( aProperty );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withPut( String aKey, String aValue ) {
			put( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withPut( String[] aKey, String aValue ) {
			put( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withPutBoolean( Collection<?> aPathElements, Boolean aValue ) {
			putBoolean( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withPutBoolean( Object aKey, Boolean aValue ) {
			putBoolean( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withPutBoolean( Object[] aPathElements, Boolean aValue ) {
			putBoolean( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withPutBoolean( String aKey, Boolean aValue ) {
			putBoolean( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withPutBoolean( String[] aPathElements, Boolean aValue ) {
			putBoolean( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withPutByte( Collection<?> aPathElements, Byte aValue ) {
			putByte( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withPutByte( Object aKey, Byte aValue ) {
			putByte( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withPutByte( Object[] aPathElements, Byte aValue ) {
			putByte( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withPutByte( String aKey, Byte aValue ) {
			putByte( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withPutByte( String[] aPathElements, Byte aValue ) {
			putByte( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withPutChar( Collection<?> aPathElements, Character aValue ) {
			putChar( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withPutChar( Object aKey, Character aValue ) {
			putChar( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withPutChar( Object[] aPathElements, Character aValue ) {
			putChar( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withPutChar( String aKey, Character aValue ) {
			putChar( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withPutChar( String[] aPathElements, Character aValue ) {
			putChar( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default <C> StrictPropertiesBuilder withPutClass( Collection<?> aPathElements, Class<C> aValue ) {
			putClass( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default <C> StrictPropertiesBuilder withPutClass( Object aKey, Class<C> aValue ) {
			putClass( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default <C> StrictPropertiesBuilder withPutClass( Object[] aPathElements, Class<C> aValue ) {
			putClass( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default <C> StrictPropertiesBuilder withPutClass( String aKey, Class<C> aValue ) {
			putClass( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default <C> StrictPropertiesBuilder withPutClass( String[] aPathElements, Class<C> aValue ) {
			putClass( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withPutDirAt( Collection<?> aPathElements, int aIndex, Object aDir ) {
			putDirAt( aPathElements, aIndex, aDir );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withPutDirAt( Collection<?> aPathElements, int aIndex, PathMap<String> aDir ) {
			putDirAt( aPathElements, aIndex, aDir );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withPutDirAt( int aIndex, Object aDir ) {
			putDirAt( aIndex, aDir );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withPutDirAt( int aIndex, PathMap<String> aDir ) {
			putDirAt( aIndex, aDir );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withPutDirAt( Object aPath, int aIndex, Object aDir ) {
			putDirAt( aPath, aIndex, aDir );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withPutDirAt( Object aPath, int aIndex, PathMap<String> aDir ) {
			putDirAt( aPath, aIndex, aDir );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withPutDirAt( Object[] aPathElements, int aIndex, Object aDir ) {
			putDirAt( aPathElements, aIndex, aDir );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withPutDirAt( Object[] aPathElements, int aIndex, PathMap<String> aDir ) {
			putDirAt( aPathElements, aIndex, aDir );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withPutDirAt( String aPath, int aIndex, Object aDir ) {
			putDirAt( aPath, aIndex, aDir );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withPutDirAt( String aPath, int aIndex, PathMap<String> aDir ) {
			putDirAt( aPath, aIndex, aDir );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withPutDirAt( String[] aPathElements, int aIndex, Object aDir ) {
			putDirAt( aPathElements, aIndex, aDir );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withPutDirAt( String[] aPathElements, int aIndex, PathMap<String> aDir ) {
			putDirAt( aPathElements, aIndex, aDir );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withPutDouble( Collection<?> aPathElements, Double aValue ) {
			putDouble( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withPutDouble( Object aKey, Double aValue ) {
			putDouble( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withPutDouble( Object[] aPathElements, Double aValue ) {
			putDouble( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withPutDouble( String aKey, Double aValue ) {
			putDouble( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withPutDouble( String[] aPathElements, Double aValue ) {
			putDouble( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default <E extends Enum<E>> StrictPropertiesBuilder withPutEnum( Collection<?> aPathElements, E aValue ) {
			putEnum( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default <E extends Enum<E>> StrictPropertiesBuilder withPutEnum( Object aKey, E aValue ) {
			putEnum( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default <E extends Enum<E>> StrictPropertiesBuilder withPutEnum( Object[] aPathElements, E aValue ) {
			putEnum( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default <E extends Enum<E>> StrictPropertiesBuilder withPutEnum( String aKey, E aValue ) {
			putEnum( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default <E extends Enum<E>> StrictPropertiesBuilder withPutEnum( String[] aPathElements, E aValue ) {
			putEnum( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withPutFloat( Collection<?> aPathElements, Float aValue ) {
			putFloat( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withPutFloat( Object aKey, Float aValue ) {
			putFloat( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withPutFloat( Object[] aPathElements, Float aValue ) {
			putFloat( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withPutFloat( String aKey, Float aValue ) {
			putFloat( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withPutFloat( String[] aPathElements, Float aValue ) {
			putFloat( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withPutInt( Collection<?> aPathElements, Integer aValue ) {
			putInt( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withPutInt( Object aKey, Integer aValue ) {
			putInt( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withPutInt( Object[] aPathElements, Integer aValue ) {
			putInt( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withPutInt( String aKey, Integer aValue ) {
			putInt( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withPutInt( String[] aPathElements, Integer aValue ) {
			putInt( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withPutLong( Collection<?> aPathElements, Long aValue ) {
			putLong( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withPutLong( Object aKey, Long aValue ) {
			putLong( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withPutLong( Object[] aPathElements, Long aValue ) {
			putLong( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withPutLong( String aKey, Long aValue ) {
			putLong( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withPutLong( String[] aPathElements, Long aValue ) {
			putLong( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withPutShort( Collection<?> aPathElements, Short aValue ) {
			putShort( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withPutShort( Object aKey, Short aValue ) {
			putShort( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withPutShort( Object[] aPathElements, Short aValue ) {
			putShort( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withPutShort( String aKey, Short aValue ) {
			putShort( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withPutShort( String[] aPathElements, Short aValue ) {
			putShort( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withPutString( Collection<?> aPathElements, String aValue ) {
			putString( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withPutString( Object aKey, String aValue ) {
			putString( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withPutString( Object[] aPathElements, String aValue ) {
			putString( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withPutString( String aKey, String aValue ) {
			putString( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withPutString( String[] aPathElements, String aValue ) {
			putString( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withRemoveFrom( Collection<?> aPathElements ) {
			removeFrom( aPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withRemoveFrom( Object aPath ) {
			removeFrom( aPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withRemoveFrom( Object... aPathElements ) {
			removeFrom( aPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withRemoveFrom( String aPath ) {
			removeFrom( aPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withRemoveFrom( String... aPathElements ) {
			removeFrom( aPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default StrictPropertiesBuilder withRemovePaths( String... aPathElements ) {
			removeFrom( aPathElements );
			return this;
		}
	}
}
