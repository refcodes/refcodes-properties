// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.properties;

import java.util.Map;
import java.util.Set;

import org.refcodes.properties.Properties.PropertiesBuilder;

/**
 * The Class AbstractPropertiesBuilderDecorator.
 *
 * @param <T> the generic type
 */
public class AbstractPropertiesBuilderDecorator<T extends PropertiesBuilder> extends AbstractPropertiesDecorator<T> implements PropertiesBuilder {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Decorates the provided {@link ResourceProperties} with additional
	 * behavior or functionality. Changes applied to the provided
	 * {@link ResourceProperties} affect the decorator.
	 * 
	 * @param aProperties The {@link ResourceProperties} to be decorated.
	 */
	public AbstractPropertiesBuilderDecorator( T aProperties ) {
		super( aProperties );
	}

	/**
	 * Make sure to set the getProperties() member variable!.
	 */
	protected AbstractPropertiesBuilderDecorator() {}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void clear() {
		getProperties().clear();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean containsValue( Object aValue ) {
		return getProperties().containsValue( aValue );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Set<Entry<String, String>> entrySet() {
		return getProperties().entrySet();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void insert( Object aFrom ) {
		getProperties().insert( aFrom );

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void insertBetween( String aToPath, Object aFrom, String aFromPath ) {
		getProperties().insertBetween( aToPath, aFrom, aFromPath );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void insertFrom( Object aFrom, String aFromPath ) {
		getProperties().insertFrom( aFrom, aFromPath );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void insertTo( String aToPath, Object aFrom ) {
		getProperties().insertTo( aToPath, aFrom );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void merge( Object aFrom ) {
		getProperties().merge( aFrom );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void mergeBetween( String aToPath, Object aFrom, String aFromPath ) {
		getProperties().mergeBetween( aToPath, aFrom, aFromPath );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void mergeFrom( Object aFrom, String aFromPath ) {
		getProperties().mergeFrom( aFrom, aFromPath );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void mergeTo( String aToPath, Object aFrom ) {
		mergeTo( aToPath, aFrom );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String put( String aKey, String aValue ) {
		return getProperties().put( aKey, aValue );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void putAll( Map<? extends String, ? extends String> aProperties ) {
		getProperties().putAll( aProperties );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String remove( Object aKey ) {
		return getProperties().remove( aKey );
	}

	// /////////////////////////////////////////////////////////////////////////
	// SUB-TYPES:
	// /////////////////////////////////////////////////////////////////////////

	//	/**
	//	 * {@inheritDoc}
	//	 */
	//	@Override
	//	public PropertiesBuilder retrieveFrom( String aFromPath ) {
	//		return getProperties().retrieveFrom( aFromPath );
	//	}
	//
	//	/**
	//	 * {@inheritDoc}
	//	 */
	//	@Override
	//	public PropertiesBuilder retrieveTo( String aToPath ) {
	//		return getProperties().retrieveTo( aToPath );
	//	}
}
