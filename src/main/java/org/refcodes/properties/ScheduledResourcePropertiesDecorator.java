// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.properties;

import java.io.IOException;
import java.text.ParseException;
import java.util.Timer;
import java.util.TimerTask;

import org.refcodes.component.StartException;
import org.refcodes.component.StopException;
import org.refcodes.controlflow.ThreadMode;
import org.refcodes.data.IoPollLoopTime;

/**
 * The {@link ScheduledResourcePropertiesDecorator} decorates
 * {@link ResourceProperties} from which the properties are to be reloaded
 * periodically via {@link #reload(ReloadMode)}. Depending on the invoked
 * constructor, you can define whether orphan removal (see
 * {@link #reload(ReloadMode)}) is to be taken care of and the poll loop time,
 * e.g. in which time interval the properties are to be reloaded.
 */
public class ScheduledResourcePropertiesDecorator extends AbstractResourcePropertiesDecorator<ResourceProperties> implements ScheduledResourceProperties {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private PropertiesScheduler _scheduler;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs the {@link ScheduledResourcePropertiesDecorator} wrapping the
	 * given {@link ResourceProperties} with default settings being a poll loop
	 * time of 10 seconds (as of {@link IoPollLoopTime#NORM} and orphan removal
	 * (as of {@link #reload(ReloadMode)} being called with <code>true</code>).
	 * The scheduling {@link Thread} is started as daemon thread (see
	 * {@link ThreadMode#DAEMON}). Immediately starts polling after construction
	 * of this instance for new properties.
	 *
	 * @param aProperties The properties from which the properties are to be
	 *        reloaded periodically.
	 * @throws IOException thrown in case accessing the resource encountered an
	 *         I/O problem.
	 * @throws ParseException Signals that an error has been reached
	 *         unexpectedly while parsing the data to be loaded.
	 * @throws IllegalStateException in case the attached resource does not
	 *         support reloading.
	 */
	public ScheduledResourcePropertiesDecorator( ResourceProperties aProperties ) throws IOException, ParseException {
		this( aProperties, IoPollLoopTime.NORM.getTimeMillis(), ReloadMode.ORPHAN_REMOVAL );
	}

	/**
	 * Constructs the {@link ScheduledResourcePropertiesDecorator} wrapping the
	 * given {@link ResourceProperties} with setting the given poll loop time
	 * and orphan removal (as of {@link #reload(ReloadMode)} being called with
	 * <code>true</code>). The scheduling {@link Thread} is started as daemon
	 * thread (see {@link ThreadMode#DAEMON}). Immediately starts polling after
	 * construction of this instance for new properties.
	 *
	 * @param aProperties The properties from which the properties are to be
	 *        reloaded periodically.
	 * @param aScheduleTimeMillis The time in milliseconds between polling for
	 *        new properties.
	 * @throws IOException thrown in case accessing the resource encountered an
	 *         I/O problem.
	 * @throws ParseException Signals that an error has been reached
	 *         unexpectedly while parsing the data to be loaded.
	 * @throws IllegalStateException in case the attached resource does not
	 *         support reloading.
	 */
	public ScheduledResourcePropertiesDecorator( ResourceProperties aProperties, int aScheduleTimeMillis ) throws IOException, ParseException {
		this( aProperties, aScheduleTimeMillis, ReloadMode.ORPHAN_REMOVAL );
	}

	/**
	 * Constructs the {@link ScheduledResourcePropertiesDecorator} wrapping the
	 * given {@link ResourceProperties} with setting the given poll loop time
	 * and the given orphan removal strategy (as of {@link #reload(ReloadMode)}
	 * being called with your argument). The scheduling {@link Thread} is
	 * started as daemon thread (see {@link ThreadMode#DAEMON}). Immediately
	 * starts polling after construction of this instance for new properties.
	 *
	 * @param aProperties The properties from which the properties are to be
	 *        reloaded periodically.
	 * @param aScheduleTimeMillis The time in milliseconds between polling for
	 *        new properties.
	 * @param aReloadMode when set to {@link ReloadMode#ORPHAN_REMOVAL}, then
	 *        properties existing in the attached resource but not(!) in the
	 *        {@link Properties} itself are(!) removed. Else properties not
	 *        existing in the attached resource are kept.
	 * @throws IOException thrown in case accessing the resource encountered an
	 *         I/O problem.
	 * @throws ParseException Signals that an error has been reached
	 *         unexpectedly while parsing the data to be loaded.
	 * @throws IllegalStateException in case the attached resource does not
	 *         support reloading.
	 */
	public ScheduledResourcePropertiesDecorator( ResourceProperties aProperties, int aScheduleTimeMillis, ReloadMode aReloadMode ) throws IOException, ParseException {
		this( aProperties, aScheduleTimeMillis, aReloadMode, ThreadMode.DAEMON );
	}

	/**
	 * Constructs the {@link ScheduledResourcePropertiesDecorator} wrapping the
	 * given {@link ResourceProperties} with setting the given poll loop time
	 * and the given orphan removal strategy (as of {@link #reload(ReloadMode)}
	 * being called with your argument). Immediately starts polling after
	 * construction of this instance for new properties.
	 *
	 * @param aProperties The properties from which the properties are to be
	 *        reloaded periodically.
	 * @param aScheduleTimeMillis The time in milliseconds between polling for
	 *        new properties.
	 * @param aReloadMode when set to {@link ReloadMode#ORPHAN_REMOVAL}, then
	 *        properties existing in the attached resource but not(!) in the
	 *        {@link Properties} itself are(!) removed. Else properties not
	 *        existing in the attached resource are kept.
	 * @param aThreadMode The {@link ThreadMode} mode of operation regarding the
	 *        {@link Thread} doing the scheduling job.
	 * @throws IOException thrown in case accessing the resource encountered an
	 *         I/O problem.
	 * @throws ParseException Signals that an error has been reached
	 *         unexpectedly while parsing the data to be loaded.
	 * @throws IllegalStateException in case the attached resource does not
	 *         support reloading.
	 */
	public ScheduledResourcePropertiesDecorator( ResourceProperties aProperties, int aScheduleTimeMillis, ReloadMode aReloadMode, ThreadMode aThreadMode ) throws IOException, ParseException {
		super( aProperties );
		// Initially call reload, thereby probing for exceptions |-->
		reload( aReloadMode );
		// Initially call reload, thereby probing for exceptions <--|
		_scheduler = new PropertiesScheduler( aProperties, aScheduleTimeMillis, aReloadMode, aThreadMode );
		// Here we cannot run into a StartException |-->
		startUnchecked();
		// Here we cannot run into a StartException <--|
	}

	/**
	 * Constructs the {@link ScheduledResourcePropertiesDecorator} wrapping the
	 * given {@link ResourceProperties} with setting the given poll loop time
	 * and orphan removal (as of {@link #reload(ReloadMode)} being called with
	 * <code>true</code>). Immediately starts polling after construction of this
	 * instance for new properties.
	 *
	 * @param aProperties The properties from which the properties are to be
	 *        reloaded periodically.
	 * @param aScheduleTimeMillis The time in milliseconds between polling for
	 *        new properties.
	 * @param aThreadMode The {@link ThreadMode} mode of operation regarding the
	 *        {@link Thread} doing the scheduling job.
	 * @throws IOException thrown in case accessing the resource encountered an
	 *         I/O problem.
	 * @throws ParseException Signals that an error has been reached
	 *         unexpectedly while parsing the data to be loaded.
	 * @throws IllegalStateException in case the attached resource does not
	 *         support reloading.
	 */
	public ScheduledResourcePropertiesDecorator( ResourceProperties aProperties, int aScheduleTimeMillis, ThreadMode aThreadMode ) throws IOException, ParseException {
		this( aProperties, aScheduleTimeMillis, ReloadMode.ORPHAN_REMOVAL, aThreadMode );
	}

	/**
	 * Constructs the {@link ScheduledResourcePropertiesDecorator} wrapping the
	 * given {@link ResourceProperties} with setting the the default poll loop
	 * time of 10 seconds (as of {@link IoPollLoopTime#NORM} and the given
	 * orphan removal strategy (as of {@link #reload(ReloadMode)} being called
	 * with your argument). The scheduling {@link Thread} is started as daemon
	 * thread (see {@link ThreadMode#DAEMON}). Immediately starts polling after
	 * construction of this instance for new properties.
	 *
	 * @param aProperties The properties from which the properties are to be
	 *        reloaded periodically.
	 * @param aReloadMode when set to {@link ReloadMode#ORPHAN_REMOVAL}, then
	 *        properties existing in the attached resource but not(!) in the
	 *        {@link Properties} itself are(!) removed. Else properties not
	 *        existing in the attached resource are kept.
	 * @throws IOException thrown in case accessing the resource encountered an
	 *         I/O problem.
	 * @throws ParseException Signals that an error has been reached
	 *         unexpectedly while parsing the data to be loaded.
	 * @throws IllegalStateException in case the attached resource does not
	 *         support reloading.
	 */
	public ScheduledResourcePropertiesDecorator( ResourceProperties aProperties, ReloadMode aReloadMode ) throws IOException, ParseException {
		this( aProperties, IoPollLoopTime.NORM.getTimeMillis(), aReloadMode );
	}

	/**
	 * Constructs the {@link ScheduledResourcePropertiesDecorator} wrapping the
	 * given {@link ResourceProperties} with setting the the default poll loop
	 * time of 10 seconds (as of {@link IoPollLoopTime#NORM} and the given
	 * orphan removal strategy (as of {@link #reload(ReloadMode)} being called
	 * with your argument). Immediately starts polling after construction of
	 * this instance for new properties.
	 *
	 * @param aProperties The properties from which the properties are to be
	 *        reloaded periodically.
	 * @param aReloadMode when set to {@link ReloadMode#ORPHAN_REMOVAL}, then
	 *        properties existing in the attached resource but not(!) in the
	 *        {@link Properties} itself are(!) removed. Else properties not
	 *        existing in the attached resource are kept.
	 * @param aThreadMode The {@link ThreadMode} mode of operation regarding the
	 *        {@link Thread} doing the scheduling job.
	 * @throws IOException thrown in case accessing the resource encountered an
	 *         I/O problem.
	 * @throws ParseException Signals that an error has been reached
	 *         unexpectedly while parsing the data to be loaded.
	 * @throws IllegalStateException in case the attached resource does not
	 *         support reloading.
	 */
	public ScheduledResourcePropertiesDecorator( ResourceProperties aProperties, ReloadMode aReloadMode, ThreadMode aThreadMode ) throws IOException, ParseException {
		this( aProperties, IoPollLoopTime.NORM.getTimeMillis(), aReloadMode, aThreadMode );
	}

	/**
	 * Constructs the {@link ScheduledResourcePropertiesDecorator} wrapping the
	 * given {@link ResourceProperties} with default settings being a poll loop
	 * time of 10 seconds (as of {@link IoPollLoopTime#NORM} and orphan removal
	 * (as of {@link #reload(ReloadMode)} being called with <code>true</code>).
	 * Immediately starts polling after construction of this instance for new
	 * properties.
	 *
	 * @param aProperties The properties from which the properties are to be
	 *        reloaded periodically.
	 * @param aThreadMode The {@link ThreadMode} mode of operation regarding the
	 *        {@link Thread} doing the scheduling job.
	 * @throws IOException thrown in case accessing the resource encountered an
	 *         I/O problem.
	 * @throws ParseException Signals that an error has been reached
	 *         unexpectedly while parsing the data to be loaded.
	 * @throws IllegalStateException in case the attached resource does not
	 *         support reloading.
	 */
	public ScheduledResourcePropertiesDecorator( ResourceProperties aProperties, ThreadMode aThreadMode ) throws IOException, ParseException {
		this( aProperties, IoPollLoopTime.NORM.getTimeMillis(), ReloadMode.ORPHAN_REMOVAL, aThreadMode );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized void dispose() {
		if ( _scheduler != null ) {
			_scheduler.dispose();
			_scheduler = null;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized void start() throws StartException {
		_scheduler.start();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized void stop() throws StopException {
		_scheduler.stop();

	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * The Class PropertiesScheduler.
	 */
	static class PropertiesScheduler {

		private Timer _timer = null;
		private final ReloadMode _reloadMode;
		private final int _scheduleTimeMillis;
		private ResourceProperties _properties;
		private final ThreadMode _threadMode;

		/**
		 * Instantiates a new properties scheduler.
		 *
		 * @param aProperties the properties
		 * @param aScheduleTimeMillis the schedule time millis
		 * @param aReloadMode the reload mode
		 * @param aThreadMode the thread mode
		 */
		PropertiesScheduler( ResourceProperties aProperties, int aScheduleTimeMillis, ReloadMode aReloadMode, ThreadMode aThreadMode ) {
			_reloadMode = aReloadMode;
			_scheduleTimeMillis = aScheduleTimeMillis;
			_properties = aProperties;
			_threadMode = aThreadMode;
		}

		/**
		 * Dispose.
		 */
		public synchronized void dispose() {
			final Timer theTimer = _timer;
			if ( theTimer != null ) {
				theTimer.cancel();
			}
			_timer = null;
			_properties = null;
		}

		/**
		 * Start.
		 *
		 * @throws StartException the start exception
		 */
		public synchronized void start() throws StartException {
			if ( _timer != null ) {
				throw new StartException( "Unable to start the scheduler which has not been stopped before!" );
			}
			_timer = new Timer( _threadMode == ThreadMode.DAEMON ? true : false );
			_timer.schedule( new TimerTask() {

				@Override
				public void run() {

					try {
						_properties.reload( _reloadMode );
					}
					catch ( IllegalStateException | IOException | ParseException ignore ) {
						/* Ignore, we might get the resource back later */
					}

				}
			}, _scheduleTimeMillis, _scheduleTimeMillis );
		}

		/**
		 * Stop.
		 *
		 * @throws StopException the stop exception
		 */
		public synchronized void stop() throws StopException {
			if ( _timer == null ) {
				throw new StopException( "Unable to stop the scheduler which has not been started again!" );
			}
			_timer.cancel();
			_timer = null;

		}
	}
}
