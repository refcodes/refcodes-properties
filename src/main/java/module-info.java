module org.refcodes.properties {
	requires org.refcodes.data;
	requires transitive org.refcodes.component;
	requires transitive org.refcodes.controlflow;
	requires transitive org.refcodes.exception;
	requires transitive org.refcodes.mixin;
	requires transitive org.refcodes.runtime;
	requires transitive org.refcodes.struct;
	requires transitive org.refcodes.struct.ext.factory;

	exports org.refcodes.properties;
}
