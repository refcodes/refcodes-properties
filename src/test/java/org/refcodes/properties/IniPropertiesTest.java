// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.properties;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.refcodes.properties.ResourceProperties.ResourcePropertiesBuilder;
import org.refcodes.runtime.ConfigLocator;
import org.refcodes.runtime.SystemProperty;

/**
 * The Class IniPropertiesTest.
 */
public class IniPropertiesTest {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final String[][] EXPECTED = new String[][] { new String[] { "/club/funcodes/runtimelogger/logger", "org.refcodes.logger.alt.cli.ConsoleLoggerSingleton" }, new String[] { "/club/funcodes/runtimelogger/logPriority", "INFO" }, new String[] { "/club/funcodes/runtimelogger", "org.refcodes.logger.RuntimeLoggerImpl" }, new String[] { "/org/refcodes/runtimelogger/logger", "org.refcodes.logger.alt.cli.ConsoleLoggerSingleton" }, new String[] { "/org/refcodes/runtimelogger/logPriority", "INFO" }, new String[] { "/org/refcodes/runtimelogger", "org.refcodes.logger.RuntimeLoggerImpl" }, new String[] { "/root/runtimelogger/logger", "org.refcodes.logger.alt.cli.ConsoleLoggerSingleton" }, new String[] { "/root/runtimelogger/logPriority", "INFO" }, new String[] { "/root/runtimelogger", "org.refcodes.logger.RuntimeLoggerImpl" } };

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testIniProperties() throws IOException, ParseException {
		final ResourceProperties theProperties = new TomlProperties( "application.ini", ConfigLocator.APPLICATION_ALL );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			for ( String eKey : theProperties.keySet() ) {
				System.out.println( eKey + "=" + theProperties.get( eKey ) );
			}
		}
		for ( String[] aEXPECTED : EXPECTED ) {
			assertEquals( aEXPECTED[1], theProperties.get( aEXPECTED[0] ) );
		}
		assertEquals( EXPECTED.length, theProperties.size() );
	}

	@Test
	public void testEdgeCase() throws IOException, ParseException {
		final ResourcePropertiesBuilder theBuilder = new TomlPropertiesBuilder( "edge_case.ini", ConfigLocator.APPLICATION_ALL );
		final List<String> theKeys = new ArrayList<>( theBuilder.keySet() );
		Collections.sort( theKeys );
		for ( String eKey : theKeys ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( eKey + " = " + theBuilder.get( eKey ) );
				// assertEquals( eKey, theBuilder.get( eKey ) );
			}
		}
		assertEquals( 21, theBuilder.size() );
	}
}
