// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.properties;

import static org.junit.jupiter.api.Assertions.*;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;

import org.junit.jupiter.api.Test;
import org.refcodes.properties.ResourceProperties.ResourcePropertiesBuilder;
import org.refcodes.runtime.SystemProperty;

public abstract class AbstractResourcePropertiesTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testSaveToLoadFromFlat() throws IOException, ParseException {
		final ResourcePropertiesBuilder thePropertiesBuilder = toResourcePropertiesBuilder();
		thePropertiesBuilder.put( "firstName", "Nolan" );
		thePropertiesBuilder.put( "lastName", "Bushnell" );
		testSaveTo( thePropertiesBuilder );
	}

	@Test
	public void testSaveToLoadFromFlatAndNested() throws IOException, ParseException {
		final ResourcePropertiesBuilder thePropertiesBuilder = toResourcePropertiesBuilder();
		thePropertiesBuilder.put( "firstName", "Nolan" );
		thePropertiesBuilder.put( "lastName", "Bushnell" );
		thePropertiesBuilder.put( "database/url", "jdbc://some/db/url" );
		thePropertiesBuilder.put( "database/user", "admin" );
		thePropertiesBuilder.put( "database/password", "secret" );
		thePropertiesBuilder.put( "remote/url", "http://some/remote/connection" );
		testSaveTo( thePropertiesBuilder );
	}

	@Test
	public void testSaveToLoadFromNested() throws IOException, ParseException {
		final ResourcePropertiesBuilder thePropertiesBuilder = toResourcePropertiesBuilder();
		thePropertiesBuilder.put( "database/url", "jdbc://some/db/url" );
		thePropertiesBuilder.put( "database/user", "admin" );
		thePropertiesBuilder.put( "database/password", "secret" );
		thePropertiesBuilder.put( "remote/url", "http://some/remote/connection" );
		testSaveTo( thePropertiesBuilder );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	protected abstract ResourcePropertiesBuilder toResourcePropertiesBuilder();

	protected abstract ResourceProperties toResourceProperties( InputStream aInputStream ) throws IOException, ParseException;

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private void testSaveTo( ResourcePropertiesBuilder aBuilder ) throws IOException, ParseException {
		final ByteArrayOutputStream theOutputStream = new ByteArrayOutputStream();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "### PROPERTIES ###" );
			}
			for ( String eKey : aBuilder.keySet() ) {
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( eKey + "=" + aBuilder.get( eKey ) );
				}
			}
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "--------------------------------------------------------------------------------" );
			}
		}
		aBuilder.saveTo( theOutputStream, "My test properties" );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "### SAVE-TO ###" );
			}
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( theOutputStream.toString() );
			}
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "--------------------------------------------------------------------------------" );
			}
		}
		// WTH do the Java Properties not UNSECAPE ?!? |-->
		final String theJavaPropertiesHack = theOutputStream.toString().replaceAll( "\\\\:", ":" );
		// WTH do the Java Properties not UNSECAPE ?!? <--|
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "### SERIALIZED ###" );
			}
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( aBuilder.toSerialized() );
			}
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "--------------------------------------------------------------------------------" );
			}
		}
		final ByteArrayInputStream theInputStream = new ByteArrayInputStream( theJavaPropertiesHack.getBytes() );
		final ResourceProperties theProperties = toResourceProperties( theInputStream );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "### LOAD-FROM ###" );
			}
			for ( String eKey : theProperties.keySet() ) {
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( eKey + "=" + theProperties.get( eKey ) );
				}
			}
		}
		assertEquals( aBuilder.size(), theProperties.size() );
		for ( String eKey : aBuilder.keySet() ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( eKey + " = " + aBuilder.get( eKey ) + " vs. " + theProperties.get( eKey ) );
			}
			assertEquals( aBuilder.get( eKey ), theProperties.get( eKey ) );
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "================================================================================" );
			}
		}
	}
}
