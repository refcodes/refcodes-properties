// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.properties;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.refcodes.properties.Properties.PropertiesBuilder;
import org.refcodes.struct.KeyNotFoundRuntimeException;

public class StrictPropertiesTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testStrictProperty() {
		final PropertiesBuilder theBuilder = new PropertiesBuilderImpl();
		theBuilder.put( "/company/name", "Atari" );
		theBuilder.putInt( "/company/height", 12 );
		final StrictProperties theProperties = new StrictPropertiesDecorator( theBuilder );
		final String theName = theProperties.get( "/company/name" );
		assertEquals( "Atari", theName );
		final Integer theHeight = theProperties.getInt( "/company/height" );
		assertEquals( (Integer) 12, theHeight );
		try {
			theProperties.get( "/company/length" );
			fail( "Expecting an exception to be thrown" );
		}
		catch ( KeyNotFoundRuntimeException e ) {
			/* expected */
		}
		try {
			theProperties.getInt( "/company/length" );
			fail( "Expecting an exception to be thrown" );
		}
		catch ( KeyNotFoundRuntimeException e ) {
			/* expected */
		}
	}
}
