// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.properties;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import org.junit.jupiter.api.Test;
import org.refcodes.properties.Properties.PropertiesBuilder;
import org.refcodes.runtime.SystemProperty;

public class ResourcePropertiesTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testJavaProperties() throws IOException, ParseException {
		final Properties theProperties = new JavaPropertiesBuilder().withSeekFrom( "application.config" );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			final List<String> theKeys = new ArrayList<>( theProperties.keySet() );
			Collections.sort( theKeys );
			for ( String eKey : theKeys ) {
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( eKey + " := " + theProperties.get( eKey ) );
				}
			}
		}
		assertEquals( "Nolan", theProperties.get( "/user/firstName" ) );
		assertEquals( "Bushnell", theProperties.get( "/user/lastName" ) );
		assertEquals( "Nolan", theProperties.get( "user/firstName" ) );
		assertEquals( "Bushnell", theProperties.get( "user/lastName" ) );
	}

	@Test
	public void testComplexType() throws IOException, ParseException {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "JSON" );
		}
		final JsonPropertiesBuilder theJsonProperties = new JsonPropertiesBuilder( getClass(), "/ComplexType.json" );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			for ( String eKey : theJsonProperties.sortedKeys() ) {
				System.out.println( eKey + " = " + theJsonProperties.get( eKey ) );
			}
		}
		final ComplexType theJsonType = theJsonProperties.toType( ComplexType.class );
		assertEquals( theJsonType.getLocale(), Locale.GERMANY );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "\nTYPE" );
		}
		final PropertiesBuilder theTypeProperties = new PropertiesBuilderImpl( theJsonType );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			for ( String eKey : theTypeProperties.sortedKeys() ) {
				System.out.println( eKey + " = " + theTypeProperties.get( eKey ) );
			}
		}
		final ComplexType theTypeType = theTypeProperties.toType( ComplexType.class );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "theJsonType.getLocale() = " + theJsonType.getLocale() );
			System.out.println( "theTypeType.getLocale() = " + theTypeType.getLocale() );
		}
		assertEquals( theJsonProperties.size(), theTypeProperties.size() );
		assertEquals( theJsonType.getLocale(), theTypeType.getLocale() );
		assertEquals( theJsonType.getTimestamp(), theTypeType.getTimestamp() );
		assertEquals( theJsonType.getLocalTimestamp(), theTypeType.getLocalTimestamp() );
	}
}
