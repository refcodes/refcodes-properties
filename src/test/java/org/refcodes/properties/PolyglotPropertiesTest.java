// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.properties;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.refcodes.data.FilenameExtension;
import org.refcodes.properties.PolyglotProperties.PolyglotPropertiesFactory;
import org.refcodes.runtime.ConfigLocator;
import org.refcodes.runtime.SystemProperty;

public class PolyglotPropertiesTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testPolyglotProperties1() throws IOException, ParseException {
		PolyglotProperties eProperties;
		final String[] theFileEndings = new String[] { FilenameExtension.JSON.getFilenameSuffix(), FilenameExtension.PROPERTIES.getFilenameSuffix(), FilenameExtension.TOML.getFilenameSuffix(), FilenameExtension.XML.getFilenameSuffix(), FilenameExtension.YAML.getFilenameSuffix() };
		for ( String eEnding : theFileEndings ) {
			eProperties = new PolyglotProperties( "polyglot" + eEnding, ConfigLocator.APPLICATION_ALL );
			assertEquals( eEnding, eProperties.get( "polyglot/ending" ) );
		}
	}

	@Test
	public void testPolyglotProperties2() throws IOException, ParseException {
		final PolyglotProperties theProperties = new PolyglotProperties( "polyglot", ConfigLocator.APPLICATION_ALL );
		assertEquals( ".toml", theProperties.get( "polyglot/ending" ) );
	}

	@Test
	public void testFileExtenisons() throws IOException, ParseException {
		final String[] theExpected = new String[] { ".toml", ".ini", ".yaml", ".xml", ".json", ".properties" };
		final List<String> theExtensions = new ArrayList<>();
		final PolyglotPropertiesFactory theFactory = new PolyglotPropertiesFactory();
		for ( String eExtension : theFactory.getFilenameSuffixes() ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( eExtension );
			}
			theExtensions.add( eExtension );
		}
		assertEquals( theExpected.length, theExtensions.size() );
		for ( String eExtension : theExpected ) {
			assertTrue( theExtensions.contains( eExtension ) );
		}
	}

	@Test
	public void testValues() throws IOException, ParseException {
		final PolyglotProperties theProperties = new PolyglotProperties( "configuration", ConfigLocator.APPLICATION_ALL );
		final Collection<String> theValues = theProperties.values();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			for ( String eValue : theValues ) {
				System.out.println( eValue );
			}
		}
		assertEquals( 6, theValues.size() );
	}

	@Test
	public void testDocumentRootPreserve() throws IOException, ParseException {
		final PolyglotProperties theProperties = new PolyglotProperties( "donaBaileyEnvelope.xml", ConfigLocator.APPLICATION_ALL, DocumentMetricsImpl.builder().withEnvelope( true ).build() );
		final List<String> theKeys = theProperties.sortedKeys();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			for ( String eKey : theKeys ) {
				System.out.println( eKey + " = " + theProperties.get( eKey ) );
			}
		}
		assertEquals( 4, theProperties.size() );
		assertEquals( "Atari", theProperties.get( "/envelope/company" ) );
		assertEquals( "Centipede", theProperties.get( "/envelope/game" ) );
		assertEquals( "Dona", theProperties.get( "/envelope/identity/firstName" ) );
		assertEquals( "Bailey", theProperties.get( "/envelope/identity/lastName" ) );
	}

	@Test
	public void testDocumentRootSkip() throws IOException, ParseException {
		final PolyglotProperties theProperties = new PolyglotProperties( "donaBaileyEnvelope.xml", ConfigLocator.APPLICATION_ALL, DocumentMetricsImpl.builder().withEnvelope( false ).build() );
		final List<String> theKeys = theProperties.sortedKeys();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			for ( String eKey : theKeys ) {
				System.out.println( eKey + " = " + theProperties.get( eKey ) );
			}
		}
		assertEquals( 4, theProperties.size() );
		assertEquals( "Atari", theProperties.get( "/company" ) );
		assertEquals( "Centipede", theProperties.get( "/game" ) );
		assertEquals( "Dona", theProperties.get( "/identity/firstName" ) );
		assertEquals( "Bailey", theProperties.get( "/identity/lastName" ) );
	}

	@Test
	public void testDelimiter1() throws IOException, ParseException {
		final PolyglotProperties theProperties = new PolyglotProperties( "donaBaileyDelimiter1.ini", ConfigLocator.APPLICATION_ALL, new DocumentMetricsImpl( '/' ) );
		final List<String> theKeys = theProperties.sortedKeys();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			for ( String eKey : theKeys ) {
				System.out.println( eKey + " = " + theProperties.get( eKey ) );
			}
		}
		assertEquals( 4, theProperties.size() );
		assertEquals( "Atari", theProperties.get( "/company" ) );
		assertEquals( "Centipede", theProperties.get( "/game" ) );
		assertEquals( "Dona", theProperties.get( "/identity/firstName" ) );
		assertEquals( "Bailey", theProperties.get( "/identity/lastName" ) );
	}

	@Test
	public void testDelimiter1Other() throws IOException, ParseException {
		final PolyglotProperties theProperties = new PolyglotProperties( "donaBaileyDelimiter1.ini", ConfigLocator.APPLICATION_ALL, new DocumentMetricsImpl( '|' ) ); // As the "donaBaileyDelimiter1.ini" already uses '/' we are fine here no matter which supported delimiter we provide! 
		final List<String> theKeys = theProperties.sortedKeys();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			for ( String eKey : theKeys ) {
				System.out.println( eKey + " = " + theProperties.get( eKey ) );
			}
		}
		assertEquals( 4, theProperties.size() );
		assertEquals( "Atari", theProperties.get( "/company" ) );
		assertEquals( "Centipede", theProperties.get( "/game" ) );
		assertEquals( "Dona", theProperties.get( "/identity/firstName" ) );
		assertEquals( "Bailey", theProperties.get( "/identity/lastName" ) );
	}

	@Test
	public void testDelimiter2() throws IOException, ParseException {
		final PolyglotProperties theProperties = new PolyglotProperties( "donaBaileyDelimiter2.ini", ConfigLocator.APPLICATION_ALL, new DocumentMetricsImpl( '\\' ) );
		final List<String> theKeys = theProperties.sortedKeys();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			for ( String eKey : theKeys ) {
				System.out.println( eKey + " = " + theProperties.get( eKey ) );
			}
		}
		assertEquals( 4, theProperties.size() );
		assertEquals( "Atari", theProperties.get( "/company" ) );
		assertEquals( "Centipede", theProperties.get( "/game" ) );
		assertEquals( "Dona", theProperties.get( "/identity/firstName" ) );
		assertEquals( "Bailey", theProperties.get( "/identity/lastName" ) );
	}

	@Test
	public void testDelimiter2Other() throws IOException, ParseException {
		final PolyglotProperties theProperties = new PolyglotProperties( "donaBaileyDelimiter2.ini", ConfigLocator.APPLICATION_ALL, new DocumentMetricsImpl( '/' ) );
		final List<String> theKeys = theProperties.sortedKeys();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			for ( String eKey : theKeys ) {
				System.out.println( eKey + " = " + theProperties.get( eKey ) );
			}
		}
		assertEquals( 4, theProperties.size() );
		assertEquals( "Atari", theProperties.get( "/company" ) );
		assertEquals( "Centipede", theProperties.get( "/game" ) );
		assertEquals( "Dona", theProperties.get( "/identity\\firstName" ) );
		assertEquals( "Bailey", theProperties.get( "/identity\\lastName" ) );
	}

	@Test
	public void testDelimiter3() throws IOException, ParseException {
		final PolyglotProperties theProperties = new PolyglotProperties( "donaBaileyDelimiter3.ini", ConfigLocator.APPLICATION_ALL, new DocumentMetricsImpl( '\\', '|' ) );
		final List<String> theKeys = theProperties.sortedKeys();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			for ( String eKey : theKeys ) {
				System.out.println( eKey + " = " + theProperties.get( eKey ) );
			}
		}
		assertEquals( 4, theProperties.size() );
		assertEquals( "Atari", theProperties.get( "/company" ) );
		assertEquals( "Centipede", theProperties.get( "/game" ) );
		assertEquals( "Dona", theProperties.get( "/identity/firstName" ) );
		assertEquals( "Bailey", theProperties.get( "/identity/lastName" ) );
	}

	@Test
	public void testDelimiter3Other() throws IOException, ParseException {
		final PolyglotProperties theProperties = new PolyglotProperties( "donaBaileyDelimiter3.ini", ConfigLocator.APPLICATION_ALL, new DocumentMetricsImpl( '/' ) );
		final List<String> theKeys = theProperties.sortedKeys();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			for ( String eKey : theKeys ) {
				System.out.println( eKey + " = " + theProperties.get( eKey ) );
			}
		}
		assertEquals( 4, theProperties.size() );
		assertEquals( "Atari", theProperties.get( "/company" ) );
		assertEquals( "Centipede", theProperties.get( "/\\game" ) );
		assertEquals( "Dona", theProperties.get( "/|identity\\firstName" ) );
		assertEquals( "Bailey", theProperties.get( "/\\identity|lastName" ) );
	}
}
