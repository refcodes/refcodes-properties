// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.properties;

import static org.junit.jupiter.api.Assertions.*;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import org.junit.jupiter.api.Test;
import org.refcodes.exception.MarshalException;
import org.refcodes.exception.UnmarshalException;
import org.refcodes.properties.ResourceProperties.ResourcePropertiesBuilder;
import org.refcodes.runtime.SystemProperty;

public class JsonPropertiesTest extends AbstractResourcePropertiesTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testVerifyEdgeCase() throws MarshalException, UnmarshalException, IOException, ParseException {
		final JsonPropertiesBuilder theDataStructure = new JsonPropertiesBuilder();
		theDataStructure.put( "aaa/aaa_111", "aaa_111" );
		theDataStructure.put( "aaa/aaa_222", "aaa_222" );
		theDataStructure.put( "aaa/aaa_333", "aaa_333" );
		theDataStructure.put( "bbb/bbb_111", "bbb_111" );
		theDataStructure.put( "bbb/bbb_222", "bbb_222" );
		theDataStructure.put( "bbb/bbb_333", "bbb_333" );
		theDataStructure.put( "ccc/ccc_111", "ccc_111" );
		theDataStructure.put( "ccc/ccc_222", "ccc_222" );
		theDataStructure.put( "ccc/ccc_333", "ccc_333" );
		final String theMarshaled = theDataStructure.toSerialized();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theMarshaled );
		}
		final JsonPropertiesBuilder theUnmarshaled = new JsonPropertiesBuilder();
		final InputStream theUnmarshaledStream = new ByteArrayInputStream( theMarshaled.getBytes( StandardCharsets.UTF_8 ) );
		theUnmarshaled.loadFrom( theUnmarshaledStream );
		assertEquals( theDataStructure.size(), theUnmarshaled.size() );
		for ( String eKey : theDataStructure.keySet() ) {
			assertEquals( theDataStructure.get( eKey ), theUnmarshaled.get( eKey ) );
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	@Override
	protected ResourcePropertiesBuilder toResourcePropertiesBuilder() {
		return new JsonPropertiesBuilder();
	}

	@Override
	protected ResourceProperties toResourceProperties( InputStream aInputStream ) throws IOException, ParseException {
		return new JsonProperties( aInputStream );
	}
}
