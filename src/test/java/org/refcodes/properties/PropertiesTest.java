// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.properties;

import static org.junit.jupiter.api.Assertions.*;

import java.io.File;
import java.math.BigDecimal;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.refcodes.properties.Properties.PropertiesBuilder;
import org.refcodes.runtime.SystemProperty;

public class PropertiesTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testClass() {
		final Class<?> theClass = UUID.class;
		final PropertiesBuilder theMap = new PropertiesBuilderImpl( theClass );
		Object eValue;
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			for ( String eKey : theMap.sortedKeys() ) {
				eValue = theMap.get( eKey );
				System.out.println( eKey + ": " + eValue + ( eValue != null ? " (" + eValue.getClass().getSimpleName() + ")" : "" ) );
			}
		}
		final Class<?> theResult = theMap.toType( Class.class );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theResult );
		}
		assertEquals( theClass, theResult );
	}

	@Test
	public void testClassArray() {
		final Class<?>[] theClasses = new Class[] { UUID.class, String.class, Boolean.class };
		final PropertiesBuilder theMap = new PropertiesBuilderImpl( theClasses );
		Object eValue;
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			for ( String eKey : theMap.sortedKeys() ) {
				eValue = theMap.get( eKey );
				System.out.println( eKey + ": " + eValue + ( eValue != null ? " (" + eValue.getClass().getSimpleName() + ")" : "" ) );
			}
		}
		final Class<?>[] theResult = theMap.toType( Class[].class );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			for ( Class<?> aTheResult : theResult ) {
				System.out.println( aTheResult );
			}
		}
		assertArrayEquals( theClasses, theResult );
	}

	@Test
	public void testUUID() {
		final UUID theUUID = UUID.randomUUID();
		final PropertiesBuilder theMap = new PropertiesBuilderImpl( theUUID );
		Object eValue;
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			for ( String eKey : theMap.sortedKeys() ) {
				eValue = theMap.get( eKey );
				System.out.println( eKey + ": " + eValue + ( eValue != null ? " (" + eValue.getClass().getSimpleName() + ")" : "" ) );
			}
		}
		final UUID theResult = theMap.toType( UUID.class );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theResult );
		}
		assertEquals( theUUID, theResult );
	}

	@Test
	public void testUUIDArray() {
		final UUID[] theUUIDs = new UUID[] { UUID.randomUUID(), UUID.randomUUID(), UUID.randomUUID() };
		final PropertiesBuilder theMap = new PropertiesBuilderImpl( theUUIDs );
		Object eValue;
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			for ( String eKey : theMap.sortedKeys() ) {
				eValue = theMap.get( eKey );
				System.out.println( eKey + ": " + eValue + ( eValue != null ? " (" + eValue.getClass().getSimpleName() + ")" : "" ) );
			}
		}
		final UUID[] theResult = theMap.toType( UUID[].class );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			for ( UUID aTheResult : theResult ) {
				System.out.println( aTheResult );
			}
		}
		assertArrayEquals( theUUIDs, theResult );
	}

	@Test
	public void testAsArray1() {
		final PropertiesBuilder theProperties = new PropertiesBuilderImpl();
		theProperties.put( "array", "1, 2, 3, 4, 5" );
		final String[] theArray = theProperties.asArray( "array", ',' );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( Arrays.toString( theArray ) );
		}
		assertEquals( 5, theArray.length );
	}

	@Test
	public void testAsArray2() {
		final PropertiesBuilder theProperties = new PropertiesBuilderImpl();
		theProperties.put( "array", "1, 2, 3, 4, 5" );
		final String[] theArray = theProperties.asArray( "array", ':' );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( Arrays.toString( theArray ) );
		}
		assertEquals( 1, theArray.length );
	}

	@Test
	public void testAsArray3() {
		final PropertiesBuilder theProperties = new PropertiesBuilderImpl();
		theProperties.put( "array", "1,, 3, 4, 5" );
		final String[] theArray = theProperties.asArray( "array", ',' );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( Arrays.toString( theArray ) );
		}
		assertEquals( 5, theArray.length );
	}

	@Test
	public void testAsBooleanArray1() {
		final PropertiesBuilder theProperties = new PropertiesBuilderImpl();
		theProperties.put( "array", "true, false, true, false, true" );
		final boolean[] theArray = theProperties.asBooleanArray( "array", ',' );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( Arrays.toString( theArray ) );
		}
		assertEquals( true, theArray[0] );
		assertEquals( false, theArray[1] );
		assertEquals( true, theArray[2] );
		assertEquals( false, theArray[3] );
		assertEquals( true, theArray[4] );
		assertEquals( 5, theArray.length );
	}

	@Test
	public void testAsBooleanArray2() {
		final PropertiesBuilder theProperties = new PropertiesBuilderImpl();
		theProperties.put( "array", "true, wahr, true, false, true" );
		try {
			theProperties.asBooleanArray( "array", ',' );
			fail( "Expecting not to get a boolean array" );
		}
		catch ( NumberFormatException expected ) {
			/* expected */
		}
	}

	@Test
	public void testAsIntegerArray1() {
		final PropertiesBuilder theProperties = new PropertiesBuilderImpl();
		theProperties.put( "array", "1, 2, 3, 4, 5" );
		final int[] theArray = theProperties.asIntArray( "array", ',' );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( Arrays.toString( theArray ) );
		}
		assertEquals( 1, theArray[0] );
		assertEquals( 2, theArray[1] );
		assertEquals( 3, theArray[2] );
		assertEquals( 4, theArray[3] );
		assertEquals( 5, theArray[4] );
		assertEquals( 5, theArray.length );
	}

	@Test
	public void testAsIntegerArray2() {
		final PropertiesBuilder theProperties = new PropertiesBuilderImpl();
		theProperties.put( "array", "1, 2, 3, 4, five" );
		try {
			theProperties.asIntArray( "array", ',' );
			fail( "Expecting not to get a boolean array" );
		}
		catch ( NumberFormatException expected ) {
			/* expected */
		}
	}

	@Test
	public void testAsByteArray1() {
		final PropertiesBuilder theProperties = new PropertiesBuilderImpl();
		theProperties.put( "array", "1, 2, 3, 4, 5" );
		final byte[] theArray = theProperties.asByteArray( "array", ',' );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( Arrays.toString( theArray ) );
		}
		assertEquals( 1, theArray[0] );
		assertEquals( 2, theArray[1] );
		assertEquals( 3, theArray[2] );
		assertEquals( 4, theArray[3] );
		assertEquals( 5, theArray[4] );
		assertEquals( 5, theArray.length );
	}

	@Test
	public void testAsByteArray2() {
		final PropertiesBuilder theProperties = new PropertiesBuilderImpl();
		theProperties.put( "array", "1, 2, 3, 4, five" );
		try {
			theProperties.asByteArray( "array", ',' );
			fail( "Expecting not to get a boolean array" );
		}
		catch ( NumberFormatException expected ) {
			/* expected */
		}
	}

	@Test
	public void testAsCharacterArray1() {
		final PropertiesBuilder theProperties = new PropertiesBuilderImpl();
		theProperties.put( "array", "1, 2, 3, 4, 5" );
		final char[] theArray = theProperties.asCharArray( "array", ',' );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( Arrays.toString( theArray ) );
		}
		assertEquals( '1', theArray[0] );
		assertEquals( '2', theArray[1] );
		assertEquals( '3', theArray[2] );
		assertEquals( '4', theArray[3] );
		assertEquals( '5', theArray[4] );
		assertEquals( 5, theArray.length );
	}

	@Test
	public void testAsCharacterArray2() {
		final PropertiesBuilder theProperties = new PropertiesBuilderImpl();
		theProperties.put( "array", "1, 2, 3, 4, five" );
		try {
			theProperties.asCharArray( "array", ',' );
			fail( "Expecting not to get a boolean array" );
		}
		catch ( NumberFormatException expected ) {
			/* expected */
		}
	}

	@Test
	public void testAsFloatArray1() {
		final PropertiesBuilder theProperties = new PropertiesBuilderImpl();
		theProperties.put( "array", "1.1, 2.2, 3.3, 4.4, 5.5" );
		final float[] theArray = theProperties.asFloatArray( "array", ',' );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( Arrays.toString( theArray ) );
		}
		final MathContext theContext = new MathContext( 2 );
		assertEquals( 1.1, new BigDecimal( theArray[0], theContext ).doubleValue() );
		assertEquals( 2.2, new BigDecimal( theArray[1], theContext ).doubleValue() );
		assertEquals( 3.3, new BigDecimal( theArray[2], theContext ).doubleValue() );
		assertEquals( 4.4, new BigDecimal( theArray[3], theContext ).doubleValue() );
		assertEquals( 5.5, new BigDecimal( theArray[4], theContext ).doubleValue() );
		assertEquals( 5, theArray.length );
	}

	@Test
	public void testAsFloatArray2() {
		final PropertiesBuilder theProperties = new PropertiesBuilderImpl();
		theProperties.put( "array", "1.1, 2.2, 3.3, 4.4, five" );
		try {
			theProperties.asFloatArray( "array", ',' );
			fail( "Expecting not to get a boolean array" );
		}
		catch ( NumberFormatException expected ) {
			/* expected */
		}
	}

	@Test
	public void testAsMap1() {
		final PropertiesBuilder theProperties = new PropertiesBuilderImpl();
		final Map<String, String> theMap = new HashMap<>();
		theMap.put( "Hello", "value0" );
		theMap.put( "World", "value1" );
		theMap.put( "!", "value2" );
		theProperties.insert( theMap );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			for ( String eKey : theProperties.sortedKeys() ) {
				System.out.println( "!->" + eKey + "=" + theProperties.get( eKey ) );
			}
		}
		final Map<?, ?> theType = theProperties.toType( Map.class );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			for ( Object eKey : theType.keySet() ) {
				System.out.println( "?->" + eKey + "=" + theType.get( eKey ) );
			}
		}
		assertEquals( "value0", theType.get( "Hello" ) );
		assertEquals( "value1", theType.get( "World" ) );
		assertEquals( "value2", theType.get( "!" ) );
	}

	@Test
	public void testAsMap2() {
		final PropertiesBuilder theProperties = new PropertiesBuilderImpl();
		final Map<String, String> theMap = new HashMap<>();
		theMap.put( "0", "value0" );
		theMap.put( "1", "value1" );
		theMap.put( "2", "value2" );
		theProperties.insert( theMap );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			for ( String eKey : theProperties.sortedKeys() ) {
				System.out.println( "!->" + eKey + "=" + theProperties.get( eKey ) );
			}
		}
		final Map<?, ?> theType = theProperties.toType( Map.class );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			for ( Object eKey : theType.keySet() ) {
				System.out.println( "?->" + eKey + "=" + theType.get( eKey ) );
			}
		}
		assertEquals( "value0", theType.get( "0" ) );
		assertEquals( "value1", theType.get( "1" ) );
		assertEquals( "value2", theType.get( "2" ) );
	}

	@Test
	public void testAsMap3() {
		final PropertiesBuilder theProperties = new PropertiesBuilderImpl();
		final Map<String, String> theMap = new HashMap<>();
		theMap.put( "map/Hello", "value0" );
		theMap.put( "map/World", "value1" );
		theMap.put( "map/!", "value2" );
		theProperties.insert( theMap );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			for ( String eKey : theProperties.sortedKeys() ) {
				System.out.println( "!->" + eKey + "=" + theProperties.get( eKey ) );
			}
		}
		final Mapper theType = theProperties.toType( Mapper.class );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			for ( String eKey : theType.getMap().keySet() ) {
				System.out.println( "?->" + eKey + "=" + theType.getMap().get( eKey ) );
			}
		}
		assertEquals( "value0", theType.getMap().get( "Hello" ) );
		assertEquals( "value1", theType.getMap().get( "World" ) );
		assertEquals( "value2", theType.getMap().get( "!" ) );
	}

	@Test
	public void testAsMap4() {
		final PropertiesBuilder theProperties = new PropertiesBuilderImpl();
		final Map<String, String> theMap = new HashMap<>();
		theMap.put( "map/0", "value0" );
		theMap.put( "map/1", "value1" );
		theMap.put( "map/2", "value2" );
		theProperties.insert( theMap );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			for ( String eKey : theProperties.sortedKeys() ) {
				System.out.println( "!->" + eKey + "=" + theProperties.get( eKey ) );
			}
		}
		final Mapper theType = theProperties.toType( Mapper.class );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			for ( String eKey : theType.getMap().keySet() ) {
				System.out.println( "?->" + eKey + "=" + theType.getMap().get( eKey ) );
			}
		}
		assertEquals( "value0", theType.getMap().get( "0" ) );
		assertEquals( "value1", theType.getMap().get( "1" ) );
		assertEquals( "value2", theType.getMap().get( "2" ) );
	}

	@Test
	public void testPropertyPath() {
		final String thePath = "/hallo/welt/";
		final PropertiesBuilder theProperties = new PropertiesBuilderImpl();
		final String thePropertyPath = theProperties.toPropertyPath( thePath );
		// if (IS_LOG_TESTS_ENABLED) System.out.println( thePropertyPath );
		assertEquals( thePath.substring( 1, thePath.length() - 1 ), thePropertyPath );
	}

	@Test
	public void testToType() {
		PropertiesBuilder theProperties = new PropertiesBuilderImpl();
		theProperties.put( "/database/user", "admin" );
		theProperties.put( "/database/password", "secret" );
		theProperties.put( "/database/url", "my:db/url" );
		theProperties.put( "/console/history", "220" );
		theProperties.put( "/console/height", "25" );
		theProperties.put( "/console/ansi", "true" );
		theProperties.put( "/type", "java.lang.String" );
		Configuration theConfig = theProperties.toType( Configuration.class );
		assertEquals( "admin", theConfig.getDatabase().getUser() );
		assertEquals( "secret", theConfig.getDatabase().getPassword() );
		assertEquals( "my:db/url", theConfig.getDatabase().getUrl() );
		assertEquals( 220, theConfig.getConsole().getHistory() );
		assertEquals( 25, theConfig.getConsole().getHeight() );
		assertEquals( true, theConfig.getConsole().isAnsi() );
		assertEquals( String.class, theConfig.getType() );
		theProperties = new PropertiesBuilderImpl( theConfig );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			final List<String> theKeys = new ArrayList<>( theProperties.keySet() );
			Collections.sort( theKeys );
			for ( String eKey : theKeys ) {
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( eKey + " := " + theProperties.get( eKey ) );
				}
			}
		}
		theConfig = theProperties.toType( Configuration.class );
		assertEquals( "admin", theConfig.getDatabase().getUser() );
		assertEquals( "secret", theConfig.getDatabase().getPassword() );
		assertEquals( "my:db/url", theConfig.getDatabase().getUrl() );
		assertEquals( 220, theConfig.getConsole().getHistory() );
		assertEquals( 25, theConfig.getConsole().getHeight() );
		assertEquals( true, theConfig.getConsole().isAnsi() );
		assertEquals( String.class, theConfig.getType() );
	}

	@Test
	public void testToTypeFragment() {
		PropertiesBuilder theProperties = new PropertiesBuilderImpl();
		theProperties.put( "/database/user", "admin" );
		theProperties.put( "/database/password", "secret" );
		theProperties.put( "/database/url", "my:db/url" );
		Configuration theConfig = theProperties.toType( Configuration.class );
		assertEquals( "admin", theConfig.getDatabase().getUser() );
		assertEquals( "secret", theConfig.getDatabase().getPassword() );
		assertEquals( "my:db/url", theConfig.getDatabase().getUrl() );
		theProperties = new PropertiesBuilderImpl( theConfig );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			final List<String> theKeys = new ArrayList<>( theProperties.keySet() );
			Collections.sort( theKeys );
			for ( String eKey : theKeys ) {
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( eKey + " := " + theProperties.get( eKey ) );
				}
			}
		}
		theConfig = theProperties.toType( Configuration.class );
		assertEquals( "admin", theConfig.getDatabase().getUser() );
		assertEquals( "secret", theConfig.getDatabase().getPassword() );
		assertEquals( "my:db/url", theConfig.getDatabase().getUrl() );
	}

	@Test
	public void testToInstance() {
		PropertiesBuilder theProperties = new PropertiesBuilderImpl();
		theProperties.put( "/database/user", "admin" );
		theProperties.put( "/database/password", "secret" );
		theProperties.put( "/database/url", "my:db/url" );
		theProperties.put( "/console/history", "220" );
		theProperties.put( "/console/height", "25" );
		theProperties.put( "/console/ansi", "true" );
		theProperties.put( "/type", "java.lang.String" );
		final Configuration theConfig = new Configuration();
		theProperties.toInstance( theConfig );
		assertEquals( "admin", theConfig.getDatabase().getUser() );
		assertEquals( "secret", theConfig.getDatabase().getPassword() );
		assertEquals( "my:db/url", theConfig.getDatabase().getUrl() );
		assertEquals( 220, theConfig.getConsole().getHistory() );
		assertEquals( 25, theConfig.getConsole().getHeight() );
		assertEquals( true, theConfig.getConsole().isAnsi() );
		assertEquals( String.class, theConfig.getType() );
		theProperties = new PropertiesBuilderImpl( theConfig );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			final List<String> theKeys = new ArrayList<>( theProperties.keySet() );
			Collections.sort( theKeys );
			for ( String eKey : theKeys ) {
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( eKey + " := " + theProperties.get( eKey ) );
				}
			}
		}
		theProperties.toInstance( theConfig );
		assertEquals( "admin", theConfig.getDatabase().getUser() );
		assertEquals( "secret", theConfig.getDatabase().getPassword() );
		assertEquals( "my:db/url", theConfig.getDatabase().getUrl() );
		assertEquals( 220, theConfig.getConsole().getHistory() );
		assertEquals( 25, theConfig.getConsole().getHeight() );
		assertEquals( true, theConfig.getConsole().isAnsi() );
		assertEquals( String.class, theConfig.getType() );
	}

	@Test
	public void testToArrayType() {
		PropertiesBuilder theProperties = new PropertiesBuilderImpl();
		theProperties.put( "/0", "admin" );
		theProperties.put( "/1", "secret" );
		theProperties.put( "/2", "my:db/url" );
		theProperties.put( "/3", "220" );
		theProperties.put( "/4", "25" );
		theProperties.put( "/5", "true" );
		theProperties.put( "/6", "java.lang.String" );
		String[] theConfig = theProperties.toType( String[].class );
		assertEquals( "admin", theConfig[0] );
		assertEquals( "secret", theConfig[1] );
		assertEquals( "my:db/url", theConfig[2] );
		assertEquals( "220", theConfig[3] );
		assertEquals( "25", theConfig[4] );
		assertEquals( "true", theConfig[5] );
		assertEquals( "java.lang.String", theConfig[6] );
		assertEquals( theProperties.size(), theConfig.length );
		theProperties = new PropertiesBuilderImpl( theConfig );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			final List<String> theKeys = new ArrayList<>( theProperties.keySet() );
			Collections.sort( theKeys );
			for ( String eKey : theKeys ) {
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( eKey + " := " + theProperties.get( eKey ) );
				}
			}
		}
		theConfig = theProperties.toType( String[].class );
		assertEquals( "admin", theConfig[0] );
		assertEquals( "secret", theConfig[1] );
		assertEquals( "my:db/url", theConfig[2] );
		assertEquals( "220", theConfig[3] );
		assertEquals( "25", theConfig[4] );
		assertEquals( "true", theConfig[5] );
		assertEquals( "java.lang.String", theConfig[6] );
		assertEquals( theProperties.size(), theConfig.length );
	}

	@Test
	public void testToArrayInstance() {
		PropertiesBuilder theProperties = new PropertiesBuilderImpl();
		theProperties.put( "/0", "admin" );
		theProperties.put( "/1", "secret" );
		theProperties.put( "/2", "my:db/url" );
		theProperties.put( "/3", "220" );
		theProperties.put( "/4", "25" );
		theProperties.put( "/5", "true" );
		theProperties.put( "/6", "java.lang.String" );
		final String[] theConfig = new String[] { "0", "1", "3", "3", "4", "5", "6" };
		theProperties.toInstance( theConfig );
		assertEquals( "admin", theConfig[0] );
		assertEquals( "secret", theConfig[1] );
		assertEquals( "my:db/url", theConfig[2] );
		assertEquals( "220", theConfig[3] );
		assertEquals( "25", theConfig[4] );
		assertEquals( "true", theConfig[5] );
		assertEquals( "java.lang.String", theConfig[6] );
		theProperties = new PropertiesBuilderImpl( theConfig );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			final List<String> theKeys = new ArrayList<>( theProperties.keySet() );
			Collections.sort( theKeys );
			for ( String eKey : theKeys ) {
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( eKey + " := " + theProperties.get( eKey ) );
				}
			}
		}
		theProperties.toInstance( theConfig );
		assertEquals( "admin", theConfig[0] );
		assertEquals( "secret", theConfig[1] );
		assertEquals( "my:db/url", theConfig[2] );
		assertEquals( "220", theConfig[3] );
		assertEquals( "25", theConfig[4] );
		assertEquals( "true", theConfig[5] );
		assertEquals( "java.lang.String", theConfig[6] );
	}

	@Test
	public void testToSmallerInstance() {
		PropertiesBuilder theProperties = new PropertiesBuilderImpl();
		theProperties.put( "/0", "admin" );
		theProperties.put( "/1", "secret" );
		theProperties.put( "/2", "my:db/url" );
		theProperties.put( "/3", "220" );
		theProperties.put( "/4", "25" );
		theProperties.put( "/5", "true" );
		theProperties.put( "/6", "java.lang.String" );
		final String[] theConfig = new String[] { "0", "1", "3", "3", "4", "5" };
		theProperties.toInstance( theConfig );
		assertEquals( "admin", theConfig[0] );
		assertEquals( "secret", theConfig[1] );
		assertEquals( "my:db/url", theConfig[2] );
		assertEquals( "220", theConfig[3] );
		assertEquals( "25", theConfig[4] );
		assertEquals( "true", theConfig[5] );
		theProperties = new PropertiesBuilderImpl( theConfig );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			final List<String> theKeys = new ArrayList<>( theProperties.keySet() );
			Collections.sort( theKeys );
			for ( String eKey : theKeys ) {
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( eKey + " := " + theProperties.get( eKey ) );
				}
			}
		}
		theProperties.toInstance( theConfig );
		assertEquals( "admin", theConfig[0] );
		assertEquals( "secret", theConfig[1] );
		assertEquals( "my:db/url", theConfig[2] );
		assertEquals( "220", theConfig[3] );
		assertEquals( "25", theConfig[4] );
		assertEquals( "true", theConfig[5] );
	}

	@Test
	public void testToBiggerArrayInstance() {
		PropertiesBuilder theProperties = new PropertiesBuilderImpl();
		theProperties.put( "/0", "admin" );
		theProperties.put( "/1", "secret" );
		theProperties.put( "/2", "my:db/url" );
		theProperties.put( "/3", "220" );
		theProperties.put( "/4", "25" );
		theProperties.put( "/5", "true" );
		theProperties.put( "/6", "java.lang.String" );
		final String[] theConfig = new String[] { "0", "1", "3", "3", "4", "5", "6", "7" };
		theProperties.toInstance( theConfig );
		assertEquals( "admin", theConfig[0] );
		assertEquals( "secret", theConfig[1] );
		assertEquals( "my:db/url", theConfig[2] );
		assertEquals( "220", theConfig[3] );
		assertEquals( "25", theConfig[4] );
		assertEquals( "true", theConfig[5] );
		assertEquals( "java.lang.String", theConfig[6] );
		assertEquals( "7", theConfig[7] );
		theProperties = new PropertiesBuilderImpl( theConfig );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			final List<String> theKeys = new ArrayList<>( theProperties.keySet() );
			Collections.sort( theKeys );
			for ( String eKey : theKeys ) {
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( eKey + " := " + theProperties.get( eKey ) );
				}
			}
		}
		theProperties.toInstance( theConfig );
		assertEquals( "admin", theConfig[0] );
		assertEquals( "secret", theConfig[1] );
		assertEquals( "my:db/url", theConfig[2] );
		assertEquals( "220", theConfig[3] );
		assertEquals( "25", theConfig[4] );
		assertEquals( "true", theConfig[5] );
		assertEquals( "java.lang.String", theConfig[6] );
		assertEquals( "7", theConfig[7] );
	}

	@Test
	public void testEdgeCase() {
		try {
			final PropertiesBuilder theProperties = new PropertiesBuilderImpl( new File( "/" ) );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				final List<String> theKeys = new ArrayList<>( theProperties.keySet() );
				Collections.sort( theKeys );
				for ( String eKey : theKeys ) {
					if ( SystemProperty.LOG_TESTS.isEnabled() ) {
						System.out.println( eKey + " := " + theProperties.get( eKey ) );
					}
				}
			}
		}
		catch ( StackOverflowError e ) {
			fail( "Must not occur!", e );
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	public static class Configuration {
		private Class<?> type;
		private DatabaseConnection _database;
		private Console _console;

		public Class<?> getType() {
			return type;
		}

		public void setType( Class<?> type ) {
			this.type = type;
		}

		public DatabaseConnection getDatabase() {
			return _database;
		}

		public void setDatabase( DatabaseConnection database ) {
			_database = database;
		}

		public Console getConsole() {
			return _console;
		}

		public void setConsole( Console console ) {
			_console = console;
		}

		@Override
		public String toString() {
			return "Configuration [database=" + _database + ", console=" + _console + "]";
		}
	}

	public static class DatabaseConnection {
		private String _user;
		private String _password;
		private String _url;

		public String getUser() {
			return _user;
		}

		public void setUser( String user ) {
			_user = user;
		}

		public String getPassword() {
			return _password;
		}

		public void setPassword( String password ) {
			_password = password;
		}

		public String getUrl() {
			return _url;
		}

		public void setUrl( String url ) {
			_url = url;
		}

		@Override
		public String toString() {
			return "DatabaseConnection [user=" + _user + ", password=" + _password + ", url=" + _url + "]";
		}
	}

	public static class Console {
		private int _history;
		private int _height;
		private boolean _ansi2;

		public int getHistory() {
			return _history;
		}

		public void setHistory( int history ) {
			_history = history;
		}

		public int getHeight() {
			return _height;
		}

		public void setHeight( int height ) {
			_height = height;
		}

		public boolean isAnsi() {
			return _ansi2;
		}

		public void setAnsi( boolean ansi ) {
			_ansi2 = ansi;
		}

		@Override
		public String toString() {
			return "Console [width=" + _history + ", height=" + _height + ", ansi=" + _ansi2 + "]";
		}
	}

	public static class Mapper {
		private Map<String, String> _map = new HashMap<>();

		public Map<String, String> getMap() {
			return _map;
		}

		public void setMap( Map<String, String> aMap ) {
			_map = aMap;
		}
	}
}
