// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.properties;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.refcodes.properties.ResourceProperties.ResourcePropertiesBuilder;
import org.refcodes.runtime.ConfigLocator;
import org.refcodes.runtime.SystemProperty;

public class TomlPropertiesTest extends AbstractResourcePropertiesTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testDelimiter1() throws IOException, ParseException {
		final TomlProperties theProperties = new TomlProperties( "donaBaileyDelimiter1.ini", ConfigLocator.APPLICATION_ALL, new DocumentMetricsImpl( '/' ) );
		final List<String> theKeys = theProperties.sortedKeys();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			for ( String eKey : theKeys ) {
				System.out.println( eKey + " = " + theProperties.get( eKey ) );
			}
		}
		assertEquals( 4, theProperties.size() );
		assertEquals( "Atari", theProperties.get( "/company" ) );
		assertEquals( "Centipede", theProperties.get( "/game" ) );
		assertEquals( "Dona", theProperties.get( "/identity/firstName" ) );
		assertEquals( "Bailey", theProperties.get( "/identity/lastName" ) );
	}

	@Test
	public void testDelimiter1Other() throws IOException, ParseException {
		final TomlProperties theProperties = new TomlProperties( "donaBaileyDelimiter1.ini", ConfigLocator.APPLICATION_ALL, new DocumentMetricsImpl( '|' ) ); // As the "donaBaileyDelimiter1.ini" already uses '/' we are fine here no matter which supported delimiter we provide! 
		final List<String> theKeys = theProperties.sortedKeys();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			for ( String eKey : theKeys ) {
				System.out.println( eKey + " = " + theProperties.get( eKey ) );
			}
		}
		assertEquals( 4, theProperties.size() );
		assertEquals( "Atari", theProperties.get( "/company" ) );
		assertEquals( "Centipede", theProperties.get( "/game" ) );
		assertEquals( "Dona", theProperties.get( "/identity/firstName" ) );
		assertEquals( "Bailey", theProperties.get( "/identity/lastName" ) );
	}

	@Test
	public void testDelimiter2() throws IOException, ParseException {
		final TomlProperties theProperties = new TomlProperties( "donaBaileyDelimiter2.ini", ConfigLocator.APPLICATION_ALL, new DocumentMetricsImpl( '\\' ) );
		final List<String> theKeys = theProperties.sortedKeys();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			for ( String eKey : theKeys ) {
				System.out.println( eKey + " = " + theProperties.get( eKey ) );
			}
		}
		assertEquals( 4, theProperties.size() );
		assertEquals( "Atari", theProperties.get( "/company" ) );
		assertEquals( "Centipede", theProperties.get( "/game" ) );
		assertEquals( "Dona", theProperties.get( "/identity/firstName" ) );
		assertEquals( "Bailey", theProperties.get( "/identity/lastName" ) );
	}

	@Test
	public void testDelimiter2Other() throws IOException, ParseException {
		final TomlProperties theProperties = new TomlProperties( "donaBaileyDelimiter2.ini", ConfigLocator.APPLICATION_ALL, new DocumentMetricsImpl( '/' ) );
		final List<String> theKeys = theProperties.sortedKeys();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			for ( String eKey : theKeys ) {
				System.out.println( eKey + " = " + theProperties.get( eKey ) );
			}
		}
		assertEquals( 4, theProperties.size() );
		assertEquals( "Atari", theProperties.get( "/company" ) );
		assertEquals( "Centipede", theProperties.get( "/game" ) );
		assertEquals( "Dona", theProperties.get( "/identity\\firstName" ) );
		assertEquals( "Bailey", theProperties.get( "/identity\\lastName" ) );
	}

	@Test
	public void testDelimiter3() throws IOException, ParseException {
		final TomlProperties theProperties = new TomlProperties( "donaBaileyDelimiter3.ini", ConfigLocator.APPLICATION_ALL, new DocumentMetricsImpl( '\\', '|' ) );
		final List<String> theKeys = theProperties.sortedKeys();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			for ( String eKey : theKeys ) {
				System.out.println( eKey + " = " + theProperties.get( eKey ) );
			}
		}
		assertEquals( 4, theProperties.size() );
		assertEquals( "Atari", theProperties.get( "/company" ) );
		assertEquals( "Centipede", theProperties.get( "/game" ) );
		assertEquals( "Dona", theProperties.get( "/identity/firstName" ) );
		assertEquals( "Bailey", theProperties.get( "/identity/lastName" ) );
	}

	@Test
	public void testDelimiter3Other() throws IOException, ParseException {
		final TomlProperties theProperties = new TomlProperties( "donaBaileyDelimiter3.ini", ConfigLocator.APPLICATION_ALL, new DocumentMetricsImpl( '/' ) );
		final List<String> theKeys = theProperties.sortedKeys();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			for ( String eKey : theKeys ) {
				System.out.println( eKey + " = " + theProperties.get( eKey ) );
			}
		}
		assertEquals( 4, theProperties.size() );
		assertEquals( "Atari", theProperties.get( "/company" ) );
		assertEquals( "Centipede", theProperties.get( "/\\game" ) );
		assertEquals( "Dona", theProperties.get( "/|identity\\firstName" ) );
		assertEquals( "Bailey", theProperties.get( "/\\identity|lastName" ) );
	}

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected ResourcePropertiesBuilder toResourcePropertiesBuilder() {
		return new TomlPropertiesBuilder();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected ResourceProperties toResourceProperties( InputStream aInputStream ) throws IOException, ParseException {
		return new TomlProperties( aInputStream );
	}
}
