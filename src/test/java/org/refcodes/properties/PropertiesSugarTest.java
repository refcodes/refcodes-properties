// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.properties;

import static org.junit.jupiter.api.Assertions.*;
import static org.refcodes.properties.PropertiesSugar.*;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.refcodes.properties.Properties.PropertiesBuilder;
import org.refcodes.properties.ResourceProperties.ResourcePropertiesBuilder;
import org.refcodes.runtime.SystemProperty;

public class PropertiesSugarTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testPropertiesPrecedence() {
		// @formatter:off
		final Properties theProperties = toPrecedence( 
				fromProperties( toProperty( "/user/firstName", "Nolan" ), toProperty( "/user/lastName", "Bushnell" ) ) ,
				fromProperties( toProperty( "/user/firstName", "Jack" ), toProperty( "/user/lastName", "Tramiel" ) ) 
		);
		// @formatter:on
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			for ( String eKey : theProperties.keySet() ) {
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( eKey + " := " + theProperties.get( eKey ) );
				}
			}
		}
		assertEquals( "Nolan", theProperties.get( "/user/firstName" ) );
		assertEquals( "Bushnell", theProperties.get( "/user/lastName" ) );
		assertEquals( "Nolan", theProperties.get( "user/firstName" ) );
		assertEquals( "Bushnell", theProperties.get( "user/lastName" ) );
	}

	@Test
	public void testJavaProperties() throws IOException, ParseException {
		final Properties theProperties = toNormalized( seekFromJavaProperties( "application.properties" ) );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			for ( String eKey : theProperties.keySet() ) {
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( eKey + " := " + theProperties.get( eKey ) );
				}
			}
		}
		assertEquals( "Jack", theProperties.get( "/user/firstName" ) );
		assertEquals( "Tramiel", theProperties.get( "/user/lastName" ) );
		assertEquals( "Jack", theProperties.get( "user/firstName" ) );
		assertEquals( "Tramiel", theProperties.get( "user/lastName" ) );
	}

	@Test
	public void testPropertiesDemo() throws IOException, ParseException {
		final Properties theProperties = toPrecedence( fromSystemProperties(), fromEnvironmentVariables(), seekFromJavaProperties( "application.config" ) );
		Configuration theConfiguration = theProperties.toType( Configuration.class );
		assertEquals( "Nolan", theConfiguration.getUser().getFirstName() );
		assertEquals( "Bushnell", theConfiguration.getUser().getLastName() );
		assertEquals( "admin", theConfiguration.getDatabase().getUser() );
		assertEquals( "secret", theConfiguration.getDatabase().getPassword() );
		assertEquals( "jdbc://my/database/url:5161", theConfiguration.getDatabase().getUrl() );
		assertEquals( 240, theConfiguration.getConsole().getHistory() );
		assertEquals( 25, theConfiguration.getConsole().getHeight() );
		assertEquals( true, theConfiguration.getConsole().isAnsi() );
		final Properties theProfile = fromProfile( theProperties );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			final List<String> theKeys = new ArrayList<>( theProfile.keySet() );
			Collections.sort( theKeys );
			for ( String eKey : theKeys ) {
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( eKey + " := " + theProfile.get( eKey ) );
				}
			}
		}
		theConfiguration = theProfile.toType( Configuration.class );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "--------------------------------------------------------------------------------" );
			}
			final List<String> theKeys = new ArrayList<>( theProfile.keySet() );
			Collections.sort( theKeys );
			for ( String eKey : theKeys ) {
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( eKey + " := " + theProfile.get( eKey ) );
				}
			}
		}
		assertEquals( "Jack", theConfiguration.getUser().getFirstName() );
		assertEquals( "Tramiel", theConfiguration.getUser().getLastName() );
		assertEquals( "admin", theConfiguration.getDatabase().getUser() );
		assertEquals( "secret", theConfiguration.getDatabase().getPassword() );
		assertEquals( "jdbc://my/database/url:5161", theConfiguration.getDatabase().getUrl() );
		assertEquals( 240, theConfiguration.getConsole().getHistory() );
		assertEquals( 25, theConfiguration.getConsole().getHeight() );
		assertEquals( true, theConfiguration.getConsole().isAnsi() );
	}

	@Disabled("Just for educational purposes")
	@Test
	public void blogExanmple() throws IOException, ParseException {
		final PropertiesBuilder theProperties = fromProperties( toProperty( "/user/firstName", "Nolan" ), toProperty( "/user/lastName", "Bsuhnell" ), toProperty( "/commodore/user/firstName", "Jack" ), toProperty( "/commodore/user/lastName", "Tramiel" ) );
		for ( String ePath : theProperties.keySet() ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( ePath + "=" + theProperties.get( ePath ) );
			}
		}
		final Properties theCommodreProperties = theProperties.retrieveFrom( "/commodore" );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println();
		}
		for ( String ePath : theCommodreProperties.keySet() ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( ePath + "=" + theCommodreProperties.get( ePath ) );
			}
		}
		final PropertiesBuilder theBuilder = toPropertiesBuilder( theCommodreProperties );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println();
		}
		for ( String ePath : theBuilder.keySet() ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( ePath + "=" + theBuilder.get( ePath ) );
			}
		}
		// ResourcePropertiesBuilder theResourceProperties = new TomlPropertiesBuilder();
		ResourcePropertiesBuilder theResourceProperties = saveToTomlProperties( theProperties, "/some/path/to/my/properties.toml" );
		theResourceProperties = loadFromTomlProperties( "/some/path/to/my/properties.toml" );
		theResourceProperties = fileToTomlProperties( theProperties, "properties.toml" );
		theResourceProperties = seekFromTomlProperties( "properties.toml" );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println();
		}
		for ( String ePath : theResourceProperties.keySet() ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( ePath + "=" + theResourceProperties.get( ePath ) );
			}
		}
		final Properties theProfile = fromProfile( theProperties, "commodore" );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println();
		}
		for ( String ePath : theProfile.keySet() ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( ePath + "=" + theProfile.get( ePath ) );
			}
		}
		final ScheduledResourceProperties theScheduled = schedule( theResourceProperties, 5000, ReloadMode.ORPHAN_REMOVAL );
		theScheduled.stopUnchecked();
		// ...
		final PropertiesBuilder theProps = fromProperties( toProperty( "message", "Hello world!" ), toProperty( "foo", "bar" ) );
		fileToTomlProperties( theProps, "application.toml" );
		// ...
		final Properties thePrecedence = toPrecedence( fromSystemProperties(), fromEnvironmentVariables(), seekFromTomlProperties( "application.toml" ) );
		final Configuration theConfig = thePrecedence.toType( Configuration.class );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theConfig );
			// ...
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	public static class Configuration {
		private User _user;
		private DatabaseConnection _database;
		private Console _console;

		public User getUser() {
			return _user;
		}

		public void setUser( User user ) {
			_user = user;
		}

		public DatabaseConnection getDatabase() {
			return _database;
		}

		public void setDatabase( DatabaseConnection database ) {
			_database = database;
		}

		public Console getConsole() {
			return _console;
		}

		public void setConsole( Console console ) {
			_console = console;
		}

		@Override
		public String toString() {
			return "Configuration [user=" + _user + ", database=" + _database + ", console=" + _console + "]";
		}
	}

	public static class User {
		private String _firstName;
		private String _lastName;

		public String getFirstName() {
			return _firstName;
		}

		public void setFirstName( String firstName ) {
			_firstName = firstName;
		}

		public String getLastName() {
			return _lastName;
		}

		public void setLastName( String lastName ) {
			_lastName = lastName;
		}

		@Override
		public String toString() {
			return "User [firstName=" + _firstName + ", lastName=" + _lastName + "]";
		}
	}

	public static class DatabaseConnection {
		private String _user;
		private String _password;
		private String _url;

		public String getUser() {
			return _user;
		}

		public void setUser( String user ) {
			_user = user;
		}

		public String getPassword() {
			return _password;
		}

		public void setPassword( String password ) {
			_password = password;
		}

		public String getUrl() {
			return _url;
		}

		public void setUrl( String url ) {
			_url = url;
		}

		@Override
		public String toString() {
			return "DatabaseConnection [user=" + _user + ", password=" + _password + ", url=" + _url + "]";
		}
	}

	public static class Console {
		private int _history;
		private int _height;
		private boolean _ansi;

		public int getHistory() {
			return _history;
		}

		public void setHistory( int history ) {
			_history = history;
		}

		public int getHeight() {
			return _height;
		}

		public void setHeight( int height ) {
			_height = height;
		}

		public boolean isAnsi() {
			return _ansi;
		}

		public void setAnsi( boolean ansi ) {
			_ansi = ansi;
		}

		@Override
		public String toString() {
			return "Console [history=" + _history + ", height=" + _height + ", ansi=" + _ansi + "]";
		}
	}
}
