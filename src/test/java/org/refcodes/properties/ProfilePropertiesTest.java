// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.properties;

import static org.junit.jupiter.api.Assertions.*;
import java.io.IOException;
import java.net.URL;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.refcodes.properties.Properties.PropertiesBuilder;
import org.refcodes.runtime.SystemProperty;

public class ProfilePropertiesTest {

	private static final String PROFILE_PROPERTIES_CONFIG = "/profile-properties.config";

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testProfileProperties() throws IOException {
		final PropertiesBuilder theBuilder = new PropertiesBuilderImpl();
		theBuilder.withPut( "name/first", "Toshihiro" ).withPut( "name/last", "Nishikado" );
		theBuilder.withPut( "atari/name/first", "Nolan" ).withPut( "atari/name/last", "Bushnell" );
		theBuilder.withPut( "commodore/name/first", "Jack" ).withPut( "commodore/name/last", "Tramiel" );
		final ProfilePropertiesDecorator theDecorator = new ProfilePropertiesDecorator( theBuilder );
		final Properties theProfile = theDecorator.toRuntimeProfile( "atari" );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			final List<String> theKeys = new ArrayList<>( theProfile.keySet() );
			Collections.sort( theKeys );
			for ( String eKey : theKeys ) {
				System.out.println( eKey + " := " + theProfile.get( eKey ) );
			}
		}
		assertEquals( "Nolan", theProfile.get( "name/first" ) );
		assertEquals( "Bushnell", theProfile.get( "name/last" ) );
	}

	@Test
	public void testProfileProjection() throws IOException {
		final PropertiesBuilder theBuilder = new PropertiesBuilderImpl();
		theBuilder.withPut( "name/first", "Toshihiro" ).withPut( "name/last", "Nishikado" );
		theBuilder.withPut( "atari/name/first", "Nolan" ).withPut( "atari/name/last", "Bushnell" );
		theBuilder.withPut( "commodore/name/first", "Jack" ).withPut( "commodore/name/last", "Tramiel" );
		final ProfilePropertiesProjection theProjection = new ProfilePropertiesProjection( theBuilder, "atari" );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			final List<String> theKeys = new ArrayList<>( theProjection.keySet() );
			Collections.sort( theKeys );
			for ( String eKey : theKeys ) {
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( eKey + " := " + theProjection.get( eKey ) );
				}
			}
		}
		assertEquals( "Nolan", theProjection.get( "name/first" ) );
		assertEquals( "Bushnell", theProjection.get( "name/last" ) );
	}

	@Test
	public void testProfilePropertiesFromFile() throws IOException, ParseException {
		final ResourceProperties theResourceProperties = new JavaPropertiesBuilder().withSeekFrom( getClass().getResource( PROFILE_PROPERTIES_CONFIG ).getFile() );
		/*
		 * List<String> theKeys = new ArrayList<String>(
		 * theResourceProperties.records() ); Collections.sort( theKeys ); for (
		 * String eKey : theKeys ) { if (IS_LOG_TESTS_ENABLED)
		 * System.out.println( eKey + " := " + theResourceProperties.get( eKey )
		 * ); }
		 */
		final ProfileProperties theProfileProperties = new ProfilePropertiesDecorator( theResourceProperties );
		final Properties theProfile = theProfileProperties.toRuntimeProfile();
		/*
		 * List<String> theKeys = new ArrayList<String>( theProfile.records() );
		 * Collections.sort( theKeys ); for ( String eKey : theKeys ) { if
		 * (IS_LOG_TESTS_ENABLED) System.out.println( eKey + " := " +
		 * theProfile.get( eKey ) ); }
		 */
		assertEquals( "someLocalDbUrl", theProfile.get( "db_url" ) );
		assertEquals( "devops", theProfile.get( "db_user" ) );
		assertEquals( true, theProfile.getBoolean( "security" ) );
	}

	@Disabled("Depends on internet connectivity :-(")
	@Test
	public void testProfilePropertiesFromUrl() throws IOException, ParseException {
		// Proxy.initializeAll();
		final URL theURL = new URL( "https://bitbucket.org/refcodes/refcodes-properties/raw/master/src/test/resources/profile-properties.config" );
		final ResourceProperties theResourceProperties = new JavaProperties( theURL );
		final ProfileProperties theProfileProperties = new ProfilePropertiesDecorator( theResourceProperties );
		final Properties theProfile = theProfileProperties.toRuntimeProfile();
		/*
		 * List<String> theKeys = new ArrayList<String>( theProfile.records() );
		 * Collections.sort( theKeys ); for ( String eKey : theKeys ) { if
		 * (IS_LOG_TESTS_ENABLED) System.out.println( eKey + " := " +
		 * theProfile.get( eKey ) ); }
		 */
		assertEquals( "someLocalDbUrl", theProfile.get( "db_url" ) );
		assertEquals( "devops", theProfile.get( "db_user" ) );
		assertEquals( true, theProfile.getBoolean( "security" ) );
	}

	@Test
	public void testProfilePropertiesByProfileFromFile() throws IOException, ParseException {
		final ResourceProperties theResourceProperties = new JavaPropertiesBuilder().withSeekFrom( getClass().getResource( PROFILE_PROPERTIES_CONFIG ).getFile() );
		final ProfileProperties theProfileProperties = new ProfilePropertiesDecorator( theResourceProperties );
		final Properties theProfile = theProfileProperties.toRuntimeProfile( "test" );
		/*
		 * List<String> theKeys = new ArrayList<String>( theProfile.records() );
		 * Collections.sort( theKeys ); for ( String eKey : theKeys ) { if
		 * (IS_LOG_TESTS_ENABLED) System.out.println( eKey + " := " +
		 * theProfile.get( eKey ) ); }
		 */
		assertEquals( "someTestDbUrl", theProfile.get( "db_url" ) );
		assertEquals( "test", theProfile.get( "db_user" ) );
		assertEquals( false, theProfile.getBoolean( "security" ) );
	}
}
