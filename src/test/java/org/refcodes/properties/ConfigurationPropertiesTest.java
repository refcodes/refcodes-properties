// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.properties;

import static org.junit.jupiter.api.Assertions.*;
import java.io.IOException;
import java.text.ParseException;
import org.junit.jupiter.api.Test;
import org.refcodes.runtime.ConfigLocator;
import org.refcodes.runtime.SystemProperty;

public class ConfigurationPropertiesTest {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final String[][] EXPECTED = new String[][] { new String[] { "/com/acme/runtimelogger/logger", "org.refcodes.logger.alt.cli.ConsoleLoggerSingleton" }, new String[] { "/com/acme/runtimelogger/logPriority", "INFO" }, new String[] { "/com/acme/runtimelogger", "org.refcodes.logger.RuntimeLoggerImpl" }, new String[] { "/root/runtimelogger/logger", "org.refcodes.logger.alt.cli.ConsoleLoggerSingleton" }, new String[] { "/root/runtimelogger/logPriority", "INFO" }, new String[] { "/root/runtimelogger", "org.refcodes.logger.RuntimeLoggerImpl" } };

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testRuntimeLoggerXml() throws IOException, ParseException {
		final ResourceProperties theProperties = new XmlProperties( "configuration.xml", ConfigLocator.APPLICATION_ALL );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			for ( String eKey : theProperties.keySet() ) {
				System.out.println( eKey + " = " + theProperties.get( eKey ) );
			}
		}
		verifyResult( theProperties );
	}

	@Test
	public void testRuntimeLoggerYaml() throws IOException, ParseException {
		final ResourceProperties theProperties = new YamlProperties( "configuration.yaml", ConfigLocator.APPLICATION_ALL );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			for ( String eKey : theProperties.keySet() ) {
				System.out.println( eKey + " = " + theProperties.get( eKey ) );
			}
		}
		verifyResult( theProperties );
	}

	@Test
	public void testRuntimeLoggerIni() throws IOException, ParseException {
		final ResourceProperties theProperties = new TomlProperties( "configuration.ini", ConfigLocator.APPLICATION_ALL );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			for ( String eKey : theProperties.keySet() ) {
				System.out.println( eKey + " = " + theProperties.get( eKey ) );
			}
		}
		verifyResult( theProperties );
	}

	@Test
	public void testRuntimeLoggerJson() throws IOException, ParseException {
		final ResourceProperties theProperties = new JsonProperties( "configuration.json", ConfigLocator.APPLICATION_ALL );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			for ( String eKey : theProperties.keySet() ) {
				System.out.println( eKey + " = " + theProperties.get( eKey ) );
			}
		}
		verifyResult( theProperties );
	}

	@Test
	public void testRuntimeLoggerProperties() throws IOException, ParseException {
		final ResourceProperties theProperties = new JavaProperties( "configuration.properties", ConfigLocator.APPLICATION_ALL );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			for ( String eKey : theProperties.keySet() ) {
				System.out.println( eKey + " = " + theProperties.get( eKey ) );
			}
		}
		verifyResult( theProperties );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private void verifyResult( ResourceProperties aProperties ) {
		for ( String[] aEXPECTED : EXPECTED ) {
			assertEquals( aEXPECTED[1], aProperties.get( aEXPECTED[0] ) );
		}
		assertEquals( EXPECTED.length, aProperties.size() );
	}
}
