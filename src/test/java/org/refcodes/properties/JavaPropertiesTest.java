// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.properties;

import static org.junit.jupiter.api.Assertions.*;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.refcodes.exception.MarshalException;
import org.refcodes.exception.UnmarshalException;
import org.refcodes.properties.ResourceProperties.ResourcePropertiesBuilder;
import org.refcodes.runtime.ConfigLocator;
import org.refcodes.runtime.SystemProperty;

public class JavaPropertiesTest extends AbstractResourcePropertiesTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testVerifyEdgeCase() throws MarshalException, UnmarshalException, IOException, ParseException {
		final JavaPropertiesBuilder theDataStructure = new JavaPropertiesBuilder();
		theDataStructure.put( "aaa/aaa_111", "aaa_111" );
		theDataStructure.put( "aaa/aaa_222", "aaa_222" );
		theDataStructure.put( "aaa/aaa_333", "aaa_333" );
		theDataStructure.put( "bbb/bbb_111", "bbb_111" );
		theDataStructure.put( "bbb/bbb_222", "bbb_222" );
		theDataStructure.put( "bbb/bbb_333", "bbb_333" );
		theDataStructure.put( "ccc/ccc_111", "ccc_111" );
		theDataStructure.put( "ccc/ccc_222", "ccc_222" );
		theDataStructure.put( "ccc/ccc_333", "ccc_333" );
		final String theMarshaled = theDataStructure.toSerialized();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theMarshaled );
		}
		final JavaPropertiesBuilder theUnmarshaled = new JavaPropertiesBuilder();
		final InputStream theUnmarshaledStream = new ByteArrayInputStream( theMarshaled.getBytes( StandardCharsets.UTF_8 ) );
		theUnmarshaled.loadFrom( theUnmarshaledStream );
		assertEquals( theDataStructure.size(), theUnmarshaled.size() );
		for ( String eKey : theDataStructure.keySet() ) {
			assertEquals( theDataStructure.get( eKey ), theUnmarshaled.get( eKey ) );
		}
	}

	@Test
	public void testDelimiter1() throws IOException, ParseException {
		final JavaProperties theProperties = new JavaProperties( "donaBaileyDelimiter1.properties", ConfigLocator.APPLICATION_ALL, new DocumentMetricsImpl( '/' ) );
		final List<String> theKeys = theProperties.sortedKeys();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			for ( String eKey : theKeys ) {
				System.out.println( eKey + " = " + theProperties.get( eKey ) );
			}
		}
		assertEquals( 4, theProperties.size() );
		assertEquals( "Atari", theProperties.get( "/company" ) );
		assertEquals( "Centipede", theProperties.get( "/game" ) );
		assertEquals( "Dona", theProperties.get( "/identity/firstName" ) );
		assertEquals( "Bailey", theProperties.get( "/identity/lastName" ) );
	}

	@Test
	public void testDelimiter1Other() throws IOException, ParseException {
		final JavaProperties theProperties = new JavaProperties( "donaBaileyDelimiter1.properties", ConfigLocator.APPLICATION_ALL, new DocumentMetricsImpl( '|' ) ); // As the "donaBaileyDelimiter1.properties" already uses '/' we are fine here no matter which supported delimiter we provide! 
		final List<String> theKeys = theProperties.sortedKeys();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			for ( String eKey : theKeys ) {
				System.out.println( eKey + " = " + theProperties.get( eKey ) );
			}
		}
		assertEquals( 4, theProperties.size() );
		assertEquals( "Atari", theProperties.get( "/company" ) );
		assertEquals( "Centipede", theProperties.get( "/game" ) );
		assertEquals( "Dona", theProperties.get( "/identity/firstName" ) );
		assertEquals( "Bailey", theProperties.get( "/identity/lastName" ) );
	}

	@Test
	public void testDelimiter2() throws IOException, ParseException {
		final JavaProperties theProperties = new JavaProperties( "donaBaileyDelimiter2.properties", ConfigLocator.APPLICATION_ALL, new DocumentMetricsImpl( '\\' ) );
		final List<String> theKeys = theProperties.sortedKeys();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			for ( String eKey : theKeys ) {
				System.out.println( eKey + " = " + theProperties.get( eKey ) );
			}
		}
		assertEquals( 4, theProperties.size() );
		assertEquals( "Atari", theProperties.get( "/company" ) );
		assertEquals( "Centipede", theProperties.get( "/game" ) );
		assertEquals( "Dona", theProperties.get( "/identity/firstName" ) );
		assertEquals( "Bailey", theProperties.get( "/identity/lastName" ) );
	}

	@Test
	public void testDelimiter2Other() throws IOException, ParseException {
		final JavaProperties theProperties = new JavaProperties( "donaBaileyDelimiter2.properties", ConfigLocator.APPLICATION_ALL, new DocumentMetricsImpl( '/' ) );
		final List<String> theKeys = theProperties.sortedKeys();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			for ( String eKey : theKeys ) {
				System.out.println( eKey + " = " + theProperties.get( eKey ) );
			}
		}
		assertEquals( 4, theProperties.size() );
		assertEquals( "Atari", theProperties.get( "/company" ) );
		assertEquals( "Centipede", theProperties.get( "/game" ) );
		assertEquals( "Dona", theProperties.get( "/identity\\firstName" ) );
		assertEquals( "Bailey", theProperties.get( "/identity\\lastName" ) );
	}

	@Test
	public void testDelimiter3() throws IOException, ParseException {
		final JavaProperties theProperties = new JavaProperties( "donaBaileyDelimiter3.properties", ConfigLocator.APPLICATION_ALL, new DocumentMetricsImpl( '\\', '|' ) );
		final List<String> theKeys = theProperties.sortedKeys();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			for ( String eKey : theKeys ) {
				System.out.println( eKey + " = " + theProperties.get( eKey ) );
			}
		}
		assertEquals( 4, theProperties.size() );
		assertEquals( "Atari", theProperties.get( "/company" ) );
		assertEquals( "Centipede", theProperties.get( "/game" ) );
		assertEquals( "Dona", theProperties.get( "/identity/firstName" ) );
		assertEquals( "Bailey", theProperties.get( "/identity/lastName" ) );
	}

	@Test
	public void testDelimiter3Other() throws IOException, ParseException {
		final JavaProperties theProperties = new JavaProperties( "donaBaileyDelimiter3.properties", ConfigLocator.APPLICATION_ALL, new DocumentMetricsImpl( '/' ) );
		final List<String> theKeys = theProperties.sortedKeys();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			for ( String eKey : theKeys ) {
				System.out.println( eKey + " = " + theProperties.get( eKey ) );
			}
		}
		assertEquals( 4, theProperties.size() );
		assertEquals( "Atari", theProperties.get( "/company" ) );
		assertEquals( "Centipede", theProperties.get( "/\\game" ) );
		assertEquals( "Dona", theProperties.get( "/|identity\\firstName" ) );
		assertEquals( "Bailey", theProperties.get( "/\\identity|lastName" ) );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected ResourcePropertiesBuilder toResourcePropertiesBuilder() {
		return new JavaPropertiesBuilder();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected ResourceProperties toResourceProperties( InputStream aInputStream ) throws IOException, ParseException {
		return new JavaProperties( aInputStream );
	}
}
