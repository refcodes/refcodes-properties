// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.properties;

import static org.junit.jupiter.api.Assertions.*;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.refcodes.runtime.SystemProperty;
import org.refcodes.struct.PathComparator;

/**
 * Tests the {@link PathComparator}.
 */
public class PathComparatorTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testPathComparator() {
		final List<String> theList = new ArrayList<>();
		theList.add( "/aaa/bbb/10" );
		theList.add( "/aaa/bbb/14" );
		theList.add( "/aaa/bbb/3" );
		theList.add( "/aaa/bbb/9" );
		theList.add( "/aaa/bbb/13" );
		theList.add( "/aaa/bbb/20" );
		theList.add( "/aaa/bbb/7" );
		theList.add( "/aaa/bbb/0" );
		theList.add( "/aaa/bbb/16" );
		theList.add( "/aaa/bbb/19" );
		theList.add( "/aaa/bbb/5" );
		theList.add( "/aaa/bbb/2" );
		theList.add( "/aaa/bbb/1" );
		theList.add( "/aaa/bbb/4" );
		theList.add( "/aaa/bbb/6" );
		theList.add( "/aaa/bbb/11" );
		theList.add( "/aaa/bbb/8" );
		theList.add( "/aaa/bbb/21" );
		theList.add( "/aaa/bbb/12" );
		theList.add( "/aaa/bbb/18" );
		theList.add( "/aaa/bbb/17" );
		theList.add( "/aaa/bbb/15" );
		theList.sort( new PathComparator( '/' ) );
		for ( int i = 0; i < theList.size(); i++ ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( theList.get( i ) );
			}
			assertTrue( theList.get( i ).endsWith( "/" + i ) );
		}
	}

	@Test
	public void testPathComparatorEdgeCase() {
		final List<String> theList = new ArrayList<>();
		theList.add( "/aaa/bbb/10" );
		theList.add( "/aaa/bbb/14" );
		theList.add( "/aaa/bbb/3" );
		theList.add( "/aaa/bbb/9" );
		theList.add( "/aaa/bbb/13" );
		theList.add( "/aaa/bbb/20" );
		theList.add( "/aaa/bbb/7" );
		theList.add( "/aaa/bbb/0" );
		theList.add( "/aaa/bbb/16" );
		theList.add( "/aaa/bbb/19" );
		theList.add( "/aaa/bbb/5" );
		theList.add( "/aaa/bbb/2" );
		theList.add( "/aaa/bbb/1" );
		theList.add( "/aaa/bbb/4" );
		theList.add( "/aaa/bbb/6" );
		theList.add( "/aaa/bbb/11" );
		theList.add( "/aaa/bbb/8" );
		theList.add( "/aaa/bbb/21" );
		theList.add( "/aaa/bbb/12" );
		theList.add( "/aaa/bbb/18" );
		theList.add( "/aaa/bbb/17" );
		theList.add( "/aaa/bbb/15" );
		theList.add( "/yyy/zzz/10" );
		theList.add( "/yyy/zzz/14" );
		theList.add( "/yyy/zzz/3" );
		theList.add( "/yyy/zzz/9" );
		theList.add( "/yyy/zzz/13" );
		theList.add( "/yyy/zzz/20" );
		theList.add( "/yyy/zzz/7" );
		theList.add( "/yyy/zzz/0" );
		theList.add( "/yyy/zzz/16" );
		theList.add( "/yyy/zzz/19" );
		theList.add( "/yyy/zzz/5" );
		theList.add( "/yyy/zzz/2" );
		theList.add( "/yyy/zzz/1" );
		theList.add( "/yyy/zzz/4" );
		theList.add( "/yyy/zzz/6" );
		theList.add( "/yyy/zzz/11" );
		theList.add( "/yyy/zzz/8" );
		theList.add( "/yyy/zzz/21" );
		theList.add( "/yyy/zzz/12" );
		theList.add( "/yyy/zzz/18" );
		theList.add( "/yyy/zzz/17" );
		theList.add( "/yyy/zzz/15" );
		theList.add( "/yyy/zzz/a" );
		theList.add( "/yyy/zzz/b" );
		theList.add( "/yyy/zzz/c" );
		theList.add( "/yyy/zzz/d" );
		theList.add( "/yyy/zzz/e" );
		theList.add( "/mmm/nmnn/" );
		theList.sort( new PathComparator( '/' ) );
		for ( String aTheList : theList ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( aTheList );
			}
		}
	}
}
