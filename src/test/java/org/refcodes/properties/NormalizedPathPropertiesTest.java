// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.properties;

import static org.junit.jupiter.api.Assertions.*;
import java.io.IOException;
import org.junit.jupiter.api.Test;
import org.refcodes.properties.Properties.PropertiesBuilder;
import org.refcodes.runtime.SystemProperty;

public class NormalizedPathPropertiesTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testNormalizedProperties() throws IOException {
		final PropertiesBuilder theBuilder = new PropertiesBuilderImpl();
		theBuilder.withPut( ".name.first", "Toshihiro" ).withPut( "name.last", "Nishikado" );
		theBuilder.withPut( ".atari.name.first", "Nolan" ).withPut( "atari.name.last", "Bushnell" );
		theBuilder.withPut( ".commodore.name.first", "Jack" ).withPut( "commodore.name.last", "Tramiel" );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			for ( String eKey : theBuilder.keySet() ) {
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( eKey + " := " + theBuilder.get( eKey ) );
				}
			}
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "--------------------------------------------------------------------------------" );
			}
		}
		final Properties theProperties = new NormalizedPropertiesDecorator( theBuilder );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			for ( String eKey : theProperties.keySet() ) {
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( eKey + " := " + theProperties.get( eKey ) );
				}
			}
		}
		assertEquals( "Toshihiro", theProperties.get( "/name/first" ) );
		assertEquals( "Nishikado", theProperties.get( "/name/last" ) );
		assertEquals( "Toshihiro", theProperties.get( "name/first" ) );
		assertEquals( "Nishikado", theProperties.get( "name/last" ) );
	}

	@Test
	public void testRetreiveFrom() {
		final PropertiesBuilder theBuilder = new PropertiesBuilderImpl();
		theBuilder.withPut( "name.first", "Toshihiro" ).withPut( "name.last", "Nishikado" );
		theBuilder.withPut( ".atari.name.first", "Nolan" ).withPut( "atari.name.last", "Bushnell" );
		theBuilder.withPut( "commodore.name.first", "Jack" ).withPut( "commodore.name.last", "Tramiel" );
		final Properties theProperties = new NormalizedPropertiesDecorator( theBuilder );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			for ( String eKey : theProperties.keySet() ) {
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( eKey + " := " + theProperties.get( eKey ) );
				}
			}
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "--------------------------------------------------------------------------------" );
			}
		}
		final Properties theRetrieved = theProperties.retrieveFrom( "/atari" );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			for ( String eKey : theRetrieved.keySet() ) {
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( eKey + " := " + theRetrieved.get( eKey ) );
				}
			}
		}
		assertEquals( "Nolan", theRetrieved.get( "/name/first" ) );
		assertEquals( "Bushnell", theRetrieved.get( "/name/last" ) );
		assertEquals( "Nolan", theRetrieved.get( "name/first" ) );
		assertEquals( "Bushnell", theRetrieved.get( "name/last" ) );
	}
}
