// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.properties;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.refcodes.runtime.SystemProperty;
import org.refcodes.struct.Property;
import org.refcodes.struct.PropertyImpl;

public class ArgsPropertiesTest {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final String[][] ARGS = new String[][] { { "--name", "FILE", "--boolean", "--active" }, { "--name", "FILE", "--boolean", "--alias", "ALIAS" }, { "--name", "FILE", "--boolean", "--alias", "ALIAS", "--active" }, { "--name", "FILE", "--boolean", "--alias", "ALIAS", "NULL" }, { "NULL", "--name", "FILE", "--boolean", "--alias", "ALIAS" }, { "NULL", "--name", "FILE", "--boolean", "--alias", "ALIAS", "NIL" }, { "NULL", "--name", "NAME0", "--name", "NAME1", "--boolean", "--alias", "ALIAS", "NIL" }, { "NULL", "/name", "NAME0", "/name", "NAME1", "/boolean", "/alias", "ALIAS", "NIL" }, { "NULL", "-name", "NAME0", "-name", "NAME1", "-boolean", "-alias", "ALIAS", "NIL" }, { "NULL", "/name", "NAME0", "-name", "NAME1", "--boolean", "/alias", "ALIAS", "NIL" } };
	private static final String[][] PROPERTIES = new String[][] { { "name=FILE", "boolean=true", "active=true" }, { "name=FILE", "boolean=true", "alias=ALIAS" }, { "name=FILE", "boolean=true", "alias=ALIAS", "active=true" }, { "name=FILE", "boolean=true", "alias=ALIAS", "null=NULL" }, { "null=NULL", "name=FILE", "boolean=true", "alias=ALIAS" }, { "name=FILE", "boolean=true", "alias=ALIAS", "null/0=NULL", "null/1=NIL" }, { "name/0=NAME0", "name/1=NAME1", "boolean=true", "alias=ALIAS", "null/0=NULL", "null/1=NIL" }, { "name/0=NAME0", "name/1=NAME1", "boolean=true", "alias=ALIAS", "null/0=NULL", "null/1=NIL" }, { "name/0=NAME0", "name/1=NAME1", "boolean=true", "alias=ALIAS", "null/0=NULL", "null/1=NIL" }, { "name/0=NAME0", "name/1=NAME1", "boolean=true", "alias=ALIAS", "null/0=NULL", "null/1=NIL" } };

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testArgsProperties() {
		for ( int i = 0; i < ARGS.length; i++ ) {
			testArgs( i );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println();
			}
		}
	}

	@Disabled
	@Test
	public void testEdgeCase() {
		testArgs( 6 );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println();
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	protected void testArgs( int i ) {
		final ArgsProperties theProperties = new ArgsProperties( ARGS[i] );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Line <" + i + ">:" );
		}
		for ( String eKey : theProperties.keySet() ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( eKey + " := " + theProperties.get( eKey ) );
			}
		}
		final String[] theExcepcted = PROPERTIES[i];
		for ( String aTheExcepcted : theExcepcted ) {
			final Property eProperty = new PropertyImpl( aTheExcepcted );
			assertEquals( eProperty.getValue(), theProperties.get( eProperty.getKey() ), "Line <" + i + "> (" + eProperty.toString() + ")" );
		}
	}
}
