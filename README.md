# README #

> The [`REFCODES.ORG`](http://www.refcodes.org/refcodes) codes represent a group of artifacts consolidating parts of my work in the past years. Several topics are covered which I consider useful for you, programmers, developers and software engineers.

## What is this repository for? ##

***This artifact provides a canonical model for processing properties from various different formats (`*.yaml`, `*.ini`, `*.toml`, `*.json`, `*.xml` or `*.properties`) and locations (JAR files, file system files, streams, HTTP resources or GIT repositories) and for converting properties to and from POJOs (plain java objects) and for applying various transformations and views to the properties.***

## Getting started ##

> Please refer to the [refcodes-properties: Managing your application's configuration](https://www.metacodes.pro/refcodes/refcodes-properties) documentation for an up-to-date and detailed description on the usage of this artifact.

## How do I get set up? ##

To get up and running, include the following dependency (without the three dots "...") in your `pom.xml`:

```
<dependencies>
	...
	<dependency>
		<artifactId>refcodes-properties</artifactId>
		<groupId>org.refcodes</groupId>
		<version>3.0.0</version>
	</dependency>
	...
</dependencies>
```

The artifact is hosted directly at [Maven Central](http://search.maven.org). Jump straight to the source codes at [Bitbucket](https://bitbucket.org/refcodes/refcodes-properties). Read the artifact's javadoc at [javadoc.io](http://www.javadoc.io/doc/org.refcodes/refcodes-properties).

## Contribution guidelines ##

* [Report issues](https://bitbucket.org/refcodes/refcodes-properties/issues)
* Finding bugs
* Helping fixing bugs
* Making code and documentation better
* Enhance the code

## Who do I talk to? ##

* Siegfried Steiner (steiner@refcodes.org)

## Terms and conditions ##

The [`REFCODES.ORG`](http://www.refcodes.org/refcodes) group of artifacts is published under some open source licenses; covered by the  [`refcodes-licensing`](https://bitbucket.org/refcodes/refcodes-licensing) ([`org.refcodes`](https://bitbucket.org/refcodes) group) artifact - evident in each artifact in question as of the `pom.xml` dependency included in such artifact.